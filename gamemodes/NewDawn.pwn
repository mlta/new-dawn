/**
 * Main executive file of NewDawn
 *
 * @author Moroz <moroz@malefics.com>
 * @author gim <myvrenik@gmail.com>
 * @author Jonathan_Rosewood <jonathan-rosewood@yandex.ru>
 *
 * Custom hooks (do not work for some reason)
 * OnPlayerLogIn(playerid, account)
 */

#pragma dynamic 17000

#include <a_samp>
#include <a_mysql>
#include <sscanf2>

#include <YSI\y_master>
#include <YSI\y_commands>
#include <YSI\y_ini>
#include <YSI\y_classes>
#include <YSI\y_groups>
#include <YSI\y_iterate>
#include <YSI\y_inline>
#include <YSI\y_dialog>
#include <YSI\y_timers>
#include <YSI\y_colours>
#include <YSI\y_text>
#include <YSI\y_hooks>

#include <streamer>
#include <WeatherSim>

#include <NewDawn/i18n>
#include <NewDawn/headers>
#include <mSelection>

#define OUTPUT     129
#define HASHSTR    129
#define SIZE_IP    129 // IPv6 support

#define GM_NAME "New Dawn"
#define GM_VERSION "0.9 Beta"

#define SALT 21398712

#define YSI_IS_SERVER

#define COLOR_NOTIFICATION X11_WHITE
#define COLOR_HELP         X11_LIGHT_GOLDENROD
#define COLOR_ACTION       0xC2A2DAAA
#define COLOR_RED          X11_RED
#define COLOR_ERROR        COLOR_RED
#define COLOR_OOC          X11_GREY67

#define COLOR_SUCCESS      X11_GREEN

#define COLOR_FADE1        X11_GREY91
#define COLOR_FADE2        X11_GREY79
#define COLOR_FADE3        X11_GREY67
#define COLOR_FADE4        X11_GREY55
#define COLOR_FADE5        X11_GREY43

#define COLOR_YELLOW       X11_YELLOW

#define RACE_WHITE     0
#define RACE_BLACK     1
#define RACE_ASIAN     2

//Function defines

#define CHECKPOINT_TYPE_SPECIAL 3
#define CHECKPOINT_TYPE_BIZZ    2
#define CHECKPOINT_TYPE_HOUSE   1
#define CHECKPOINT_TYPE_NONE    0

#define JOB_CARJACKER  1
#define JOB_TRUCKER    2
#define JOB_EMT        3

forward LoadPlayerInfo(query[], playerid, extraid, connectionHandle);
forward UserTryToLogIn(playerid, dialogid, response, listitem, string:inputtext[]);
forward SpawnChangeDialog(playerid, dialogid, response, listitem, string:inputtext[]);

native WP_Hash(buffer[], len, const str[]);

loadtext Core[Messages], Core[Errors];

new GM_WEB[32];

new mysql_server[24],
    mysql_db[24],
    mysql_user[32], 
    mysql_password[32],
	bool:mysql_debug_enabled,
	bool:mysql_mutex,
    MySQL;

new SexNames[2][7] = {
    "Male", "Female"
};

new MaleSkins[174][2] = {
    {7,RACE_BLACK},{14,RACE_BLACK},{15,RACE_WHITE},{21,RACE_BLACK},{28,RACE_BLACK},
    {29,RACE_WHITE},{30,RACE_WHITE},{32,RACE_WHITE},{33,RACE_WHITE},{34,RACE_WHITE},
    {35,RACE_WHITE},{43,RACE_WHITE},{44,RACE_WHITE},{47,RACE_WHITE},{48,RACE_WHITE},
    {58,RACE_WHITE},{60,RACE_WHITE},{62,RACE_WHITE},{67,RACE_BLACK},{68,RACE_WHITE},
    {72,RACE_WHITE},{73,RACE_WHITE},{94,RACE_WHITE},{95,RACE_WHITE},{100,RACE_WHITE},
    {101,RACE_WHITE},{112,RACE_WHITE},{121,RACE_WHITE},{122,RACE_WHITE},{123,RACE_WHITE},
    {124,RACE_WHITE},{125,RACE_WHITE},{126,RACE_WHITE},{127,RACE_WHITE},{128,RACE_WHITE},
    {132,RACE_WHITE},{133,RACE_WHITE},{142,RACE_BLACK},{143,RACE_BLACK},{144,RACE_BLACK},
    {156,RACE_BLACK},{158,RACE_WHITE},{159,RACE_WHITE},{160,RACE_WHITE},{161,RACE_WHITE},
    {162,RACE_WHITE},{167,RACE_WHITE},{168,RACE_BLACK},{170,RACE_WHITE},{176,RACE_BLACK},
    {177,RACE_WHITE},{179,RACE_WHITE},{181,RACE_WHITE},{182,RACE_BLACK},{183,RACE_BLACK},
    {184,RACE_WHITE},{188,RACE_WHITE},{202,RACE_WHITE},{206,RACE_WHITE},{209,RACE_WHITE},
    {210,RACE_WHITE},{217,RACE_WHITE},{220,RACE_BLACK},{222,RACE_BLACK},{234,RACE_WHITE},
    {235,RACE_WHITE},{236,RACE_WHITE},{241,RACE_WHITE},{242,RACE_WHITE},{247,RACE_WHITE},
    {248,RACE_WHITE},{250,RACE_WHITE},{254,RACE_WHITE},{258,RACE_WHITE},{259,RACE_WHITE},
    {261,RACE_WHITE},{262,RACE_BLACK},{264,RACE_WHITE},{291,RACE_WHITE},{292,RACE_WHITE},
    {293,RACE_BLACK},{299,RACE_WHITE},{1,RACE_WHITE},{2,RACE_WHITE},
    {19,RACE_BLACK},{22,RACE_BLACK},{23,RACE_BLACK},{26,RACE_WHITE},{36,RACE_WHITE},
    {37,RACE_WHITE},{49,RACE_WHITE},{51,RACE_BLACK},{52,RACE_WHITE},{96,RACE_WHITE},
    {99,RACE_WHITE},{203,RACE_WHITE},{204,RACE_WHITE},{80,RACE_BLACK},{81,RACE_WHITE},
    {18,RACE_WHITE},{45,RACE_WHITE},{97,RACE_WHITE},{146,RACE_WHITE},{154,RACE_WHITE},
    {252,RACE_WHITE},{102,RACE_WHITE},{103,RACE_WHITE},{104,RACE_WHITE},{105,RACE_WHITE},{106,RACE_WHITE},
    {107,RACE_WHITE},{108,RACE_WHITE},{109,RACE_WHITE},{110,RACE_WHITE},{114,RACE_WHITE},{115,RACE_WHITE},
    {116,RACE_WHITE},{173,RACE_WHITE},{174,RACE_WHITE},{175,RACE_WHITE},
    {17,RACE_BLACK},{20,RACE_BLACK},{24,RACE_BLACK},{25,RACE_BLACK},{46,RACE_WHITE},
    {57,RACE_WHITE},{59,RACE_WHITE},{66,RACE_BLACK},{82,RACE_WHITE},{83,RACE_BLACK},
    {84,RACE_WHITE},{113,RACE_WHITE},{117,RACE_WHITE},{118,RACE_WHITE},{120,RACE_WHITE},
    {147,RACE_WHITE},{165,RACE_WHITE},{166,RACE_WHITE},{171,RACE_WHITE},{180,RACE_BLACK},
    {185,RACE_BLACK},{186,RACE_WHITE},{187,RACE_WHITE},{189,RACE_WHITE},{221,RACE_BLACK},
    {223,RACE_WHITE},{227,RACE_WHITE},{228,RACE_WHITE},{229,RACE_WHITE},{240,RACE_WHITE},
    {249,RACE_BLACK},{253,RACE_BLACK},{272,RACE_WHITE},{290,RACE_WHITE},{294,RACE_WHITE},
    {295,RACE_WHITE},{296,RACE_BLACK},{297,RACE_BLACK},{111,RACE_WHITE},{295,RACE_WHITE},
    {296,RACE_BLACK},{297,RACE_BLACK},
    //0.3d
    {3,RACE_WHITE},{4,RACE_BLACK},{5,RACE_BLACK},{6,RACE_BLACK},{8,RACE_WHITE},{42,RACE_WHITE},
    {86,RACE_BLACK},{119,RACE_WHITE},{149,RACE_BLACK},{208,RACE_WHITE},{273,RACE_WHITE},{289,RACE_WHITE}
};

new FemaleSkins[80][2] = {
    {56,RACE_WHITE},{69,RACE_BLACK},{93,RACE_WHITE},{131,RACE_WHITE},{151,RACE_WHITE},
    {157,RACE_WHITE},{191,RACE_WHITE},{192,RACE_WHITE},{198,RACE_WHITE},{201,RACE_WHITE},
    {205,RACE_WHITE},{211,RACE_WHITE},{226,RACE_WHITE},{245,RACE_WHITE},{246,RACE_WHITE},
    {178,RACE_BLACK},{207,RACE_WHITE},{225,RACE_WHITE},{238,RACE_BLACK},{243,RACE_WHITE},
    {63,RACE_WHITE},{64,RACE_WHITE},{75,RACE_WHITE},{87,RACE_WHITE},{152,RACE_WHITE},
    {237,RACE_WHITE},{244,RACE_WHITE},{256,RACE_BLACK},{257,RACE_WHITE},{298,RACE_WHITE},
    {88,RACE_WHITE},{89,RACE_WHITE},{129,RACE_WHITE},{130,RACE_WHITE},{196,RACE_WHITE},
    {197,RACE_WHITE},{199,RACE_WHITE},{231,RACE_WHITE},{232,RACE_WHITE},{138,RACE_WHITE},
    {10,RACE_BLACK},{13,RACE_BLACK},{31,RACE_WHITE},{39,RACE_WHITE},{54,RACE_WHITE},
    {9,RACE_BLACK},{11,RACE_BLACK},{12,RACE_WHITE},{40,RACE_WHITE},{55,RACE_WHITE},
    {76,RACE_WHITE},{85,RACE_WHITE},{91,RACE_WHITE},{141,RACE_WHITE},{148,RACE_BLACK},
    {150,RACE_WHITE},{169,RACE_WHITE},{172,RACE_WHITE},{194,RACE_WHITE},{214,RACE_WHITE},
    {215,RACE_BLACK},{216,RACE_WHITE},{219,RACE_BLACK},{224,RACE_WHITE},{233,RACE_WHITE},
    {263,RACE_WHITE},
    {38,RACE_WHITE},{90,RACE_WHITE},{92,RACE_WHITE},{41,RACE_WHITE},{190,RACE_BLACK},
    {193,RACE_WHITE},{195,RACE_BLACK},{138,RACE_WHITE},{139,RACE_BLACK},{140,RACE_WHITE},
    {145,RACE_WHITE},{251,RACE_WHITE},{138,RACE_WHITE}, {65,RACE_BLACK}
};

enum eParams {
    epName[24],
    epType
};
/* { PVar, Type} Types: 3 - int, 2 - float, 1 - string*/
new const SavedParams[][eParams] = {
    { "Dead", 3 }, 
    { "Sex", 3 }, { "Race", 3 }, { "Age", 3 }, { "MinutesOn", 3 },  { "Premium", 3 },
    { "Money", 3}, { "Bank",  3 }, { "Job", 3 },
    { "Org", 3 }, { "Rank", 3}, {"Skin", 3}, 
	{ "JailTime", 3},  { "JailCell", 3},
    { "LastX", 2}, {"LastY", 2}, { "LastZ", 2}, { "LastFA", 2}, { "LastInt", 3 },{ "LastVW", 3},
    { "EquippedMelee", 3 }, { "EquippedMisc", 3 },  
    { "EquippedGun1", 3 }, { "EquippedAmmo1", 3 },  { "EquippedSerial1", 3 },
    { "EquippedGun2", 3 }, { "EquippedAmmo2", 3 },  { "EquippedSerial2", 3 },
    { "EquippedExplosives", 3 }, 
	{ "EquippedPhone", 3 }, { "PhoneNumber", 3 }, 
    { "DrugAddictLevel", 3 },
    
    /*Look extremely ugly, but that's easier*/
    // That looks just awful, use sscanf2 for this you lazy bitch!
    { "InvItem0", 3 }, { "InvQuantity0", 3 }, { "InvSerial0", 3 }, 
    { "InvItem1", 3 }, { "InvQuantity1", 3 }, { "InvSerial1", 3 }, 
    { "InvItem2", 3 }, { "InvQuantity2", 3 }, { "InvSerial2", 3 }, 
    { "InvItem3", 3 }, { "InvQuantity3", 3 }, { "InvSerial3", 3 }, 
    { "InvItem4", 3 }, { "InvQuantity4", 3 }, { "InvSerial4", 3 }, 
    { "InvItem5", 3 }, { "InvQuantity5", 3 }, { "InvSerial5", 3 }, 
    { "InvItem6", 3 }, { "InvQuantity6", 3 }, { "InvSerial6", 3 }, 
    { "InvItem7", 3 }, { "InvQuantity7", 3 }, { "InvSerial7", 3 }, 
    { "InvItem8", 3 }, { "InvQuantity8", 3 }, { "InvSerial8", 3 }, 
    { "InvItem9", 3 }, { "InvQuantity9", 3 }, { "InvSerial9", 3 }, 
    { "InvItem10", 3 }, { "InvQuantity10", 3 }, { "InvSerial10", 3 }, 
    { "InvItem11", 3 }, { "InvQuantity11", 3 }, { "InvSerial11", 3 }, 
    { "InvItem12", 3 }, { "InvQuantity12", 3 }, { "InvSerial12", 3 }, 
    { "InvItem13", 3 }, { "InvQuantity13", 3 }, { "InvSerial13", 3 }, 
    { "InvItem14", 3 }, { "InvQuantity14", 3 }, { "InvSerial14", 3 },
	{ "Description_Look", 1 }, { "Description_Look2", 1 }, { "Description_Voice", 1 }
};

new Classes[sizeof(MaleSkins)+sizeof(FemaleSkins)];

enum premiumLevels {
    Group:Bronze,
    Group:Silver,
    Group:Gold
};

new Group:gMale, Group:gFemale,
    Group:gBlack, Group:gWhite, Group:gAsian,
    Group:gPremium[premiumLevels],
    Group:gPlayers, Group:gGuests,
    Group:gCountdown,
    Group:gAdmins[MAX_ADMIN_LEVEL];

new Language:LRussian, Language:LEnglish;

new Iterator:Vehicle<MAX_VEHICLES>;
    
#include <NewDawn/functions>

INI:NewDawn[Settings](name[], value[]) {
    INI_String("Website", GM_WEB, sizeof(GM_WEB));
    return 0;
}
INI:NewDawn[MySQL](name[], value[]) {
    INI_String("Server", mysql_server, sizeof(mysql_server));
    INI_String("Database", mysql_db, sizeof(mysql_db));
    INI_String("User", mysql_user, sizeof(mysql_user));
    INI_String("Password", mysql_password, sizeof(mysql_password));
    INI_Bool("Debug", mysql_debug_enabled);
    INI_Bool("Mutex", mysql_mutex);
    return 0;
}

stock Spawn(playerid) {
    new string[512], pID = GetPVarInt(playerid, "ID");
    format(string, sizeof(string), "{DDDDDD}Last known position (%s)", GetZoneName(GetPVarFloat(playerid, "LastX"), GetPVarFloat(playerid, "LastY")));
	foreach(new p : Propertys) {
		if(PropertyInfo[p][prOwnerID] != pID)
			continue;
		format(string, sizeof(string), "%s\nProperty #%d: %s", string, p, PropertyInfo[p][prDescript]);
	}

    Dialog_ShowCallback(playerid, using callback SpawnChangeDialog, DIALOG_STYLE_LIST, "Choose your spawn location", string, "Select");
    return 1;
}

main() { }

hook OnGameModeInit() {

    INI_Load("NewDawn.conf");
	
    mysql_debug(mysql_debug_enabled);
	enable_mutex(mysql_mutex);
	
	MySQL = mysql_connect(mysql_server, mysql_user, mysql_db, mysql_password);
	if(mysql_ping(MySQL) == -1) {
		printf("Fatal Error: MySQL connection failed: %d", mysql_errno(MySQL));
		MySQL = -1;
		return 0;
	}

    printf("MySQL connection %s@%s has been established!", mysql_user, mysql_server);

    DisableNameTagLOS();
    SetNameTagDrawDistance(10.0);
    DisableInteriorEnterExits();
	EnableStuntBonusForAll(false);
    SetGameModeText(GM_NAME" "#GM_VERSION);
    
    Group_SetGlobalCommandDefault(false);
    
    gGuests = Group_Create("Guests");
    gPlayers = Group_Create("Players");
    Group_SetCommandDefault(gPlayers, true);
    
    gMale = Group_Create("Male");
    gFemale = Group_Create("Female");
    gAsian = Group_Create("Asian");
    gBlack = Group_Create("Black");
    gWhite = Group_Create("White");
    
    gCountdown = Group_Create("Countdown");

    LRussian = Langs_Add("RU", "Russian");
    LEnglish = Langs_Add("EN", "English");

    new i=0;
    for(i=0;i<sizeof(MaleSkins);i++)
        Classes[i] = Class_AddForGroup(gMale, MaleSkins[i][0], -1753.6824,886.0016,295.8750, 0.0);
    for(new k=0;k<sizeof(FemaleSkins);k++)
        Classes[i+k] = Class_AddForGroup(gFemale, FemaleSkins[k][0], -1753.6824,886.0016,295.8750, 0.0);
    
    for(new k=0;k<MAX_CYC;k++) CycloneInit(k);
    SetTimer("CycloneProcess", 30000, true); // moves cyclones
    SetWeatherTime(12, 00); // default time (!) not an SetWorldTime (!)
    SetTimer("UpdateWeather", 10000, true); // weather updates players
    
    gPremium[Bronze] = Group_Create("Bronze premium");
    gPremium[Silver] = Group_Create("Silver premium");
    gPremium[Gold] = Group_Create("Gold premium");

    printf("============================");
    printf("|                          |");
    printf("|         %10s    |", GM_NAME);
    printf("|          %s         |", GM_VERSION);
    printf("|   (c) M-E   2011-2013    |");
    printf("|                          |");
    printf("============================");

    ManualVehicleEngineAndLights();

    return 1;
}

#include <NewDawn/maps>
#include <NewDawn/modules>
#include <NewDawn/jobs>

stock UpdateLastPos(playerid) {
    new Float:x, Float:y, Float:z, Float:fa;
    GetPlayerPos(playerid, x,y,z);
    GetPlayerFacingAngle(playerid, fa);
    SetPVarFloat(playerid, "LastX", x);
    SetPVarFloat(playerid, "LastY", y);
    SetPVarFloat(playerid, "LastZ", z);
    SetPVarFloat(playerid, "LastFA", fa);
    SetPVarInt(playerid, "LastInt", GetPlayerInterior(playerid));
    SetPVarInt(playerid, "LastVW", GetPlayerVirtualWorld(playerid));
    SetSpawnInfo(playerid, 1, GetPVarInt(playerid, "Skin"), GetPVarFloat(playerid, "LastX"), GetPVarFloat(playerid, "LastY"), GetPVarFloat(playerid, "LastZ"), GetPVarFloat(playerid, "LastFA"), 0,0,0,0,0,0);
    return true;
}

public OnPlayerExitVehicle(playerid,vehicleid)
{
    return 1;
}

public OnPlayerUpdate(playerid) {    
	return 1;
}

public OnGameModeExit() {
    foreach(Player, playerid) 
    {
		Text_Send(playerid, $GAMEMODE_EXIT);
        Kick(playerid);
    }
    DestroyAllDynamicCPs();
    DestroyAllDynamic3DTextLabels();
    
    mysql_close(MySQL);
    return 1;
}

public OnPlayerConnect(playerid) {
	if(MySQL == -1) {
		SendClientMessage(playerid, COLOR_RED, "*** SERVER LAUNCH ERROR ***");
		Kick(playerid);
		return 1;
	}
    new string[OUTPUT];

    Langs_SetPlayerLanguage(playerid, LRussian);

    Group_SetPlayer(gGuests, playerid, true);
    Group_SetPlayer(gPlayers, playerid, false);

    Group_SetPlayer(gPremium[Bronze], playerid, false);
    Group_SetPlayer(gPremium[Silver], playerid, false);
    Group_SetPlayer(gPremium[Gold], playerid, false);

    Group_SetPlayer(gMale, playerid, false);
    Group_SetPlayer(gFemale, playerid, false);
    Group_SetPlayer(gWhite, playerid, false);
    Group_SetPlayer(gBlack, playerid, false);
    Group_SetPlayer(gAsian, playerid, false);
    Group_SetPlayer(gCountdown, playerid, false);

    SetPVarInt(playerid, "RentedHouse", -1);
    SetPVarInt(playerid, "LogAttempts", 5); 
    mysql_format(MySQL, string, "SELECT * FROM `characters` as t1 LEFT JOIN `players` as t2 ON t1.PlayerID = t2.PlayerID WHERE `Name` = '%s' LIMIT 1;", GetPlayerNameEx(playerid, false));
    mysql_query_callback(playerid, string, "LoadPlayerInfo", playerid, MySQL, true);
	
	SetPlayerColor(playerid, 0xFFFFFF00);
    return 1;
}

hook OnPlayerDisconnect(playerid, reason) {
	if(!LoggedIn(playerid)) 
		return 1;
    UpdateLastPos(playerid);
    SavePlayer(playerid);
    
	return 1;
}

public OnPlayerText(playerid, text[]) {
    if(!LoggedIn(playerid)) return false;
    new string[OUTPUT], onPhoneWith = GetPVarInt(playerid, "OnPhoneWith")-1;
	
	if(onPhoneWith >= 0) {
		new Float:x, Float:y, Float:z;
		GetPlayerPos(playerid, x, y, z);
		format(string, sizeof(string), "%s says [%s] (cellphone):", GetPlayerNameEx(playerid));
		SendLocalMessageEx(string, text, "", 15.0, x,y,z, 5, { COLOR_FADE1, COLOR_FADE2, COLOR_FADE3, COLOR_FADE4, COLOR_FADE5}, GetPlayerInterior(playerid), GetPlayerVirtualWorld(playerid));
		GetPlayerPos(onPhoneWith, x, y, z);
		SendLocalMessageEx(string, text, "", 1.0, x,y,z, 6, { X11_YELLOW, COLOR_FADE1, COLOR_FADE2, COLOR_FADE3, COLOR_FADE4, COLOR_FADE5}, GetPlayerInterior(onPhoneWith), GetPlayerVirtualWorld(onPhoneWith));
		
	}
	else {
		format(string, sizeof(string), "/say %s", text);
		Command_ReProcess(playerid, string, 0);
	}
    return false;
}

public OnPlayerCommandText(playerid, cmdtext[]) {
    if(!LoggedIn(playerid)) return true;
    return true;
}

public OnPlayerRequestClass(playerid, classid) { 
    PlayerWeatherLock(playerid);
    if(!LoggedIn(playerid)) return 0;
    SetPlayerPos(playerid, -1753.6824,886.0016,295.8750);
    SetPlayerCameraPos(playerid, -1747.6824,886.0016,295.8750);
    SetPlayerCameraLookAt(playerid, -1753.6824,886.0016,295.8750);
    return 1;
}

public OnPlayerRequestSpawn(playerid) {
    if(!LoggedIn(playerid)) return 0;
    return 1;
}

public OnPlayerSpawn(playerid){
    PlayerWeatherUnLock(playerid);
    SetPlayerSkin(playerid, GetPVarInt(playerid, "Skin"));
    if(GetPVarInt(playerid, "Dead") != 0) {
		SetPlayerPosFindZ(playerid, GetPVarFloat(playerid, "LastX"),  GetPVarFloat(playerid, "LastY"),  GetPVarFloat(playerid, "LastZ"));
		Text_Send(playerid, $DEATH_MESSAGE_LINEONE); 
		ApplyAnimation(playerid,"PED","KD_left",4.1,0,1,1,1,0);
        defer AcceptDeath(playerid);
		TogglePlayerControllable(playerid, false);
    }
	
	for(new i=0;i<5;i++) {
		new weapon, ammo, serial;
		GetEquippedItemInfo(playerid, i, weapon, ammo, serial);
		SetPVarInt(playerid, EquipSlots[i][1], 0);
		SetEquippedItem(playerid, i, weapon, ammo, serial);
	}
    return 1;
}

YCMD:accept(playerid, params[], help) {
    if(help) return Text_Send(playerid, $COMMAND_HELP, YCMD:acceptdeath, "accept character death");
	if(!strcmp(params, "death", true)) {
		if(!GetPVarInt(playerid, "AcceptDeathAllowed")) 
			return Text_Send(playerid, $ACCEPT_DEATH_UNREADY);
		DeletePVar(playerid, "Dead");
		DeletePVar(playerid, "AcceptDeathAllowed");
		Spawn(playerid);
	}
    return 1;
}

timer AcceptDeath[60000](playerid) {
    SetPVarInt(playerid, "AcceptDeathAllowed", 1);
    Text_Send(playerid, $ACCEPT_DEATH_READY);
    return 1;
}

task Countdown[1000]() {
    new secs = 0, secstring[5];
    foreach(new playerid : Group(gCountdown)) {
        secs = GetPVarInt(playerid, "CountdownTime")-1;
        SetPVarInt(playerid, "CountdownTime", secs);
        valstr(secstring,secs);
        if(secs == 0) {
            Player_FinishCountdown(playerid);
        }
        else {
            GameTextForPlayer(playerid, secstring, 1000, 5);
        }
    }
}

task MinuteTimer[60000]() {
	foreach(new playerid : Player) {
		new minutesOn = GetPVarInt(playerid, "MinutesOn")+1; 
		SetPVarInt(playerid, "MinutesOn", minutesOn);
		SetPlayerScore(playerid, minutesOn/240);
	}
}


public LoadPlayerInfo(query[], playerid, extraid, connectionHandle) {
    new rows=0, fields=0;
    cache_get_data(rows, fields, connectionHandle);
    if(rows != 1) {
        Text_Send(playerid, $NO_ACCOUNT_MESSAGE, GM_WEB);
        defer KickPlayer(playerid);
        return 0;
    }
    new string[256];
    cache_get_field_content(0, "id", string, connectionHandle);
    SetPVarInt(playerid, "ID", strval(string));
	
    cache_get_field_content(0, "Password", string, connectionHandle); 
    SetPVarString(playerid, "Password", string);
	cache_get_field_content(0, "PlayerID", string, connectionHandle);
	SetPVarInt(playerid, "PlayerID", strval(string));
	CheckLists(playerid);
    cache_get_field_content(0, "Username", string, connectionHandle); 
    SetPVarString(playerid, "Username", string);
    cache_get_field_content(0, "Email", string, connectionHandle); 
    SetPVarString(playerid, "Email", string);
    cache_get_field_content(0, "AdmCode", string, connectionHandle); 
    SetPVarString(playerid, "AdmCode", string);
	cache_get_field_content(0, "AdminLevel", string, connectionHandle);
	SetPVarInt(playerid, "AdminLevel", strval(string));
	
	foreach(new ply : Player) {
		if(GetPVarInt(playerid, "PlayerID") == GetPVarInt(ply, "PlayerID") && LoggedIn(ply) && ply != playerid) {
			Text_Send(playerid, $USER_USED, ply, ply);
			defer KickPlayer(playerid);
			return 0;
		}
	}
    
    for(new i=0;i<sizeof(SavedParams);i++) {
        cache_get_field_content(0, SavedParams[i][epName], string, connectionHandle);
        if(SavedParams[i][epType] == 3) //int 
            SetPVarInt(playerid, SavedParams[i][epName], strval(string));
        else if(SavedParams[i][epType] == 2) // float
            SetPVarFloat(playerid, SavedParams[i][epName], floatstr(string));
        else if(SavedParams[i][epType] == 1) // string
            SetPVarString(playerid, SavedParams[i][epName], string);
    }
    
    if(GetPVarInt(playerid, "Sex") == 0) 
        Group_SetPlayer(gFemale, playerid, true);
    else
        Group_SetPlayer(gMale, playerid, true);
    SetPlayerRace(playerid, GetPVarInt(playerid, "Race"));
    new money = GetPVarInt(playerid, "Money");
    ResetPlayerMoney(playerid);
    GivePlayerMoney(playerid, money);

    
    format(string, sizeof(string), "\
            {FFFFFF}________________________________________________\n\n\
            {33CCFF}Welcome to the server, {FFFFFF}%s{33CCFF}.\n\
            Please, type down your password to log in.\n\n\
            {FFFFFF}________________________________________________\n\n", GetPlayerNameEx(playerid));
    Dialog_ShowCallback(playerid, using callback UserTryToLogIn, DIALOG_STYLE_PASSWORD, "Signing in", string, "Log in", "Leave");

    return 0;
}

hook OnPlayerLogIn(playerid, account) {
    SetPVarInt(playerid, "LoggedIn", 1);
    static HookTime[MAX_PLAYERS];
    if(HookTime[playerid]++ == 1) 
        return 0;
    Group_SetPlayer(gGuests, playerid, false);
    Group_SetPlayer(gPlayers, playerid, true);
    for(new i = 0; i < sizeof(gPremium) && i < GetPVarInt(playerid, "Premium"); i++) {
        Group_SetPlayer(gPremium[premiumLevels:i], playerid, true);
    }   
    for(new i = 0; i < sizeof(gAdmins) && i < GetPVarInt(playerid, "AdminLevel"); i++)
        Group_SetPlayer(gAdmins[i], playerid, true);
#if defined  vehicles_included
	ReloadPlayerVehicleList(playerid);
#endif
#if defined factions_included
	new faction = GetPlayerFaction(playerid)-1;
	if(faction > 0 && faction < MAX_FACTIONS) {
		Group_SetPlayer(FactionInfo[faction][fGroup], playerid, true);
	}
#endif
    return 1;
}

public OnPlayerEnterDynamicCP(playerid, checkpointid) {
    return 1;
}

public OnPlayerLeaveDynamicCP(playerid, checkpointid) {
    return 1;
}

public OnPlayerClickPlayerTextDraw(playerid, PlayerText:playertextid) {
    if(_:playertextid == INVALID_TEXT_DRAW) 
        return CancelSelectTextDraw(playerid);
    return true;
}

public UserTryToLogIn(playerid, dialogid, response, listitem, string:inputtext[]) {
    #pragma unused dialogid, listitem
    new str[HASHSTR];
    if(!response) {
        Text_Send(playerid, $REGISTRATION_LEAVE);
        defer KickPlayer(playerid);
		return 1;
    }
    else {
        new passwd[HASHSTR];
        GetPVarString(playerid, "Password", passwd, sizeof(passwd));
        WP_Hash(str, sizeof(str), inputtext);
        if(!strcmp(str, passwd, true, strlen(str))) {
            Text_Send(playerid, $LOGGING_IN_SUCCESS);
            hook OnPlayerLogIn(playerid, GetPVarInt(playerid, "ID"));
            Spawn(playerid);
        }
        else {
            new attempts = GetPVarInt(playerid, "LogAttempts")-1;
            SetPVarInt(playerid, "LogAttempts", attempts);
            if(attempts == -1) {
                Text_Send(playerid, $WRONGPASS_BAN_MSG);
                BanEx(playerid, "5 Failed attempts to log in");
            }
            else if(attempts == 0) 
                format(str, sizeof(str), "\
                    {FFFFFF}________________________________________________\n\n\
                    {33CCFF}You have entered a wrong password.\n\
                    It is your last chance to log in!\n\n\
                    {FFFFFF}________________________________________________\n\n");
            else
                format(str, sizeof(str), "\
                    {FFFFFF}________________________________________________\n\n\
                    {33CCFF}You have entered a wrong password.\n\
                    Attempts left: %d\n\n\
                    {FFFFFF}________________________________________________\n\n", attempts);
            Dialog_ShowCallback(playerid, using callback UserTryToLogIn, DIALOG_STYLE_PASSWORD, "Signing in", str, "Log in", "Leave");
        }
        
    }
   
    SetSpawnInfo(playerid, 1, GetPVarInt(playerid, "Skin"), GetPVarFloat(playerid, "LastX"), GetPVarFloat(playerid, "LastY"), GetPVarFloat(playerid, "LastZ"), GetPVarFloat(playerid, "LastFA"), 0,0,0,0,0,0);
	return 1;
}

public SpawnChangeDialog(playerid, dialogid, response, listitem, string:inputtext[]) {
    #pragma unused inputtext, response, dialogid
    switch(listitem) {
        case 0: { // last known
            SpawnPlayer(playerid);      
            MovePlayer(playerid, GetPVarFloat(playerid, "LastX"), GetPVarFloat(playerid, "LastY"), GetPVarFloat(playerid, "LastZ"),
                GetPVarFloat(playerid, "LastFA"), GetPVarInt(playerid, "LastInt"), GetPVarInt(playerid, "LastVW"));
            return 1;
        }
        default: {//houses
			new count = 0, pID = GetPVarInt(playerid, "ID");
			foreach(new p : Propertys) {
				if(PropertyInfo[p][prOwnerID] == pID)
					count++;
				if(count == listitem) {
					SpawnPlayer(playerid);  	
					
					MovePlayer(playerid, 
						PropertyInfo[p][prEnter][0], 
						PropertyInfo[p][prEnter][1],
						PropertyInfo[p][prEnter][2]);
                    break;
				}
			}
            return 1;
        }
    }
    return 1;
}

public OnPlayerDeath(playerid, killerid, reason) {
	SetPVarInt(playerid, "Dead", 1);
	UpdateLastPos(playerid);
	return 1;
}


hook OnRconCommand(cmd[]) {
	if(!strcmp(cmd, "restart", false)) {
		SendRconCommand("exit");
		return 1;
	}
	return 1;
}
