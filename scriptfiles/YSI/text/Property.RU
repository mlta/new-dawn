[Messages]
PROPERTY_INFOTITLE = -- ���������� � ������������ \"%s\" --
PROPERTY_INFO_1 = ������� ����� [%d], ������� ������������ [%d]
PROPERTY_INFO_2 = ������ ����� [%s], ������ ������������ [%s]
PROPERTY_BIZZ_INFO = ��������� [%d/%d], ������� ��� [%d], ������ [$%d]
BUSINESS_LIST = Business #%d: %s
HOUSES_LIST = House #%d: %s
HOUSE_MARKED_ON_MAP = ��� ������� �� �����!
PROPERTY_BOUGHT1 = - �����������! �� ��������� ����� �������������!
PROPERTY_BOUGHT2 = - ����������� ������� /property ��� ���������� ��������������!
IS_A_HOUSE = ��� ���������� ������ ����������� ������� /house
EDIT_OBJECT = ������� �� ������, ������� ������ ��������
PROPERTY_CREATED = ������������� ������� �������. ���: %s | ��������: %d | ����: $%d
PROPERTY_DESTROYED = ������������� '%s' (SQLID:%d) �������
[Errors]
NOT_NEAR = �� ������ ���� � ����� � ������������!
NOT_IN_A_PROPERTY = �� ������ ���� ������ ������������!
NOT_A_HOUSE = ��� �� ���!
NOT_ON_SALE = - ��� ������������� �� �� �������!
NOT_YOUR_PROPERTY = - ��� ������������ �� ����������� ���!
UNKNOWN_ID = ����� �������� �������������!
NO_DOOR_HERE = ��� ������ ����������!
LIMITED_PROPERTY_PER_PLAYER = �� �� ������ ���������� ������ %d �������������!
UNKNOWN_TYPE = ����������� ��� ������������!