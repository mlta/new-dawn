[Messages]
ITEM_WAS_GIVEN_BY = %s ������� ��� %d %s (# %d)
ITEM_WAS_GIVEN_TO = �� �������� %s %d %s (# %d)
ITEM_LIST = ������� %d (%s): ���[%s], ������[%d], ���������[%d], ��������[%d], ���[%f]
ITEM_PICKUP = �� ��������� %s � ���������� %d ��. 
[Errors]
NO_FIREARM_EQUIPPED     = �� �� ������ ��������� �������, ������ ��� � ��� ��� ����������� ������.
WEAPON_CALIBRE_DIFFERS  = �� �� ������ ��������� �������, ������ ��� ������ ���������� �� ������.
DROPPED_ITEMS_NO_SLOTS  = ��������, ����� �� ��������� �������� ��������. ���������� �����.
NO_ITEMS_TO_PICK_UP     = �� ������� ���������, ������� ����� ���������.
ITEM_ALREADY_PICKED_UP  = ���-�� ��� ������ ���� �������.
PLAYER_DOESNT_HAVE_ITEM = � ��� ��� ����� ��������!
AMOUNT_MUST_BE_POSITIVE = ���������� ������ ���� �������������!
AMOUNT_IS_TOO_HIGH      = � ���������, ��� ������� ������� ��������. �������� ������ �����.
INVENTORY_HAS_NO_ROOM   = ������������ ����� � ���������.
INVENTORY_IS_EMPTY      = ��������� ����!
GIVING_FAILED           = � ���� �������� �������� ��������� ������!
ITEM_REQUIRED = ��� ����� ������� '%s'!