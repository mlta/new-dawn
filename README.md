# NewDawn SA-MP gamemode #

NewDawn - is a modular framework-gamemode (scenario) for San Andreas Multiplayer Server.

### INSTALLATION ###

* Extract the contents of the repository to the original copy of the SA-MP server.
* Configure the main configuration file (server.cfg).
* Compile the game mode using a PAWN compiler 3.2.3664 (part of the server).
* etc.. (todo)

### REQUIREMENTS ###

We highly recommend that you use the following version of the third-party software.
Since the gamemode is developed and tested on them.

* [San Andreas Multiplayer Server 0.3e](http://sa-mp.com/download) or higer
* [YSI library 3.1](http://forum.sa-mp.com/showthread.php?t=321092) by Y_Less or higer
* [Streamer Plugin v2.7.4](http://forum.sa-mp.com/showthread.php?t=102865) by Incognito or higer
* [sscanf 2.8.1](http://forum.sa-mp.com/showthread.php?t=120356) by Y_Less or higer
* [MySQL plugin R39-2](http://forum.sa-mp.com/showthread.php?t=56564) by BlueG or higer
* [Whirlpool 1.0](http://forum.sa-mp.com/showthread.php?t=65290) by Y_Less or higer
* fixes2 plugin

The NewDawn Developer Team