/**
 * List include files of functions
 * @author Moroz <moroz@malefics.com>
 * @author Jonathan_Rosewood <jonathan-rosewood@yandex.ru>
 *
 * Example of major functions:
 * #include <gamemode/functions/filename.inc>
 *
 * Example of minor features:
 * #tryinclude <gamemode/functions/filename.inc>
 *
 */

#include <NewDawn/functions/misc>
#include <NewDawn/functions/checkpoints>
#include <NewDawn/functions/weapons>
#include <NewDawn/functions/players>
#include <NewDawn/functions/vehicles>
#include <NewDawn/functions/zones>
#include <NewDawn/functions/dialogs>
#include <NewDawn/functions/special_actions>
