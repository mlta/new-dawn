/**
 * ������������ ���� ������ ��������� ��� ������� �����
 *
 * @version 0.5 
 * @author Moroz <moroz@malefics.com
 *
 *
 * �������: 
 *
 * @see Player_ShowVehicleInventory(playerid, vehicle)
 * @see Vehicle_SortInventory(veh)
 * @see Vehicle_GetInventoryItemCount(vehicleid);
 * @see Vehicle_AddItem(vehicle, item, quantity, serial=0)
 */
 
 #if !defined inventory_included || defined vehicle_inventory_included
	#endinput
#endif 
#define vehicle_inventory_included

#define VEHICLE_TRUNK_SIZE 40


forward PutInCarInventory_Numbered(playerid, dialogid, response, listitem, string:inputtext[]);
forward VehicleItemObserve(playerid, dialogid, response, listitem, string:inputtext[]);
forward VehicleItemAction(playerid, dialogid, response, listitem, string:inputtext[]);
forward VehicleDialog(playerid, dialogid, response, listitem, string:inputtext[]);
forward PutInCarInventory(playerid, dialogid, response, listitem, string:inputtext[]);
