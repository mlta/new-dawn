/**
 * admin.inc
 *
 * Header file for admin system
 * @author Moroz <moroz@malefics.com>
 * @version 1.0
 *
 * 
 * Functions:
 * @see SaveFaction(factionid)
 *
 */

 #if defined admin_included
	#endinput
#endif
#define admin_included


#define ADMIN_SPEC_TYPE_NONE 0
#define ADMIN_SPEC_TYPE_PLAYER 1
#define ADMIN_SPEC_TYPE_VEHICLE 2 


#define MAX_ADMIN_LEVEL        (5)
#define ADMIN_LEVEL_NONE       (0)
#define ADMIN_LEVEL_TESTER     (1)
#define ADMIN_LEVEL_MODERATOR  (2)
#define ADMIN_LEVEL_REGULAR    (3)
#define ADMIN_LEVEL_LEAD       (4)
#define ADMIN_LEVEL_SUPER      (5)
