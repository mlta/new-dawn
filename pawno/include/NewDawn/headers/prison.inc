 /**
 * Header file for prison module
 *
 * @version 1.0
 * @author Moroz <igorsmorozov@gmail.com>
 */
 
#if defined prison_included || !defined __property_inc_included
	#endinput
#endif
#define prison_included

#define MAX_PRISON_CELLS 10 // Per prison



enum prisonCellInfo {
	pcID,
	Float:pcPosX,
	Float:pcPosY,
	Float:pcPosZ,
	Float:pcRotZ,
	pcInt,
	pcVW
};
new PrisonCellInfo[P_COUNT][MAX_PRISON_CELLS][prisonCellInfo];

