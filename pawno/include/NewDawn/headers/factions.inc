/**
 * factions.inc
 *
 * �������� ���� ������� �������
 * @author Moroz <moroz@malefics.com>
 * @version 1.0
 *
 * �������:
 *
 * Functions:
 * @see SaveFaction(factionid)
 * @see CreateFaction(factionid, name[], owner)
 * @see CountFactionMembersOnline(factionid)
 *
 */

 #if defined factions_included
	#endinput
#endif
#define factions_included


#define FACTION_TYPE_NONE       0
#define FACTION_TYPE_POLICE     1 // or any law enforcement agency
#define FACTION_TYPE_MILITARY   2 // or any PMC 
#define FACTION_TYPE_MEDICS     3 
#define FACTION_TYPE_CRIMINAL   4 // mafias, gangs, etc.
#define FACTION_TYPE_CIVILIAN   5 //taxi, skydiving, etc.
#define FACTION_TYPE_GOVERNMENT 6

#define MAX_FACTIONS 20
#define MAX_RANK 15

#define FACTION_RANK_ACCESS_MAX (10)

forward LoadFactions(query[], index, extraid, connectionHandle);

new const FactionTypes[][] = {
	"Empty", "Law enforcement", 
	"Medical", "Criminal", "Civilian"
};

enum fInfo {
	Group:fGroup,
	fName[32],
	fOwner,
	fOwnerName[24],
	fType,
	fBank,
	fLevel,
	fPoints,
	fMembers
};

#define RANK_ACCESS_LEADER   10

new FactionInfo[MAX_FACTIONS][fInfo];
new FactionRanks[MAX_FACTIONS][MAX_RANK][16];
new FactionRankAccess[MAX_FACTIONS][MAX_RANK];
new FactionRankPaycheck[MAX_FACTIONS][MAX_RANK];

enum fLog {
	flog_none   = 0,
	flog_join   = 1,
	flog_leave  = 2,
	flog_name   = 3,
	flog_create = 4,
	flog_delete = 5,
	flog_move   = 6,
	flog_type   = 7,
	flog_rename = 8,
	flog_promote = 9
};
