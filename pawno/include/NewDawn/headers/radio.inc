#if defined radio_included
    #endinput
#endif
#define radio_included

#define MAX_RADIO 24


enum radioInfo {
	rsName[64],
	rsURL[128]
};

new const RadioInfo[MAX_RADIO][radioInfo] = {
	{"Highway 181 (Classic country)", "http://yp.shoutcast.com/sbin/tunein-station.pls?id=297243" }, 
	{"The Howard Stern channel", "http://yp.shoutcast.com/sbin/tunein-station.pls?id=189904" }, 
	{"SKY.FM - 80s", "http://yp.shoutcast.com/sbin/tunein-station.pls?id=632691" }, 
	{"SKY.FM - Smooth Jazz", "http://yp.shoutcast.com/sbin/tunein-station.pls?id=86433" }, 
	{"SKY.FM - Classical", "http://yp.shoutcast.com/sbin/tunein-station.pls?id=349525" }, 
	{"SKY.FM - New Age / World Music", "http://yp.shoutcast.com/sbin/tunein-station.pls?id=314987" }, 
	{"SKY.FM - 70s", "http://yp.shoutcast.com/sbin/tunein-station.pls?id=193169" }, 
	{"SKY.FM - Pop hits", "http://yp.shoutcast.com/sbin/tunein-station.pls?id=595424" }, 
	{"SKY.FM - Salsa", "http://yp.shoutcast.com/sbin/tunein-station.pls?id=835183" }, 
	{"GotRadio - Big Band", "http://yp.shoutcast.com/sbin/tunein-station.pls?id=1519847" }, 
	{"Comedy 104", "http://yp.shoutcast.com/sbin/tunein-station.pls?id=143090" }, 
	{"DEFJAY - R&B", "http://yp.shoutcast.com/sbin/tunein-station.pls?id=520036" }, 
	{"181.FM - Hip Hop", "http://yp.shoutcast.com/sbin/tunein-station.pls?id=611786" }, 
	{"Dirty South Radio - Rap", "http://yp.shoutcast.com/sbin/tunein-station.pls?id=1524202" }, 
	{"181.FM - 90s Alternative", "http://yp.shoutcast.com/sbin/tunein-station.pls?id=1034265" }, 
	{"MUSIK.METAL - Metal", "http://yp.shoutcast.com/sbin/tunein-station.pls?id=318248" }, 
	{"HARD ROCK HEAVEN - 80s hard rock and metal", "http://yp.shoutcast.com/sbin/tunein-station.pls?id=106750" }, 
	{"181.FM - Kickin' country", "http://yp.shoutcast.com/sbin/tunein-station.pls?id=1283687" }, 
	{"Folk Alley", "http://yp.shoutcast.com/sbin/tunein-station.pls?id=1273365" }, 
	{"Smoothbeats.", "http://yp.shoutcast.com/sbin/tunein-station.pls?id=9054" }, 
	{"La Grosse Radio Reggae.", "http://yp.shoutcast.com/sbin/tunein-station.pls?id=5661" }, 
	{"Coast To Coast", "http://radio.ctcfm.com:8000/listen.pls" }, 
	{"Power Metal - ROCKRADIO.com", "http://listen.rockradio.com/public3/powermetal.pls" }, 
	{"Best of the 80's", "http://www.sky.fm/play/the80s" }
};