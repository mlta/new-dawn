/**
 * drugs.inc
 *
 * ������������ ���� ��� ������� ����������.
 * @author gim <myvrenik@gmail.com>
 *
 */
 
#if defined __drugs_inc_included
    #endinput
#endif
#define __drugs_inc_included

new CurrentPlayerDrugTimer[MAX_PLAYERS];

enum drEffects {
    eDrunk,
    eGreenWeather,
    eRedWeather,
    eCrazyWeather,
    eRedWindWeather,
    eDynamicWeather,
    eGlares,
    eRandomObjects,
    eLights,
    eSounds
}

enum drTypes {
    tCannabis,
    tCocaine,
    tCrack,
    tHeroine,
    tLSD,
    tMeth
}

enum drDrugInfoEnum {
    drName[10],
    drHealthInc,
    drHealthIncTime,
    drHealthIncFullTime,
    drAddictive
};
new const DrugInfo[drTypes][drDrugInfoEnum] = {
    {"Cannabis", 3, 15, 180, 0},
    {"Cocaine", 7, 10, 100, 3},
    {"Crack", 10, 15, 120, 5},
    {"Heroine", 5, 15, 180, 10},
    {"LSD", 3, 10, 180, 0},
    {"Meth", 5, 15, 150, 5}
};

new const DrugSoundsURL[][100] = {
    "http://www.sounddogs.com/sound-effects/25/mp3/265964_SOUNDDOGS__si.mp3",
    "http://www.sounddogs.com/sound-effects/25/mp3/265970_SOUNDDOGS__si.mp3"
};

// Some settings
new bool:WeatherEffectsAllowed = true;
INI:NewDawn[Drugs](name[], value[]) {
    INI_Bool("WeatherEffects", WeatherEffectsAllowed);
    return 0;
}
