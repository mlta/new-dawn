/*
 * Thanks gim aka gimini for the GunObjects
*/

new GunObjects[47] = {
	0,331,333,334,335,336,337,338,339,341,321,322,323,324,325,326,342,343,344,
	0,0,0,346,347,348,349,350,351,352,353,355,356,372,357,358,359,360,361,362,
	363,364,365,366,367,368,368,371
};

new MagazineSize[47] = {
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	17, 17, 7, 8, 2, 7, 50, 30, 30, 50, 50, 6, 5,
	1, 1, 50, 500, 1, 1, 500, 500, 36, 1, 1, 1
};


enum weaponTypes {
	WEAPON_TYPE_NONE,
	WEAPON_TYPE_MELEE,
	WEAPON_TYPE_HANDGUN,
	WEAPON_TYPE_SHOTGUN,
	WEAPON_TYPE_SMG,
	WEAPON_TYPE_ASSAULT_RIFLE,
	WEAPON_TYPE_RIFLE,
	WEAPON_TYPE_LAUNCHER,
	WEAPON_TYPE_EXPLOSIVE,
	WEAPON_TYPE_MISC
};

stock bool:IsValidWeapon(weaponid) {
	return !(weaponid < 0 || weaponid > 46 || weaponid >= 19 && weaponid <= 21);
}

stock weaponTypes:GetWeaponType(weaponid) {
	if(weaponid < 1 || weaponid > 46) 
		return WEAPON_TYPE_NONE;
	if(weaponid >=1 && weaponid <= 15)
		return WEAPON_TYPE_MELEE;
	if(weaponid >= 16 && weaponid <= 18 || weaponid == 39)
		return WEAPON_TYPE_EXPLOSIVE;
	if(weaponid >= 22 && weaponid <= 24)
		return WEAPON_TYPE_HANDGUN;
	if(weaponid >= 25 && weaponid <= 27)
		return WEAPON_TYPE_SHOTGUN;
	if(weaponid == 28 || weaponid == 29 || weaponid == 32)
		return WEAPON_TYPE_SMG;
	if(weaponid == 30 || weaponid == 31)
		return WEAPON_TYPE_ASSAULT_RIFLE;
	if(weaponid >= 35 && weaponid <= 38)
		return WEAPON_TYPE_LAUNCHER;
	return WEAPON_TYPE_MISC;
}

stock GetWeaponSlot(weaponid) {
	switch (weaponid)
	{
		case 2..9: return 1;
		case 10..15: return 10;
		case 16..18,39: return 8;
		case 22..24: return 2;
		case 25..27: return 3;
		case 28,29,32: return 4;
		case 30,31: return 5;
		case 33,34: return 6;
		case 35..38: return 7;
		case 41..43: return 9;
		case 44..46: return 11;
		default: return 0;
	} 
	return 0;
}
