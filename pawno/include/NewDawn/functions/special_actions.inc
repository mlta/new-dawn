/**
 * special_actions.inc - custom set of special actions.
 * @author gim <myvrenik@gmail.com>
 * 
 * This source code is public domain. You can relicense this code
 * under whatever license you want.
 * 
 * Functions:
 * SetPlayerSpecialActionEx(playerid, saction)
 * GetPlayerSpecialActionEx(playerid)
 * 
 */
#if defined __special_actions_inc_included
    #endinput
#endif
#define __special_actions_inc_included

#define SetPlayerHoldingObject(%1,%2,%3,%4,%5,%6,%7,%8,%9) SetPlayerAttachedObject(%1,MAX_PLAYER_ATTACHED_OBJECTS-1,%2,%3,%4,%5,%6,%7,%8,%9)
#define StopPlayerHoldingObject(%1) RemovePlayerAttachedObject(%1,MAX_PLAYER_ATTACHED_OBJECTS-1)
#define IsPlayerHoldingObject(%1) IsPlayerAttachedObjectSlotUsed(%1,MAX_PLAYER_ATTACHED_OBJECTS-1)

enum {
    /*
    Defined by a_samp.inc:
    SPECIAL_ACTION_DRINK_BEER
    SPECIAL_ACTION_SMOKE_CIGGY
    SPECIAL_ACTION_DRINK_WINE
    SPECIAL_ACTION_DRINK_SPRUNK
    */
    SPECIAL_ACTION_DRINK_BEER2,
    SPECIAL_ACTION_DRINK_BEER3,
    SPECIAL_ACTION_DRINK_WINE2,
    SPECIAL_ACTION_DRINK_WINE3,
    SPECIAL_ACTION_DRINK_VODKA
}

/**
 * Works similarry to default SetPlayerSpecialAction
 * 
 * @param int playerid:
 * @param int saction: Special action ID
 * 
 */
stock SetPlayerSpecialActionEx(playerid, saction)
{
    if(IsPlayerHoldingObject(playerid)) StopPlayerHoldingObject(playerid);
    switch(saction)
    {
        case SPECIAL_ACTION_SMOKE_CIGGY: SetPlayerHoldingObject(playerid, 3044, 6, 0.004882, 0.028505, -0.011920, 41.412254, 331.423767, 64.427078);
        case SPECIAL_ACTION_DRINK_BEER: SetPlayerHoldingObject(playerid, 1543, 6, 0.05, 0.03, -0.3, 0, 0, 0);
        case SPECIAL_ACTION_DRINK_BEER2: SetPlayerHoldingObject(playerid, 1950, 6, 0.05, 0.03, -0.1, 0, 0, 0);
        case SPECIAL_ACTION_DRINK_BEER3: SetPlayerHoldingObject(playerid, 1544, 6, 0.05, 0.03, -0.3, 0, 0, 0);
        case SPECIAL_ACTION_DRINK_WINE: SetPlayerHoldingObject(playerid, 1487, 6, 0.05, 0.03, -0.1, 0, 0, 0);
        case SPECIAL_ACTION_DRINK_WINE2: SetPlayerHoldingObject(playerid, 1512, 6, 0.05, 0.03, -0.1, 0, 0, 0);
        case SPECIAL_ACTION_DRINK_WINE3: SetPlayerHoldingObject(playerid, 1664, 6, 0.05, 0.03, -0.1, 0, 0, 0);
        case SPECIAL_ACTION_DRINK_VODKA: SetPlayerHoldingObject(playerid, 1668, 6, 0.05, 0.03, -0.1, 0, 0, 0);
    }
    SetPVarInt(playerid, "PlayerSpecialAction", saction);
    return 1;
}
/**
 * Works similarry to default GetPlayerSpecialAction
 * 
 * @param int playerid:
 * 
 */
stock GetPlayerSpecialActionEx(playerid) return GetPVarInt(playerid, "PlayerSpecialAction");

//----------------------------------------------------------------------
#define PRESSED(%0) \
        (((newkeys & (%0)) == (%0)) && ((oldkeys & (%0)) != (%0)))
hook OnPlayerKeyStateChange(playerid, newkeys, oldkeys) {
    if(PRESSED(KEY_LOOK_BEHIND) || PRESSED(KEY_FIRE))
    {
        new playerSpecialAction = GetPlayerSpecialActionEx(playerid);
        if(playerSpecialAction > 0 && GetPlayerState(playerid) == PLAYER_STATE_ONFOOT)
        {
            if(playerSpecialAction == SPECIAL_ACTION_SMOKE_CIGGY) ApplyAnimation(playerid, "GANGS", "drnkbr_prtl", 4.1,0,1,1,0,0);
            else
            {
                /*if(PlayerInfo[playerid][pSex] == SEX_MALE) ApplyAnimation(playerid,"BAR","dnk_stndM_loop",4.1,0,1,1,0,0);
                else ApplyAnimation(playerid,"BAR","dnk_stndF_loop",4.1,0,1,1,0,0);*/
                ApplyAnimation(playerid, "GANGS", "drnkbr_prtl", 4.1,0,1,1,0,0);
                new DrunkLevel = GetPlayerDrunkLevel(playerid);
                new NewDrunkLevel;
                if(DrunkLevel < 50000) NewDrunkLevel = DrunkLevel+1000;
                SetPlayerDrunkLevel(playerid, NewDrunkLevel);
            }
        }
    }
    else if(PRESSED(KEY_SPRINT))
    {
        StopPlayerHoldingObject(playerid);
        DeletePVar(playerid, "PlayerSpecialAction");
    }
}
