
#define HookCommand(%1,%2) CallRemoteFunction("OnPlayerCommandText", "ds", %1, %2)

/**
 * Kicks a player after a short while
 *
 * @param int playerid
 */
timer KickPlayer[500](playerid) {
	Kick(playerid);
	return 1;
}


/**
 * �������� ��������� ������� ����������� � ������������ ������� �� ��������� ���������, ������������ ��������� � ����������� ����
 * @status todo
 *
 * @param todo
 *
 */

stock SendLocalMessage(string[], Float:R, Float:X, Float:Y, Float:Z, parts, colors[], Interior=0, VWorld = 0) {
	new Float:dist, buff[2][OUTPUT], bool:doubled=false;
	FormatMessage(buff, "", string, "");
	doubled = strlen(buff[1])>0;
	foreach (Player, playerid) {
		dist = GetPlayerDistanceFromPoint(playerid, X,Y,Z);
		if(dist > R || VWorld != GetPlayerVirtualWorld(playerid) || Interior != GetPlayerInterior(playerid)) continue;
		SendClientMessage(playerid, colors[floatround(dist/R*parts, floatround_floor)], buff[0]);
		if(doubled) {
			SendClientMessage(playerid, colors[floatround(dist/R*parts, floatround_floor)], buff[1]);
		}
	} 
	return;
}


/**
 * �������� ��������� ������� ����������� � ������������ ������� �� ��������� ���������, ������������ ��������� � ����������� ����
 * @status todo
 *
 * @param todo
 *
 */

stock SendLocalMessageEx(beginning[], string[], ending[], Float:R, Float:X, Float:Y, Float:Z, parts, colors[], Interior=0, VWorld = 0) {
	new Float:dist, buff[2][OUTPUT], bool:doubled=false;
	FormatMessage(buff, beginning, string, ending);
	doubled = strlen(buff[1])>0;
	foreach (Player, playerid) {
		dist = GetPlayerDistanceFromPoint(playerid, X,Y,Z);
		if(dist > R || VWorld != GetPlayerVirtualWorld(playerid) || Interior != GetPlayerInterior(playerid)) continue;
		SendClientMessage(playerid, colors[floatround(dist/R*parts, floatround_floor)], buff[0]);
		if(doubled) {
			SendClientMessage(playerid, colors[floatround(dist/R*parts, floatround_floor)], buff[1]);
		}
	}
	return;
}

stock SendClientMessageEx(playerid, color, start[], text[], ending[]) {
	new buff[2][OUTPUT], bool:doubled=false;
	FormatMessage(buff, start, text, ending);
	doubled = strlen(buff[1])>0;
	SendClientMessage(playerid, color, buff[0]);
	if(doubled) {
		SendClientMessage(playerid, color, buff[1]);
	}
	return 1;
}

stock SendClientMessageToAllEx(color, start[], text[], ending[]) {
	new buff[2][OUTPUT], bool:doubled=false;
	FormatMessage(buff, start, text, ending);
	doubled = strlen(buff[1])>0;
	SendClientMessageToAll(color, buff[0]);
	if(doubled) {
		SendClientMessageToAll(color, buff[1]);
	}
	return 1;
}

stock SendPlayerMessageThroughTheDoor(playerid, Float:Rad, string:string[], stringSize) {
    // ����� >> ��������
    new Property = GetPlayerNearProperty(playerid, Rad, true, false);
    if(Property != -1) {
        format(string, stringSize, "[Behind the door] %s");
        SendLocalMessageEx(string, "", "", Rad, HouseInts[PropertyInfo[Property][prInt]][piPos][0], HouseInts[PropertyInfo[Property][prInt]][piPos][1], HouseInts[PropertyInfo[Property][prInt]][piPos][2], 5, {COLOR_FADE1, COLOR_FADE2, COLOR_FADE3, COLOR_FADE4, COLOR_FADE5}, HouseInts[PropertyInfo[Property][prInt]][piInt], PropertyInfo[Property][prWorld]);
    }
    // �������� >> �����
    else {
        Property = GetPlayerInProperty(playerid);
        if(Property != -1 && IsPlayerInRangeOfPoint(playerid, Rad, HouseInts[PropertyInfo[Property][prInt]][piPos][0], HouseInts[PropertyInfo[Property][prInt]][piPos][1], HouseInts[PropertyInfo[Property][prInt]][piPos][2])) {
            format(string, stringSize, "[Behind the door] %s");
            SendLocalMessageEx(string, "", "", Rad, PropertyInfo[Property][prEnter][0], PropertyInfo[Property][prEnter][1], PropertyInfo[Property][prEnter][2], 5, {COLOR_FADE1, COLOR_FADE2, COLOR_FADE3, COLOR_FADE4, COLOR_FADE5}, 0, P_MIN_VWORLD);
        }
    }
    return 1;
}

/**
 * ���������� ��� ������ � ����� �� ���� �����
 *
 * @param int id ������
 * @param bool ��� ��������
 *
 * @return string ������ � ������ ������
 *
 */

stock GetPlayerNameEx(playerid, stylized=true) {
	new string[MAX_PLAYER_NAME];
	GetPlayerName(playerid, string, sizeof(string));
	if(stylized)
		for(new i=0;i<MAX_PLAYER_NAME;i++)
			if(string[i]=='_') string[i] = ' ';
	return string;
}


/* ============= POSITION-RELATED FUNCTIONS ========*/

stock MovePlayer(playerid, Float:PosX, Float:PosY, Float:PosZ, Float:Angle=0.0, Interior=-1, VirtualWorld=-1) {
	SetPlayerPos(playerid, PosX, PosY, PosZ);
	SetPlayerFacingAngle(playerid, Angle);
	if(Interior >= 0) SetPlayerInterior(playerid, Interior);
	if(VirtualWorld >= 0) SetPlayerVirtualWorld(playerid, VirtualWorld);
	SetCameraBehindPlayer(playerid);
	return 1;
}

stock Float:GetDistance(playerid, player) {
	new Float:x, Float:y, Float:z;
	GetPlayerPos(player, x,y,z);
	return GetPlayerDistanceFromPoint(playerid, x,y,z);
}

/**
 * Function is used to get distance between a player and a vehicle
 *
 * @param int player id
 * @param int vehicle id
 * @return float distance
 */

stock Float:PlayerToVehicle(playerid, vehicle) {
	new Float:x, Float:y, Float:z;
	GetVehiclePos(vehicle, x, y, z);
	return GetPlayerDistanceFromPoint(playerid, x, y, z);
}

/**
 * ���������� ��������� � ������ ������
 *
 * @param int ID ������
 * @returns int ID ������ (0 ���� �� �������)
 */

stock GetClosestVehicle(playerid, Float:distance=0.0) {
	new Float:dist=distance, Float:tmpdist, car=0, 
		Float:x, Float:y, Float:z;
	GetPlayerPos(playerid, x, y, z);
	
	foreach(Vehicle, vehicleid){
		tmpdist = GetVehicleDistanceFromPoint(vehicleid,x,y,z);
		if((tmpdist < dist || car == 0 && tmpdist < distance)) {
			dist = tmpdist;
			car = vehicleid;
		}
	}
	return car;
}


/* ============= PROFILE-RELATED FUNCTIONS =========== */

stock LoggedIn(playerid) {
	return GetPVarInt(playerid, "LoggedIn");
}

stock SetPlayerRace(playerid, race) {
	SetPVarInt(playerid, "Race", race);
	SavePlayer(playerid);
	new i=0;
	for(i=0;i<sizeof(MaleSkins);i++) {
		if(MaleSkins[i][1] != race || !GetPVarInt(playerid, "Sex")) 
			Class_SetPlayer(Classes[i], playerid, false);
		else 	
			Class_SetPlayer(Classes[i], playerid, true);
	}
	for(new k=0;k<sizeof(FemaleSkins);k++) {
		if(FemaleSkins[k][1] != race || GetPVarInt(playerid, "Sex") == 1) 
			Class_SetPlayer(Classes[i], playerid, false);
		else 	
			Class_SetPlayer(Classes[i+k], playerid, true);
	}
}

stock SavePlayer(playerid) {
	if(!LoggedIn(playerid)) return false;
	new string[2048], str[256], buff[256];
	format(string, sizeof(string), "UPDATE `characters` SET ");
	for(new i=0;i<sizeof(SavedParams);i++) {
	    if(SavedParams[i][epType] == 3)
		    format(string, sizeof(string), "%s`%s`='%d',", string, SavedParams[i][epName], GetPVarInt(playerid, SavedParams[i][epName]));
		else if(SavedParams[i][epType] == 2)
			format(string, sizeof(string), "%s`%s`='%f',", string, SavedParams[i][epName], GetPVarFloat(playerid, SavedParams[i][epName]));
		else if(SavedParams[i][epType] == 1) {
			if(GetPVarString(playerid, SavedParams[i][epName], str, sizeof(str)) != 0) {
				mysql_real_escape_string(str, buff, MySQL);
				format(string, sizeof(string), "%s`%s`='%s',", string, SavedParams[i][epName], buff);
			}
			else {
				format(string, sizeof(string), "%s`%s`='',", string, SavedParams[i][epName]);
			}
		}
	}
	strmid(string, string, 0, strlen(string)-1);
	mysql_format(MySQL, string, "%s WHERE `id`='%d'", string, GetPVarInt(playerid, "ID"));
	mysql_query(string, MySQL);

	return true;
}



/**
 * Initializes a countdown and triggers a callback (playerid, bool:cancelled)
 * Name of callback is passed through argument.
 *
 * @param int player id
 * @param int seconds to pass
 * @param string callback name
 * @returns 0 if player is not connected or already has a countdown on him, 1 otherwise
 */
 
stock Player_InitializeCountdown(playerid, seconds, callback[]) {
	if(Group_GetPlayer(gCountdown, playerid) || !LoggedIn(playerid)) {
		return 0;
	}
	SetPVarInt(playerid, "CountdownTime", seconds);
	SetPVarString(playerid, "CountdownCallback", callback);
	Group_SetPlayer(gCountdown, playerid, true);
	return 1;
}

/**
 * Stops an initialized countdown
 */

stock Player_FinishCountdown(playerid) {
	if(Group_GetPlayer(gCountdown, playerid)) {
		new callback[32];
		GetPVarString(playerid, "CountdownCallback", callback, sizeof(callback));
		CallLocalFunction(callback, "dd", playerid, false);
		DeletePVar(playerid, "CountdownCallback");
		Group_SetPlayer(gCountdown, playerid, false);
	}
	return 1;
}

