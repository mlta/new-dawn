#include <YSI\y_hooks>

#define MAX_MAPS 30
#define MAX_MAP_ITEM 100
#pragma unused MapObjects
forward LoadMap(query[], id, extraid, connectionHandle);


new MapGroups[MAX_MAPS][24];

enum mapObject {
	moID,
	moModel,
	Float:moPosX,
	Float:moPosY,
	Float:moPosZ,
	Float:moRotX,
	Float:moRotY,
	Float:moRotZ,
	moInterior,
	moVirtualWorld,
	Float:moMovedPosX,
	Float:moMovedPosY,
	Float:moMovedPosZ,
	Float:moMovedRotX,
	Float:moMovedRotY,
	Float:moMovedRotZ
};
new MapObjects[MAX_MAPS][MAX_MAP_ITEM];
new Iterator:MapGroup[MAX_MAPS]<MAX_MAP_ITEM>;

stock LoadMapGroup(name[]) {
	if(Iter_Count(MapGroup) == MAX_MAPS) {
		return false;
	}
	new id=-1, query[OUTPUT];
	for(new i=0;i<MAX_MAPS;i++) {
		if(!Iter_Contains(MapGroup, i)) {
			id = i;
			strmid(MapGroups[i], name, 0, sizeof(MapGroups[i]));
			break;
		}
	}
	if(id == -1) {
		return false;
	}
	mysql_format(query, mysql, "SELECT * FROM `maps` WHERE `group`='%s' LIMIT %d", query, MAX_MAP_ITEM);
	mysql_query_callback(id, query, "LoadMap", 0, MySQL, true);
	return true;

}

public LoadMap(query[], id, extraid, connectionHandle) {
	new rows=0, fields=0;
	cache_get_data(rows, fields, connectionHandle);
	if(!rows) {
	    return 1;
	}
	for(new i=0;i<rows;i++) {
//		Iterator_Add(MaxMaps,	CreateDynamicObject(
	}
	return 1;
}


hook OnGameModeInit() {
	Iter_Init(MapGroup);
}