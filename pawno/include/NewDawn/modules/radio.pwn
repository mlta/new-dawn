
#if !defined radio_included || !defined vehicles_included
    #endinput
#endif

#include <YSI\y_text>

loadtext Core[Messages], Core[Errors], Core[Commands];

stock IsPlayerRadioStreamed(playerid) {
	new tmp[128];
	GetPVarString(playerid, "RadioStream", tmp, sizeof(tmp));
	return (strlen(tmp) > 0);
}

stock StopPlayerRadioStream(playerid) {
	StopAudioStreamForPlayer(playerid);
	DeletePVar(playerid, "RadioStream");
	return 1;
}

stock StreamRadioToPlayer(playerid, radio)  {
	if(radio < 0 || radio >= MAX_RADIO)
		return 0;
	SetPVarString(playerid, "RadioStream", RadioInfo[radio][rsURL]);
	PlayAudioStreamForPlayer(playerid, RadioInfo[radio][rsURL]);
	return 1;
}

forward RadioTuningDialog(playerid, dialogid, response, listitem, string:inputtext[]);
public RadioTuningDialog(playerid, dialogid, response, listitem, string:inputtext[]) {
	if(!response) {
		return 1;
	}
	new veh = GetPlayerVehicleID(playerid);
	if(Vehicles[veh][vTunedRadio] != 0) {
		listitem --;
	}
	if(listitem == -1) { // in case radio is streamed
		foreach(new ply : Player) {
			if(IsPlayerInVehicle(ply, veh)) {
				StopPlayerRadioStream(ply);
			}
		}
		Vehicles[veh][vTunedRadio] = 0;
		return 1;
	}	
	Vehicles[veh][vTunedRadio] = listitem+1;
	
	foreach(new ply : Player) {
		if(IsPlayerInVehicle(ply, veh)) {
			StreamRadioToPlayer(ply, listitem);
		}
	}
	return 1;
}



hook OnPlayerStateChange(playerid, newstate, oldstate) {
	if(newstate == PLAYER_STATE_DRIVER || newstate == PLAYER_STATE_PASSENGER) {
		new veh = GetPlayerVehicleID(playerid);
		if(Vehicles[veh][vTunedRadio] != 0) {
			StreamRadioToPlayer(playerid, Vehicles[veh][vTunedRadio]-1);
		}
	}
	else if(oldstate == PLAYER_STATE_DRIVER || oldstate == PLAYER_STATE_PASSENGER) {
		StopPlayerRadioStream(playerid);
	}
}

YCMD:tune(playerid, params[], help) {
	new seat = GetPlayerVehicleSeat(playerid), 
		veh = GetPlayerVehicleID(playerid);
	if(seat != 1 && seat != 0) {
		Text_Send(playerid, $STATE_IN_VEHICLE);
		return 1;
	}
	new string[2048]; // max dialog size
	if(Vehicles[veh][vTunedRadio] != 0) {
		strcat(string, "Turn OFF\n");
	}
	for(new i=0;i<MAX_RADIO;i++) {
		strcat(string, RadioInfo[i][rsName]);
		strcat(string, "\n");
	}
	Dialog_ShowCallback(playerid, using callback RadioTuningDialog, DIALOG_STYLE_LIST, "Select radio station", string, "Select", "Cancel");
	return 1;
}