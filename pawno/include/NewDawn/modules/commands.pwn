
#include <YSI\y_text>


loadtext Core[Chats], Core[Commands], Core[Errors], Core[Messages];



public OnPlayerCommandPerformed(playerid, cmdtext[], success) {
    if(!success) { 
		Text_Send(playerid, $COMMAND_ERROR, cmdtext);
		return 1;
	}
    return false;
}


YCMD:low(playerid, params[], help) {
	if(help) return Text_Send(playerid, $COMMAND_HELP, YCMD:low, "send a message to player very nearby you in low In-Character chat");
	if(isnull(params)) return Text_Send(playerid, $COMMAND_SYNTAX, YCMD:low, "<text>");
	
	new string[OUTPUT], Float:x, Float:y, Float:z;
	format(string, sizeof(string), "%s whispers:", GetPlayerNameEx(playerid));
	GetPlayerPos(playerid, x,y,z);
	SendLocalMessageEx(string, params, "", 5.0, x,y,z, 5, {COLOR_FADE1, COLOR_FADE2, COLOR_FADE3, COLOR_FADE4, COLOR_FADE5}, GetPlayerInterior(playerid), GetPlayerVirtualWorld(playerid));
	return true;
}

YCMD:say(playerid, params[], help) {
	if(help) return Text_Send(playerid, $COMMAND_HELP, YCMD:say, "send a message to players nearby you in main In-Character chat");
    if(isnull(params)) return Text_Send(playerid, $COMMAND_SYNTAX, YCMD:say, "<text>");

	new string[OUTPUT], Float:x, Float:y, Float:z;
	GetPlayerPos(playerid, x,y,z);
	if(!GetPVarString(playerid, "Description_Voice", string, sizeof(string))) {
		format(string, sizeof(string), "%s says:", GetPlayerNameEx(playerid));
	}
	else {
		format(string, sizeof(string), "%s says [%s]:", GetPlayerNameEx(playerid), string);
	}
	SendLocalMessageEx(string, params, "", 15.0, x,y,z, 5, {COLOR_FADE1, COLOR_FADE2, COLOR_FADE3, COLOR_FADE4, COLOR_FADE5}, GetPlayerInterior(playerid), GetPlayerVirtualWorld(playerid));
    format(string, sizeof(string), "%s %s", string, params);
    SendPlayerMessageThroughTheDoor(playerid, 3.0, string, sizeof(string));
	return true;
}

YCMD:shout(playerid, params[], help) {
	if(help) return Text_Send(playerid, $COMMAND_HELP, YCMD:shout, "send a message to players nearby you in shout In-Character chat");
	if(isnull(params)) return Text_Send(playerid, $COMMAND_SYNTAX, YCMD:shout, "<text>");

	new string[OUTPUT], Float:x, Float:y, Float:z;
	GetPlayerPos(playerid, x,y,z);
	format(string, sizeof(string), "%s shouts:", GetPlayerNameEx(playerid));
	SendLocalMessageEx(string, params, "", 30.0, x,y,z, 5, {COLOR_FADE1, COLOR_FADE2, COLOR_FADE3, COLOR_FADE4, COLOR_FADE5}, GetPlayerInterior(playerid), GetPlayerVirtualWorld(playerid));
    format(string, sizeof(string), "%s %s", string, params);
    SendPlayerMessageThroughTheDoor(playerid, 20.0, string, sizeof(string));
	return true;
}

YCMD:whisper(playerid, params[], help) {
	if(help) return Text_Send(playerid, $COMMAND_HELP, YCMD:whisper, "send a message to specified player in whisper In-Character chat");
    new string[OUTPUT], text[OUTPUT], partplayerid = INVALID_PLAYER_ID;
    if(sscanf(params, "us[OUTPUT]", partplayerid, text)) return Text_Send(playerid, $COMMAND_SYNTAX, YCMD:whisper, "<player id> <text>");
    if(!IsPlayerConnected(partplayerid)) return Text_Send(playerid, $TARGET_NOT_LOGGED);
	if(partplayerid == playerid) return Text_Send(playerid, $PLAYER_CMD_SELF);

	format(string, sizeof(string), "%s whispers: %s", GetPlayerNameEx(playerid), text);
	SendClientMessage(partplayerid, COLOR_FADE1, string);
	SendClientMessage(playerid, COLOR_FADE1, string);
	format(string, sizeof(string), "/me whispers to %s", GetPlayerNameEx(partplayerid)); 
	HookCommand(playerid, string);
	return true;
}

YCMD:pm(playerid, params[], help) {
	if(help) return Text_Send(playerid, $COMMAND_HELP, YCMD:pm, "send a message to specified player in Out-Of-Character chat");
	new string[OUTPUT], text[OUTPUT], partplayerid = INVALID_PLAYER_ID;
	if(sscanf(params, "us[120]", partplayerid, text)) return Text_Send(playerid, $COMMAND_SYNTAX, YCMD:pm, "<player id> <text>");
	if(!IsPlayerConnected(partplayerid)) return Text_Send(playerid, $TARGET_NOT_LOGGED);
	if(partplayerid == playerid) return Text_Send(playerid, $PLAYER_CMD_SELF);

	format(string, sizeof(string), "PM from %s to %s: %s", GetPlayerNameEx(playerid), GetPlayerNameEx(partplayerid), text);
	SendClientMessage(partplayerid, COLOR_YELLOW, string);
	SendClientMessage(playerid, COLOR_YELLOW, string);
	return true;
}

YCMD:me(playerid, params[], help) {
	if(help) return Text_Send(playerid, $COMMAND_HELP, YCMD:me, "send a message to players nearby you in action chat");
	if(isnull(params)) return Text_Send(playerid, $COMMAND_SYNTAX, YCMD:me, "<action>");

	new string[OUTPUT], Float:x, Float:y, Float:z;
	GetPlayerPos(playerid, x,y,z);
	format(string, sizeof(string), "* %s", GetPlayerNameEx(playerid));
	SendLocalMessageEx(string, params, "", 30.0, x,y,z, 1, {COLOR_ACTION}, GetPlayerInterior(playerid), GetPlayerVirtualWorld(playerid));
	return true;
}

YCMD:ame(playerid, params[], help) {
	if(help) return Text_Send(playerid, $COMMAND_HELP, YCMD:me, "send a message to players nearby you in action chat");
	if(isnull(params))  return Text_Send(playerid, $COMMAND_SYNTAX, YCMD:ame, "<action>");

	new string[OUTPUT], Float:x, Float:y, Float:z;
	GetPlayerPos(playerid, x,y,z);
	format(string, sizeof(string), "* %s", GetPlayerNameEx(playerid));
	SendLocalMessageEx(string, params, "", 5.0, x,y,z, 1, {COLOR_ACTION}, GetPlayerInterior(playerid), GetPlayerVirtualWorld(playerid));
	format(string, sizeof(string), "%s %s", string, params);
	SetPlayerChatBubble(playerid, string, COLOR_ACTION, 30.0, 10000);
	return true;
}



YCMD:b(playerid, params[], help) {
	if(help) return Text_Send(playerid, $COMMAND_HELP, YCMD:b, "send a message to local OOC chat");
	if(isnull(params)) return Text_Send(playerid, $COMMAND_SYNTAX, YCMD:b, "<OOC chat>");

	new string[OUTPUT], Float:x, Float:y, Float:z;
	GetPlayerPos(playerid, x,y,z);
	format(string, sizeof(string), "(( %s [%d]:", GetPlayerNameEx(playerid, false), playerid);
	SendLocalMessageEx(string, params, " ))", 15.0, x,y,z, 1, {COLOR_OOC}, GetPlayerInterior(playerid), GetPlayerVirtualWorld(playerid));
	return true;
}

YCMD:do(playerid, params[], help) {
	if(help) return Text_Send(playerid, $COMMAND_HELP, YCMD:do, "send a message to players nearby you in 3rd action chat");
	if(isnull(params)) return Text_Send(playerid, $COMMAND_SYNTAX, YCMD:do, "<action>");

	new string[144], Float:x, Float:y, Float:z;
	GetPlayerPos(playerid, x,y,z);
	format(string, sizeof(string), "(( %s ))", GetPlayerNameEx(playerid));
	SendLocalMessageEx("*", params, string, 30.0, x,y,z, 1, {COLOR_ACTION}, GetPlayerInterior(playerid), GetPlayerVirtualWorld(playerid));
	return true;
}


YCMD:stats(playerid, params[], help) {
	if(!LoggedIn(playerid)) return true;
	new name[MAX_PLAYER_NAME];
	GetPVarString(playerid, "Username", name, sizeof(name));
    Text_Send(playerid, $PLAYER_STAT, GetPlayerNameEx(playerid), name, GetPVarInt(playerid, "MinutesOn")/60);
	return true;
}

YCMD:vworld(playerid, params[], help)
{
	new string[50];
	format(string, sizeof(string), "������� ����������� ���: %d", GetPlayerVirtualWorld(playerid));
	SendClientMessage(playerid, 0xA9C4E400, string);
	return true;
}

YCMD:enter(playerid, params[], help) {
    if(!LoggedIn(playerid)) return true;
    new Property = GetPlayerNearProperty(playerid, 5.0, true, false);
    if(Property == -1) return SendClientMessage(playerid, COLOR_NOTIFICATION, "��� ������ ����������!");
    if(PropertyInfo[Property][prLocked]) return GameTextForPlayer(playerid, "~r~Locked", 2000, 1);
    OnPlayerEnterProperty(playerid, Property);
    return true;
}

YCMD:exit(playerid, params[], help) {
    if(!LoggedIn(playerid)) return true;
    new Property = GetPlayerNearProperty(playerid, 5.0, false, true);
    if(Property == -1) return SendClientMessage(playerid, COLOR_NOTIFICATION, "��� ������ ����������!");
    if(PropertyInfo[Property][prLocked]) return GameTextForPlayer(playerid, "~r~Locked", 2000, 1);
    OnPlayerExitProperty(playerid, Property);
    return true;
}

YCMD:lock(playerid, params[], help) {
    if(!LoggedIn(playerid)) return true;
    new Property = GetPlayerNearProperty(playerid, 5.0, true, false);
    if(Property == -1) Property = GetPlayerNearProperty(playerid, 5.0, false, true);
    if(Property == -1) return SendClientMessage(playerid, COLOR_NOTIFICATION, "��� ������ ����������!");
    if(PropertyInfo[Property][prOwnerID] != GetPVarInt(playerid, "ID")) return SendClientMessage(playerid, COLOR_NOTIFICATION, "� ��� ��� ����� �� ���� �����!");
    if(PropertyInfo[Property][prLocked]) {
        PropertyInfo[Property][prLocked] = false;
        GameTextForPlayer(playerid, "~w~Door ~g~Unlocked", 3000, 3);
    }
    else {
        PropertyInfo[Property][prLocked] = true;
        GameTextForPlayer(playerid, "~w~Door ~r~Locked", 3000, 3);
    }
    PlayerPlaySound(playerid, 1145, 0.0, 0.0, 0.0);
    return true;
}

YCMD:language(playerid, params[], help) {
	if(!strcmp(params, "ru", true, 2)) {
		Langs_SetPlayerLanguage(playerid, LRussian);
	}
	else {
		Langs_SetPlayerLanguage(playerid, LEnglish);
	}
	return 1;
}


YCMD:pay(playerid, params[], help) {
	new ply, sum, string[OUTPUT], name[MAX_PLAYER_NAME];
	if(sscanf(params, "ui", ply, sum)) { 
		Text_Send(playerid, $COMMAND_SYNTAX, YCMD:pay, "<playerid> <sum>");
		return 1;
	}
	if(!IsPlayerConnected(ply)) { 
		Text_Send(playerid, $TARGET_NOT_LOGGED);
		return 1;
	}
	if(sum < 0 || GetPlayerMoney(playerid) < sum) {
		Text_Send(playerid, $MONEY_INVALID); 
		return 1;
	}
	if(GetDistance(playerid, ply) > 7.5) {
		Text_Send(playerid, $PLAYER_IS_TOO_FAR);
		return 1;
	}
	GivePlayerMoney(playerid, -sum);
	GivePlayerMoney(ply, sum);
	name = GetPlayerNameEx(ply);
	format(string, sizeof(string), "/me takes out $%d and hands it to %s", sum, name);
	HookCommand(playerid, string);
	return 1;
}

YCMD:examine(playerid, params[], help) {
	new ply, string[OUTPUT];
	if(sscanf(params, "u", ply)) { 
		Text_Send(playerid, $COMMAND_SYNTAX, YCMD:examine, "<playerid>");
		return 1;
	}
	if(!IsPlayerConnected(ply)) { 
		Text_Send(playerid, $TARGET_NOT_LOGGED);
		return 1;
	}
	Text_Send(playerid, $DESCRIPTION_NAME, GetPlayerNameEx(ply));
	Text_Send(playerid, $DESCRIPTION_SEX, SexNames[GetPVarInt(ply, "Sex")]);
	if(GetPVarString(ply, "Description_Look", string, sizeof(string)) != 0) {
		Text_Send(playerid, $DESCRIPTION_LOOK, string);
	}
	if(GetPVarString(ply, "Description_Look2", string, sizeof(string)) != 0) {
		Text_Send(playerid, $DESCRIPTION_LOOK, string);
	}
	return 1;
}
YCMD:describe(playerid, params[], help) {
	new option[9], description[122];
	if(sscanf(params, "s[9]s[121]", option, description) || help == 2) {
		Text_Send(playerid, $COMMAND_SYNTAX, YCMD:describe, "<option> <description>");
		Text_Send(playerid, $COMMAND_ARGUMENTS, "option - look / look2 / voice");
		return 1;
	}
	if(!strcmp(option, "voice", true)) {
		if(strlen(description) > 20) {
			Text_Send(playerid, $DESCRIPTION_LENGTH, 20);
			return 1;
		}
		SetPVarString(playerid, "Description_Voice", description);
	}
	else if(!strcmp(option, "look", true)) {
		SetPVarString(playerid, "Description_Look", description);
	}
	else if(!strcmp(option, "look2", true)) {
		SetPVarString(playerid, "Description_Look2", description);
	}
	else {
		Command_ReProcess(playerid, "/describe", 2);
		return 1;
	}
	Text_Send(playerid, $DESCRIPTION_CHANGED);
	return 1;
}

YCMD:id(playerid, params[], help) {
	new name[MAX_PLAYER_NAME], username[MAX_PLAYER_NAME];
	if(is_integer(params, 10)) {
		new ply = strval(params);
		if(!LoggedIn(ply)) {
			Text_Send(playerid, $TARGET_NOT_LOGGED);
			return 1;
		}
		GetPlayerName(ply, name, sizeof(name));
		GetPVarString(ply, "Username", username, sizeof(username));
		Text_Send(playerid, $PLAYER_LIST, name, ply, username);
	}
	foreach(new ply : Player) {
		GetPlayerName(ply, name, sizeof(name));
		if(strfind(name, params, true) != -1) {
			GetPVarString(ply, "Username", username, sizeof(username));
			Text_Send(playerid, $PLAYER_LIST, name, ply, username);
		}
	}
	return 1;
}