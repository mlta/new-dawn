/**
 * factions_law.pwn
 *
 * Source code file for law enforcement factions
 * @author Moroz <moroz@malefics.com>
 * @version 1.0
 *
 * Commands:
 * @see YCMD:cuff - Access Level 0. Cuffs (item) required
 *
 */

#if !defined inventory_included || !defined factions_included
	#endinput
#endif

#include <YSI\y_text>

loadtext Core[Commands], Core[Errors], Items[Errors];


forward ActionCuff(playerid, ply, response);
forward ActionUncuff(playerid, ply, response);


public ActionCuff(playerid, ply, response) {
	if(!response) {
		return 0;
	}
	SetPlayerSpecialAction(ply, SPECIAL_ACTION_CUFFED);
	TakeItem(playerid, GetItemSlot(playerid, ITEM_CUFFS), 1);
	return 1;
}


public ActionUncuff(playerid, ply, response) {
	if(!response) {
		return 0;
	}
	SetPlayerSpecialAction(ply, SPECIAL_ACTION_NONE);
	GiveItem(playerid, ITEM_CUFFS, 1);
	return 1;
}

YCMD:cuff(playerid, params[], help) {
	new ply = INVALID_PLAYER_ID, string[OUTPUT], string2[OUTPUT],
		pFaction = GetPlayerFaction(playerid)-1, 
		pRank = GetPlayerRank(playerid)-1;
	if(sscanf(params, "u", ply)) {
		Text_Send(playerid, $COMMAND_SYNTAX, YCMD:cuff, "<playerid>");
		return 1;
	}
	if(!LoggedIn(ply)) {
		Text_Send(playerid, $TARGET_NOT_LOGGED);
		return 1;
	}
	if(GetItemSlot(playerid, ITEM_CUFFS) == -1) {
		Text_Send(playerid, $ITEM_REQUIRED, Items[ITEM_CUFFS][itemName]);
		return 1;
	}
	if(GetPlayerSpecialAction(ply) == SPECIAL_ACTION_CUFFED) {
		Text_Send(playerid, $PLAYER_CUFFED);
		return 1;
	}
	if(pFaction < 0 || FactionInfo[pFaction][fType] != 1 || FactionRankAccess[pFaction][pRank] < 1) {
		format(string, sizeof(string), "You are about to cuff %s (%d). Awaiting permission...", GetPlayerNameEx(ply), ply);
		format(string2, sizeof(string2), "{FFFFFF}%s (%d) wants to cuff you. Do you accept it?\n{FF0000}Note:{FFFFFF} you can get punished for refusing.", GetPlayerNameEx(playerid), playerid);
		CreateOffer(playerid, ply, string, string2, "ActionCuff");
	}
	else {
		ActionCuff(playerid, ply, 1);
	}
	return 1;
}



YCMD:uncuff(playerid, params[], help) {
	new ply = INVALID_PLAYER_ID, string[OUTPUT], string2[OUTPUT],
		pFaction = GetPlayerFaction(playerid)-1, 
		pRank = GetPlayerRank(playerid)-1;
	if(sscanf(params, "u", ply)) {
		Text_Send(playerid, $COMMAND_SYNTAX, YCMD:uncuff, "<playerid>");
		return 1;
	}
	if(!LoggedIn(ply)) {
		Text_Send(playerid, $TARGET_NOT_LOGGED);
		return 1;
	}
	if(GetPlayerSpecialAction(ply) != SPECIAL_ACTION_CUFFED) {
		Text_Send(playerid, $PLAYER_NOT_CUFFED);
		return 1;
	}
	if(pFaction < 0 || FactionInfo[pFaction][fType] != 1 || FactionRankAccess[pFaction][pRank] < 1) {
		format(string, sizeof(string), "You are about to uncuff %s (%d). Awaiting permission...", GetPlayerNameEx(ply), ply);
		format(string2, sizeof(string2), "{FFFFFF}%s (%d) wants to uncuff you. Do you accept it?\n{FF0000}Note:{FFFFFF} you can get punished for refusing.", GetPlayerNameEx(playerid), playerid);
		CreateOffer(playerid, ply, string, string2, "ActionUncuff");
	}
	else {
		ActionUncuff(playerid, ply, 1);
	}
	return 1;
}
