/**
 * admin.pwn
 *
 * Source file for admin system
 * @author Jonathan_Rosewood <jonathan-rosewood@yandex.ru>
 * @author Moroz <moroz@malefics.com>
 * @version 0.1
 *
 * Functiions:
 * @see SetAdminCommand(command[], level)
 * @see GetPlayerAdminLevel(playerid)
 *
 * Commands:
 * [A]@see YCMD:cmd
 * [A]@see YCMD:spec
 * [A]@see YCMD:goto
 * [A]@see YCMD:gethere
 * [A]@see YCMD:sethp
 * [A]@see YCMD:setarmor
 * [A]@see YCMD:givegun
 * [A]@see YCMD:veh
 * [A]@see YCMD:setskin
 * [A]@see YCMD:givemoney
 *
 */
 
#if !defined admin_included
	#endinput
#endif
 
#include <YSI\y_text>


loadtext Core[Chats], Core[Commands], Core[Errors];



/**
 * This macro does not really set an admin command, but it does check whether the player is an admin or not
 */
#define SetAdminCommand(%1,%2); \
	if(!IsAnAdmin(%1,%2)) { \ 
		Text_Send(%1, $ADMIN_LEVEL_MIN, %2); \
		return 1; \
	}




/**
 * Returns a player's admin level  
 *
 * @param int playerid
 * @return int
 */
stock GetPlayerAdminLevel(playerid) {
	return GetPVarInt(playerid, "AdminLevel");
}

/**
 * Tells whether the player is a functioning admin or not
 *
 * @param int playerid
 * @param int minimal admin level 
 * @return int/bool 
 */
stock IsAnAdmin(playerid, level) {
	return GetPlayerAdminLevel(playerid) >= level && GetPVarInt(playerid, "ADuty");
}

/**
 * Adds a record to the kick log
 * 
 * @param int Player ID
 * @param int Admin ID
 * @param const string reason
 * @param int Timestamp
 * @param const string IP 
 */

stock KickLog(playerid, adminid, const string:reason[], timestamp, const string:ip[]) {
	new query[256];
	mysql_format(MySQL, query, "INSERT INTO `log_kick` VALUES ( NULL, '%d', '%d', '%s', '%s', '%d')", playerid, adminid, reason, ip, timestamp);
	mysql_query(query, MySQL, false);
	return;
}

/**
 * Sets whether a player is on his admin duty or not
 *
 * @param int playerid
 * @param bool set
 */
stock SetPlayerAdminDuty(playerid, bool:set) {
	SetPVarInt(playerid, "ADuty", int:set);
	SetPlayerColor(playerid, (set) ? 0xff149300 : 0xFFFFFF00);
	return;
}


forward Dialog_Aduty(playerid, dialogid, response, listid, string:inputtext[]);
public Dialog_Aduty(playerid, dialogid, response, listid, string:inputtext[])  {
	new code[HASHSTR], buff[HASHSTR];
	if(!response) {
		return 1;
	}
	WP_Hash(buff, HASHSTR, inputtext);
	GetPVarString(playerid, "AdmCode", code, sizeof(code));
	if(strcmp(buff, code, true)) {
		BanEx(playerid, "Wrong admin code");
		return 1;
	}
	
	SetPlayerAdminDuty(playerid, true);
	return 1;
}

/* ASSISANT AND HIGHER (LEVEL 1+) */

YCMD:aduty(playerid, params[], help) {
	if(GetPlayerAdminLevel(playerid) < 1) {
		return 1;
	}
	if(IsAnAdmin(playerid, 1)) {
		SetPlayerAdminDuty(playerid, false);
		return 1;
	}
	Dialog_ShowCallback(playerid, using callback Dialog_Aduty, DIALOG_STYLE_PASSWORD, LANG_ADMIN_ADUTY_TITLE, LANG_ADMIN_ADUTY_CONTENT, ND_BUTTON_SUBMIT, ND_BUTTON_CANCEL);
	return 1;
}


/* MODERATOR AND HIGHER (LEVEL 2+) */

YCMD:kick(playerid, params[], help) {
	new reason[64], ply = INVALID_PLAYER_ID, ip[SIZE_IP], username[MAX_PLAYER_NAME]; // support for IPv6
	SetAdminCommand(playerid, ADMIN_LEVEL_MODERATOR);
	if(sscanf(params, "us[64]", ply, reason)) {
		Text_Send(playerid, $COMMAND_SYNTAX, YCMD:kick, "<playerid> <reason>");
		return 1;
	}
	if(!IsPlayerConnected(ply)) {
		Text_Send(playerid, $TARGET_NOT_ONLINE);
		return 1;
	}
	GetPlayerIp(ply, ip, sizeof(ip));
	GetPVarString(playerid, "Username", username, sizeof(username));
	KickLog(GetPVarInt(ply, "PlayerID"), GetPVarInt(playerid, "PlayerID"), reason, gettime(), ip);
	
	Text_Send(gPlayers, $PLAYER_LOG_KICKED, GetPlayerNameEx(ply, false), username, reason);
	
	Kick(ply);
	return 1;
}

YCMD:ban(playerid, params[], help) {
	
	new reason[64], ply = INVALID_PLAYER_ID, 
		username[MAX_PLAYER_NAME]; 
	SetAdminCommand(playerid, ADMIN_LEVEL_MODERATOR);
	if(sscanf(params, "us[64]", ply, reason)) {
		Text_Send(playerid, $COMMAND_SYNTAX, YCMD:ban, "<playerid> <reason>");
		return 1;
	}
	if(!IsPlayerConnected(ply)) {
		Text_Send(playerid, $TARGET_NOT_ONLINE);
		return 1;
	}
	GetPVarString(playerid, "Username", username, sizeof(username));
	foreach(new i : Player) {
		Text_Send(i, $PLAYER_LOG_BANNED, GetPlayerNameEx(ply, false), username, reason);
	}
	BanEx(ply, reason);
	return 1;
}


YCMD:aooc(playerid, params[], help) {
	new string[OUTPUT];
	SetAdminCommand(playerid, ADMIN_LEVEL_MODERATOR);
	if(isnull(params)) { 
		Text_Send(playerid, $COMMAND_SYNTAX, YCMD:aooc, "<message>");
		return 1;
	}
	GetPVarString(playerid, "Username", string, sizeof(string));
	format(string, sizeof(string), "[AOOC] %s:", string);
	SendClientMessageToAllEx(COLOR_NOTIFICATION, string, params, "");
	return 1;
}

/* ADMINISTRATOR AND HIGHER (LEVEL 3+) */

YCMD:tpto(playerid, params[], help)
{
    new Float:PosX, Float:PosY, Float:PosZ, string[OUTPUT];
	SetAdminCommand(playerid, ADMIN_LEVEL_REGULAR);
	if(sscanf(params, "P< ,>fff", PosX, PosY, PosZ)) {
		Text_Send(playerid, $COMMAND_HELP, YCMD:tpto, "<XYZ position>");
		return 1;
	}
    format(string, sizeof(string), "You have been teleported: %.3f,%.3f,%.3f", PosX, PosY, PosZ);
    SendClientMessage(playerid, X11_LIGHT_BLUE, string);
    if(IsPlayerInAnyVehicle(playerid)) SetVehiclePos(GetPlayerVehicleID(playerid), PosX, PosY, PosZ);
    else MovePlayer(playerid, PosX, PosY, PosZ);
	return 1;
}

YCMD:goto(playerid, params[], help) {
	new ply = INVALID_PLAYER_ID, Float:x, Float:y, Float:z;
	SetAdminCommand(playerid, ADMIN_LEVEL_REGULAR);
	if(sscanf(params, "u", ply)) { 
		Text_Send(playerid, $COMMAND_SYNTAX, YCMD:goto, "<playerid>");
		return 1;
	}
	if(!LoggedIn(ply)) { 
		Text_Send(playerid, $TARGET_NOT_LOGGED);
		return 1;
	}
	GetPlayerPos(ply, x,y,z);
	if(GetPlayerState(playerid) == PLAYER_STATE_DRIVER) {
		MoveVehicle(GetPlayerVehicleID(playerid), x,y,z, 0.0, GetPlayerInterior(ply), GetPlayerVirtualWorld(ply));
		SetPlayerInterior(playerid, GetPlayerInterior(ply));
		SetPlayerVirtualWorld(playerid, GetPlayerVirtualWorld(ply));
	}
	else {
		MovePlayer(playerid, x,y,z+1, 0.0, GetPlayerInterior(ply), GetPlayerVirtualWorld(ply));
	}
	return 1;
}

YCMD:gethere(playerid, params[], help) {
	new Float:x, Float:y, Float:z, ply = INVALID_PLAYER_ID;
	SetAdminCommand(playerid, ADMIN_LEVEL_REGULAR);
	if(sscanf(params, "u", ply)) { 
		Text_Send(playerid, $COMMAND_SYNTAX, YCMD:gethere, "<playerid>");
		return 1;
	}
	if(!LoggedIn(ply)) { 
		Text_Send(playerid, $TARGET_NOT_LOGGED);
	}
	GetPlayerPos(playerid, x,y,z);
	if(GetPlayerState(ply) == PLAYER_STATE_DRIVER) {
		MoveVehicle(GetPlayerVehicleID(ply), x,y,z, 0.0, GetPlayerInterior(playerid), GetPlayerVirtualWorld(playerid));
		SetPlayerInterior(ply, GetPlayerInterior(playerid));
		SetPlayerVirtualWorld(ply, GetPlayerVirtualWorld(playerid));
	}
	else MovePlayer(ply, x,y,z+1, 0.0, GetPlayerInterior(playerid), GetPlayerVirtualWorld(playerid));
	return 1;
}

YCMD:sethp(playerid, params[], help) {
	new Float:health, ply = INVALID_PLAYER_ID;
	SetAdminCommand(playerid, ADMIN_LEVEL_REGULAR);
	if(sscanf(params, "uf", ply, health)) { 
		Text_Send(playerid, $COMMAND_SYNTAX, YCMD:sethp, "<playerid> <amount>");
		return 1;
	}
	if(!LoggedIn(ply)) { 
		Text_Send(playerid, $TARGET_NOT_LOGGED);
		return 1;
	}
	SetPlayerHealth(ply, health);
	return 1;
}

YCMD:setarmor(playerid, params[], help) {
	new Float:armor, ply = INVALID_PLAYER_ID;
	SetAdminCommand(playerid, ADMIN_LEVEL_REGULAR);
	if(sscanf(params, "uI(100)", ply, armor)) { 
		Text_Send(playerid, $COMMAND_SYNTAX, YCMD:setarmor, "<playerid> <amount>");
		return 1;
	}
	if(!LoggedIn(ply)) {
		Text_Send(playerid, $TARGET_NOT_LOGGED);
		return 1;
	}
	if(armor > 100) return 1;
	SetPlayerArmour(ply, armor);
	return 1;
}




YCMD:setskin(playerid, params[], help) {
	new ply = INVALID_PLAYER_ID, skin;
	SetAdminCommand(playerid, ADMIN_LEVEL_REGULAR);
	if(sscanf(params, "ui", ply, skin)) { 
		Text_Send(playerid, $COMMAND_SYNTAX, YCMD:setskin, "<playerid> <skinid>");
		return 1;
	}
	if(!LoggedIn(ply)) {
		Text_Send(playerid, $TARGET_NOT_LOGGED);
		return 1;
	}
	if(skin < 0 || skin > 299) {
		return 1;
	}
	SetPlayerSkin(ply, skin);
	return 1;
}


YCMD:setint(playerid, params[], help) {
	SetAdminCommand(playerid, ADMIN_LEVEL_REGULAR);
	if(sscanf(params, "ui", params[0], params[1])) {
		Text_Send(playerid, $COMMAND_SYNTAX, YCMD:setint, "<playerid> <interior>");
		return 1;
	}
	if(!LoggedIn(params[0])) {
		Text_Send(playerid, $TARGET_NOT_LOGGED);
		return 1;
	}
    
    new string[100];
    SetPlayerInterior(playerid, params[1]);
    format(string, sizeof(string), "�� �������� ������ %s �������� �� %d.", GetPlayerNameEx(params[0]), params[1]);
    SendClientMessage(playerid, X11_LIGHT_BLUE, string);
    format(string, sizeof(string), "����� %s ������� ��� �������� �� %d.", GetPlayerNameEx(playerid), params[1]);
    SendClientMessage(params[0], X11_LIGHT_BLUE, string);
	return 1;
}

YCMD:setvw(playerid, params[], help) {
	SetAdminCommand(playerid, ADMIN_LEVEL_REGULAR);
	if(sscanf(params, "ui", params[0], params[1])) {
		Text_Send(playerid, $COMMAND_SYNTAX, YCMD:setvw, "<playerid> <VW>");
		return 1;
	}
	if(!LoggedIn(params[0])) {
		Text_Send(playerid, $TARGET_NOT_LOGGED);
		return 1;
	}
    
    new string[100];
    SetPlayerVirtualWorld(playerid, params[1]);
    format(string, sizeof(string), "�� �������� ������ %s ����������� ��� �� %d.", GetPlayerNameEx(params[0]), params[1]);
    SendClientMessage(playerid, X11_LIGHT_BLUE, string);
    format(string, sizeof(string), "����� %s ������� ��� ����������� ��� �� %d.", GetPlayerNameEx(playerid), params[1]);
    SendClientMessage(params[0], X11_LIGHT_BLUE, string);
	return 1;
}

YCMD:it(playerid, params[], help) {
	SetAdminCommand(playerid, ADMIN_LEVEL_REGULAR);
	if(isnull(params)) { 
		Text_Send(playerid, $COMMAND_SYNTAX, YCMD:it, "<action>");
		return 1;
	}
	SendClientMessageToAllEx(COLOR_ACTION, "*", params, "");
	return 1;
}

/* LEAD ADMINISTRATOR AND HIGHER (LEVEL 4+)*/


YCMD:veh(playerid, params[], help) {
	new model, color, color2;
	SetAdminCommand(playerid, ADMIN_LEVEL_LEAD);
	if(sscanf(params, "iI(1)I(1)", model, color, color2)) { 
		Text_Send(playerid, $COMMAND_SYNTAX, YCMD:veh, "<model> <colorid> <colorid>");
		return 1;
	}
	if(model < 400 || model > 610) {
		return 1;
	}
	new Float:x, Float:y, Float:z, Float:fa, vehicle;
	GetPlayerPos(playerid, x,y,z);
	GetPlayerFacingAngle(playerid, fa);
	vehicle = CreateVehicle(model, x,y,z, fa, color, color2, 0);
	SetVehicleNumberPlate(vehicle, "SPAWNED");
	LinkVehicleToInterior(vehicle, GetPlayerInterior(playerid));
	SetVehicleVirtualWorld(vehicle, GetPlayerVirtualWorld(playerid));
	PutPlayerInVehicle(playerid, vehicle, 0);
	Vehicles[vehicle][vOwner] = INVALID_PLAYER_ID;
	SetVehicleParamsEx(vehicle, 1, 0, 0, 0, 0, 0, 0);
	return 1;
}


YCMD:givemoney(playerid, params[], help) {
	new ply = INVALID_PLAYER_ID, money;
	SetAdminCommand(playerid, ADMIN_LEVEL_LEAD);
	if(sscanf(params, "ui", ply, money)) {
		Text_Send(playerid, $COMMAND_SYNTAX, YCMD:givemoney, "<playerid> <amount>");
		return 1;
	}
	if(!LoggedIn(ply)) {
		Text_Send(playerid, $TARGET_NOT_LOGGED);
		return 1;
	}
	GivePlayerMoney(ply, money);
	return 1;
}
