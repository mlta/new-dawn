/**
 * Executive file of phone system
 *
 * @version 0.1 
 * @author Moroz <moroz@malefics.com
 */
#if !defined inventory_included
	#endinput
#endif 

#include <YSI\y_text>


loadtext Core[Chats], Core[Commands], Core[Errors], Core[Messages];

YCMD:call(playerid, params[], help) {
	new phoneModel, phoneNumber, targetPhone;
	GetEquippedItemInfo(playerid, EQUIP_SLOT_PHONE, phoneModel, targetPhone, phoneNumber);
	if(phoneModel == 0) {
		Text_Send(playerid, $YOU_HAVE_NO_PHONE);
		return 1;
	}
	sscanf(params, "I(0)", targetPhone);
	if(targetPhone == 0) {
		Text_Send(playerid, $COMMAND_SYNTAX, YCMD:call, "<number>");
		return 1;
	}
	foreach(new ply : Player) {
		if(GetPVarInt(ply, "PhoneNumber") == targetPhone) {
			if(GetPVarInt(ply, "OnPhoneWith")) {
				Text_Send(playerid, $PHONE_IS_BUSY);
				return 1;
			}
			SetPVarInt(playerid, "OnPhoneWith", ply+1);
			SetPVarInt(ply, "OnPhoneWith", -playerid-1);
		}
	}
	return 1;
}

// pick ups the phone call
YCMD:p(playerid, params[], help) {
	new onPhoneWith = -GetPVarInt(playerid, "OnPhoneWith")-1;
	if(onPhoneWith < 0) {
		Text_Send(playerid, $PHONE_NO_CALL);
		return 1;
	}
	SetPVarInt(playerid, "OnPhoneWith", onPhoneWith);
	Text_Send(onPhoneWith, $PHONE_PICKED_UP);
	Command_ReProcess(playerid, "/me picks up the phone", 0);
	return 1;
}

// hangs up the phone
YCMD:h(playerid, params[], help) {
	new onPhoneWith = GetPVarInt(playerid, "OnPhoneWith");
	if(onPhoneWith < 0) {
		onPhoneWith = -onPhoneWith;
	}
	onPhoneWith -= 1;
	SetPVarInt(playerid, "OnPhoneWith", 0);
	SetPVarInt(onPhoneWith, "OnPhoneWith", 0);
	Text_Send(onPhoneWith, $PHONE_HUNG_UP);
	Command_ReProcess(playerid, "/me hangs up the phone", 0);
	return 1;
}