/** 
 * Source file for anti cheating system 
 *
 * @author Moroz <igorsmorozov@gmail.com>
 */


#if !defined anticheat_included
	#endinput
#endif


/**
 * Internal function: 
 * reports a hack
 *
 * @param int player id
 * @param int hack type
 */

stock ReportHack(playerid, HACK_TYPE:type) { 
	CallRemoteFunction("OnHackReported", "iii", playerid, int:type, gettime());
	return;
}

  
/* ======= ANTI-MONEY CHEAT ===========*/


stock GetPlayerMoney_AC(playerid) {
	return GetPVarInt(playerid, "Money");
}

#define GetPlayerMoney GetPlayerMoney_AC



stock GivePlayerMoney_AC(playerid, amount) {
	SetPVarInt(playerid, "Money", GetPlayerMoney(playerid)+amount);
	return GivePlayerMoney(playerid, amount);
}

#define GivePlayerMoney GivePlayerMoney_AC

stock ResetPlayerMoney_AC(playerid) {
	SetPVarInt(playerid, "Money", 0);
	return ResetPlayerMoney(playerid);
}

#define ResetPlayerMoney ResetPlayerMoney_AC

/* ====================================*/ 

hook OnPlayerUpdate(playerid) {
	new Float:x, Float:y, Float:z, Float:spd, 
		admin = IsAnAdmin(playerid, 1),
		pState = GetPlayerState(playerid);
	GetPlayerVelocity(playerid, x, y, z);
	
	spd = floatsqroot(x*x+y*y+z*z);
	
	if(spd > MAX_SPEED_ON_FOOT && pState == PLAYER_STATE_ONFOOT && GetPlayerSurfingVehicleID(playerid) == INVALID_VEHICLE_ID) {
		ReportHack(playerid, HACK_TYPE_AIRBRAKE);
		SetPlayerVelocity(playerid, 0.0, 0.0, 0.0);
	}
	if(GetPlayerSpecialAction(playerid) == SPECIAL_ACTION_USEJETPACK && !admin) {
		SetPlayerSpecialAction(playerid, SPECIAL_ACTION_NONE);
		ReportHack(playerid, HACK_TYPE_JETPACK);
	}
	return 1;
}