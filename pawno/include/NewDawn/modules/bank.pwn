/**
 * bank.pwn
 *
 * Source code file for banking system
 * @author Moroz <moroz@malefics.com>
 * @version 1.0
 *
 * Functions:
 * @see IsAtBank(playerid)
 * Commands:
 * @see YCMD:bank
 * @see YCMD:withdraw
 * @see YCMD:transfer
 *
 * REQUIREMENTS:
 * The script requires a BankPos array to be defined.
 * I recommend defining it in bank.pwn map
 */


#if defined bank_included
	#endinput
#endif
#define bank_included

#include <YSI\y_text>

loadtext Core[Messages], Core[Commands], Core[Errors];

/**
 * Returns whether the player is next to a bank or not
 */
stock IsAtBank(playerid) {
	for(new i=0;i<sizeof(BankPos);i++) {
		if(IsPlayerInRangeOfPoint(playerid, 2.0, BankPos[i][0], BankPos[i][1], BankPos[i][2])) {
			return 1;
		}
	}
	return 0;
}

YCMD:bank(playerid, params[], help) {
	if(!IsAtBank(playerid)) {
		return 1;
	}
	new sum = 0, bank = GetPVarInt(playerid, "Bank");
	if(sscanf(params, "i", sum)) {
		Text_Send(playerid, $COMMAND_SYNTAX, YCMD:bank, "<amount>");
		return 1;
	}
	if(sum < 1) {
		Text_Send(playerid, $MONEY_INVALID);
		return 1;
	}
	if(bank < sum) {
		Text_Send(playerid, $PLAYER_CANT_AFFORD);
		return 1;
	}
	SetPVarInt(playerid, "Bank", bank + sum);
	GivePlayerMoney(playerid, -sum);
	Text_Send(playerid, $BANK_DEPOSIT, sum, bank-sum);
	return 1;
}

YCMD:withdraw(playerid, params[], help) {
	if(!IsAtBank(playerid)) {
		return 1;
	}
	new sum = 0, bank = GetPVarInt(playerid, "Bank");
	if(sscanf(params, "i", sum)) {
		Text_Send(playerid, $COMMAND_SYNTAX, YCMD:withdraw, "<amount>");
		return 1;
	}
	if(sum < 1) {
		Text_Send(playerid, $MONEY_INVALID);
		return 1;
	}
	if(bank < sum) {
		Text_Send(playerid, $PLAYER_CANT_AFFORD);
		return 1;
	}
	SetPVarInt(playerid, "Bank", bank - sum);
	GivePlayerMoney(playerid, sum);
	Text_Send(playerid, $BANK_WITHDRAW, sum, bank-sum);
	return 1;
}

YCMD:transfer(playerid, params[], help) {
	if(!IsAtBank(playerid)) {
		return 1;
	}
	new sum = 0, ply = INVALID_PLAYER_ID, bank = GetPVarInt(playerid, "Bank"), bank2;
	if(sscanf(params, "ui", ply, sum)) {
		Text_Send(playerid, $COMMAND_SYNTAX, YCMD:transfer, "<amount>");
		return 1;
	}
	if(!LoggedIn(ply)) {
		Text_Send(playerid, $TARGET_NOT_LOGGED);
		return 1;
	}
	if(sum < 1) {
		Text_Send(playerid, $MONEY_INVALID);
		return 1;
	}
	if(bank < sum) {
		Text_Send(playerid, $PLAYER_CANT_AFFORD);
		return 1;
	}
	bank2 = GetPVarInt(ply, "Bank");
	SetPVarInt(playerid, "Bank", bank - sum);
	SetPVarInt(ply, "Bank", bank2 + sum);
	Text_Send(playerid, $BANK_TRANSFER, sum, GetPlayerNameEx(ply), bank-sum);
	Text_Send(playerid, $BANK_TRANSFER, sum, GetPlayerNameEx(playerid), bank2+sum);
	return 1;
}