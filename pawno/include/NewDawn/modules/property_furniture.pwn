#if !defined __property_inc_included || defined property_furniture_included
	#endinput
#endif
#define property_furniture_included

#include <YSI\y_text>

loadtext Core[Messages], Core[Errors], Property[Messages], Property[Errors];

stock UpdateFurniture(property, slot) {
	if(Furniture[property][slot][furnID] == 0) {
		return 0;
	}
	new string[256];
	format(string, sizeof(string), 
		"UPDATE `property_furniture` \
		SET `PosX`='%f', `PosY`='%f', `PosZ`='%f', `RotX`='%f', `RotY`='%f', `RotZ`='%f' WHERE `id`='%d'",
		Furniture[property][slot][furnPosX],
		Furniture[property][slot][furnPosY],
		Furniture[property][slot][furnPosZ],
		Furniture[property][slot][furnRotX],
		Furniture[property][slot][furnRotY],
		Furniture[property][slot][furnRotZ],
		Furniture[property][slot][furnID]
	);	
	mysql_query(string, MySQL);
	return 1;
}

stock CreateFurniture(property, slot, data[furnInfo]) {
	if(Furniture[property][slot][furnID] != 0) {
		return 0;
	}
	data[furnObject] = CreateDynamicObject(
		Items[data[furnItem]][itemModel], 
		data[furnPosX],
		data[furnPosY],
		data[furnPosZ],
		data[furnRotX],
		data[furnRotY],
		data[furnRotZ],
		data[furnVirtualWorld],
		data[furnInterior], 
		-1, 
		50.0
	);
	Furniture[property][slot] = data;
	Iter_Add(PropFurniture[property], slot);
	return data[furnObject];
}

stock SortFurniture(propety) {
	new tmp[furnInfo];
	for(new i=0;i<MAX_FURNITURE-1;i++) {
		if(Furniture[property][i][furnID] == 0) {
			Furniture[property][i] = Furniture[property][i+1];
			Furniture[property][i+1] = tmp;
			Iter_Remove(PropFurniture[property], i+1);
			Iter_Add(PropFurniture[property], i);
		}
	}
	return;
}

forward AddFurnitureCallback(query[], property, slot, connectionHandle);
public AddFurnitureCallback(query[], property, slot, connectionHandle) {
	Furniture[property][slot][furnID] = mysql_insert_id(connectionHandle);
	return 1;
}


stock AddFurniture(property, slot, data[furnInfo]) {
	if(Furniture[property][slot][furnID] != 0) {
		return 0;
	}
	new string[256];
	format(string, sizeof(string), 
		"INSERT INTO `property_furniture` \
		( `Property`, `Item`, `PosX`, `PosY`, `PosZ`, `RotX`, `RotY`, `RotZ`, `Interior`, `VirtualWorld`) \
		VALUES  \
		( '%d', '%d', '%f', '%f', '%f', '%f', '%f', '%f', '%d', '%d' )",
		PropertyInfo[property][prID],
		data[furnItem],
		data[furnPosX],
		data[furnPosY],
		data[furnPosZ],
		data[furnRotX],
		data[furnRotY],
		data[furnRotZ],
		data[furnInterior],
		data[furnVirtualWorld]
	);	
    mysql_query_callback(property, string, "AddFurnitureCallback", slot, MySQL);
	return CreateFurniture(property, slot, data);
}

stock AddFurnitureEx(property, data[furnInfo]) {
	for(new i=0;i<MAX_FURNITURE;i++) {
		if(Furniture[property][i][furnID] == 0) {
			return AddFurniture(property, i, data);
		}
	}
	return 0;
	
}

stock RemoveFurniture(property, slot) {
	if(Furniture[property][slot][furnID] == 0) {
		return 0;
	}
	new tmp[furnInfo];
	DeleteDynamicObject(Property[property][slot][furnObject]);
	Property[property][slot] = tmp;
	Iter_Remove(PropFurniture[property], slot);
}
stock DeleteFurniture(property, slot) {
	if(Furniture[property][slot][furnID] == 0) {
		return 0;
	}
	new string[64];
	format(string, sizeof(string), "DELETE FROM `property_furniture` WHERE `id`='%d'", Furniture[property][slot][furnID]);
    mysql_query(string, MySQL);
	RemoveFurniture(property, slot);
}

forward LoadPropertyFurniture(query[], property, extraid, connectionHandle);
public LoadPropertyFurniture(query[], property, extraid, connectionHandle) {
    new string[256], tmp[furnInfo], i = 0;
	mysql_store_result(connectionHandle);
	while(mysql_fetch_row_format(string, "|", connectionHandle) && i < MAX_FURNITURE) {
		sscanf(string, "p<|>e<iiiffffffii>", tmp);
		CreateFurniture(property-1, i, tmp);
		i++;
	}
	mysql_free_result(connectionHandle);
	return 1;
}



forward OnPlayerPurchaseFurniture(playerid, response, item);
public OnPlayerPurchaseFurniture(playerid, response, item) {
	if(response) {
		if(GetPlayerMoney(playerid) < Items[item][itemCost]) {
			Text_Send(playerid, $PLAYER_CANT_AFFORD);
			return 1;
		}
		new Float:a, data[furnInfo], obj;
		data[furnItem] = item;
		data[furnVirtualWorld] = GetPlayerVirtualWorld(playerid);
		data[furnInterior] = GetPlayerInterior(playerid);
		GetPlayerPos(playerid, data[furnPosX], data[furnPosY], data[furnPosZ]);
		GetPlayerFacingAngle(playerid, a);
		data[furnPosX] += 2.0*floatcos(a);
		data[furnPosY] += 2.0*floatsin(a);
		data[furnRotZ] = a+180;
		obj = AddFurnitureEx(GetPlayerInProperty(playerid), data);
		Streamer_Update(playerid);
		EditDynamicObject(playerid, obj);
		GivePlayerMoney(playerid, -Items[item][itemCost]);
		Text_Send(playerid, $ITEM_BOUGHT, Items[item][itemName], Items[item][itemCost]);
	}
	return 1;
}

public OnPlayerEditDynamicObject(playerid, objectid, response, Float:x, Float:y, Float:z, Float:rx, Float:ry, Float:rz) {
	if(response == 1) {
		new property = GetPlayerInProperty(playerid);
		if(property < 0) {
			return 1;
		}
		for(new slot=0;slot<MAX_FURNITURE;slot++) {
			if(Furniture[property][slot][furnObject] == objectid) {
				Furniture[property][slot][furnPosX] = x;
				Furniture[property][slot][furnPosY] = y;
				Furniture[property][slot][furnPosZ] = z;
				Furniture[property][slot][furnRotX] = rx;
				Furniture[property][slot][furnRotY] = ry;
				Furniture[property][slot][furnRotZ] = rz;
				UpdateFurniture(property, slot);
				break;
			}
		}
	}
	return 1;
}

public OnPlayerSelectDynamicObject(playerid, objectid, modelid, Float:x, Float:y, Float:z) {
	new property = GetPlayerInProperty(playerid);
	if(property < 0) {
		return 1;
	}
	if(PropertyInfo[property][prOwnerID] != GetPVarInt(playerid, "ID")) {
		return 1;
	}
	EditDynamicObject(playerid, objectid);
	return 1;
}


public OnPlayerModelSelectionEx(playerid, response, extraid, modelid, listitem) {
	if(extraid == 1) {
		if(!response) {
			Command_ReProcess(playerid, "/furniture", 0);
			return 1;
		}
		listitem += FIRST_FURNITURE_ITEM_INDEX;
		new string[256];
		format(string, sizeof(string), "Are you sure you want to buy {00EE00}%s{FFFFFF} for {55FF55}$%d{FFFFFF}?",
			Items[listitem][itemName], Items[listitem][itemCost]);
		CreateConfirmationDialog(playerid, string, "OnPlayerPurchaseFurniture", listitem);

		return 1;
	}
	return 1;
}



forward FurnitureDialog_Main(playerid, dialogid, response, listitem, string:inputtext[]);
public FurnitureDialog_Main(playerid, dialogid, response, listitem, string:inputtext[]) {
	if(!response) {
		return 1;
	}
	if(listitem == 0) { // purchase
		ShowItemSelection(playerid, FIRST_FURNITURE_ITEM_INDEX, FIRST_FURNITURE_ITEM_INDEX+FURNITURE_ITEM_COUNT-1, "Furniture items", 1, 1, 0.0, 0.0, 45.0, 0.95);
	}
	else if(listitem == 1) { //edit
		SelectObject(playerid);
		Text_Send(playerid, $EDIT_OBJECT);
	}
	return 1;
}


YCMD:furniture(playerid, cmdtext[], help) {
	new prop = GetPlayerInProperty(playerid);
	if(prop < 0 || PropertyInfo[prop][prOwnerID] != GetPVarInt(playerid, "ID")) {
		Text_Send(playerid, $NOT_IN_A_PROPERTY);
		return 1;
	}
    Dialog_ShowCallback(playerid, using callback FurnitureDialog_Main, DIALOG_STYLE_LIST, "[Furniture] Select an option", "Buy\nEdit", "Select", "Close");
	return 1;
}