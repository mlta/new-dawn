/**
 * inventory.pwn
 *
 * �������� ���� ������� ���������
 * @author Moroz <moroz@malefics.com>
 * @author gim
 * @version 1.0
 *
 *
 * �������:
 * @see YCMD:inv
 * @see YCMD:pickup
 * @see YCMD:giveitem
 * @see YCMD:printitems
 */
 
 
 #if !defined inventory_included
	#endinput
#endif


#include <YSI\y_text>

loadtext Items[Messages], Items[Errors], Core[Commands], Core[Errors];

/**
 * ������� �� ��������� ������ ������� ��� ������ slotid � ���������� N
 * 
 * @param int ID ������
 * @param int ����
 * @param int ���-�� ��������� (0 ��� ����, ����� ������ ���)
 */
 
stock TakeItem(playerid, itemslot, quantity=0) {
	if(itemslot < 0 || itemslot >= MAX_ITEMS) 
		return;
	new item, qu, serial;
	GetPlayerSlotInfo(playerid, itemslot, item, qu, serial);
	if(qu == 0 || item == 0) 
		return;
	SetPVarInt(playerid, InvQuantity[itemslot], qu-quantity);
	if(qu <= quantity || quantity == 0) {
		SetPVarInt(playerid, InvItem[itemslot], 0);
		SetPVarInt(playerid, InvQuantity[itemslot], 0);
		SetPVarInt(playerid, InvSerial[itemslot], 0);
	}
	SortPlayerInventory(playerid);
	return;
}
 
/**
 * ��������� ��������� ������.
 * 
 * @param int ID ������
 */

stock SortPlayerInventory(playerid) {
	if(!LoggedIn(playerid)) return;
	for(new i=0;i<MAX_ITEMS;i++) {
		if(!GetPVarInt(playerid, InvItem[i])) {
			if(GetPVarInt(playerid, InvQuantity[i]) == 0) {
				SetPVarInt(playerid, InvItem[i], 0);
				SetPVarInt(playerid, InvSerial[i], 0);
			}
			for(new j=i;j<MAX_ITEMS;j++) {
				if(GetPVarInt(playerid, InvItem[j])) {
					SetPVarInt(playerid, InvItem[i], GetPVarInt(playerid, InvItem[j]));
					SetPVarInt(playerid, InvQuantity[i], GetPVarInt(playerid, InvQuantity[j]));
					SetPVarInt(playerid, InvSerial[i], GetPVarInt(playerid, InvSerial[j]));
					SetPVarInt(playerid, InvItem[j], 0);
					SetPVarInt(playerid, InvQuantity[j], 0);
					break;
				}
			}
		}
	}
	return;
}

/**
 * ���������� ���� ������� � ��������� ������
 *
 * @param int ID ������
 * @param int ID �������� � ������
 * @param int �������� �����, �� ������� ���� ������
 * @return int ���� �������� ��� -1 ���� �� ������
 */

stock GetItemSlot(playerid, itemid, serialNumber=0) {
	if(!IsPlayerConnected(playerid) || !LoggedIn(playerid) || itemid < 0) 
		return -1;
	for(new i=0;i<MAX_ITEMS;i++) {
		if(GetPVarInt(playerid, InvItem[i]) == itemid && (GetPVarInt(playerid, InvSerial[i]) == serialNumber || serialNumber == 0)) 
			return i;
	}
	return -1;
}


/**
 * ������� ��� �������� � ������������ �����
 *
 * @param int ID ������
 * @param int ����, ��� �������� ���������
 * @return float ���
 */

stock Float:CalculateItemWeight(playerid, slot) {
	if(!IsPlayerConnected(playerid) || !LoggedIn(playerid) || slot < 0 || slot >= MAX_ITEMS) 
		return 0.0;
	return Items[GetPVarInt(playerid, InvItem[slot])][itemWeight] * GetPVarInt(playerid, InvQuantity[slot]);	
}

/**
 * ������� ������ ��� ������
 *
 * @param int ID ������
 * @return float ����� ���
 */

stock Float:CalculatePlayerWeight(playerid) {
	if(!IsPlayerConnected(playerid) || !LoggedIn(playerid)) 
		return 0.0;
	new Float:weight = 0.0;
	for(new i=0;i<MAX_ITEMS;i++) {
		weight += Items[GetPVarInt(playerid, InvItem[i])][itemWeight] 
			* GetPVarInt(playerid, InvQuantity[i]);
	}
	return weight;
}

/**
 * ���� ��������� ���� � ��������� ������
 *
 
 * @param int ID ������
 * @return int ��������� ���� ��� -1 ���� �� ������
 */

stock GetFreePlayerSlot(playerid) {
	if(!IsPlayerConnected(playerid) || !LoggedIn(playerid)) 
		return -1;
	for(new i=0;i<MAX_ITEMS;i++) {
		if(!GetPVarInt(playerid, InvItem[i])) 
			return i;
	}
	return -1;
}

/**
 * ������� ���������� ��������� ������ � ��������� ������
 *
 * @param int ID ������
 * @return int ���������� ��������� ������
 */
 
stock GetFreePlayerSlotsCount(playerid) {
	if(!IsPlayerConnected(playerid) || !LoggedIn(playerid)) 
		return 0;
	new count=0;
	for(new i=0;i<MAX_ITEMS;i++) {
		if(!GetPVarInt(playerid, InvItem[i])) 
			count++;
	}
	return count;
}

/**
 * ��������� ������ ������� � ���������
 *
 * @param int ID ������
 * @param int ID ��������
 * @param int ���������� 
 * @param int �������� ����� (�����������)
 * @return int -1 ���� ���-�� ����� �� ��� (��������, ��� ���� � ���������)
 */

stock GiveItem(playerid, item, quantity, serialNumber=0) {
	new replacementSlot = GetItemSlot(playerid, item, serialNumber);
	if(item < 0 || item >= sizeof(Items)) 
		return -1;
	if(quantity == 0 || item == 0) 
		return 0;
	
	if(replacementSlot == -1 || Items[item][itemType] == ITEM_TYPE_WEAPON)
		replacementSlot = GetFreePlayerSlot(playerid);
	if(replacementSlot == -1)
		return -1;
		
	SetPVarInt(playerid, InvItem[replacementSlot], item);
	SetPVarInt(playerid, InvQuantity[replacementSlot], GetPVarInt(playerid, InvQuantity[replacementSlot])+quantity);
	SetPVarInt(playerid, InvSerial[replacementSlot], serialNumber);
	SortPlayerInventory(playerid);
	return 0;
}

/**
 * ���������� ���������� �� ������������� ��������
 *  
 * @param int ID ������
 * @param int ���� ����������
 * @param int& ����������, ���� �������� ID ��������
 * @param int& ����������, ���� �������� ����������
 * @param int& ����������, ���� �������� �������� �����
 * @return -1 ���� ���� ��� ����������� ������, 0 � ������ ������
 */

stock GetEquippedItemInfo(playerid, eqSlot, &item, &quantity, &serial) {
	if(eqSlot < 0 || eqSlot > MAX_EQUIP_SLOT)
		return -1;
	item = GetPVarInt(playerid, EquipSlots[eqSlot][0]);
	quantity = GetPVarInt(playerid, EquipSlots[eqSlot][1]);
	serial = GetPVarInt(playerid, EquipSlots[eqSlot][2]);
	return 0;
}


/**
 * ��������� ������ ������������ ���������
 *
 * @param int ID ������
 * @param int ���� ���������� (0..MAX_EQUIP_SLOT-1)
 * @param int ID ��������
 * @param int ����������
 * @param int �������� ����� �������� (�����������)
 * @return int -1 ���� ����� �� �� ������� ��� ������� �� ����������, 
	0 � ������ ������.
 */

stock SetEquippedItem(playerid, eqSlot, item, quantity, serial=0) {
	if(!LoggedIn(playerid) || eqSlot < 0 || eqSlot >= MAX_EQUIP_SLOT || item < 0 || item >= sizeof(Items))
		return -1;
	SetPVarInt(playerid, EquipSlots[eqSlot][0], item);
	SetPVarInt(playerid, EquipSlots[eqSlot][1], quantity+GetPVarInt(playerid, EquipSlots[eqSlot][1]));
	SetPVarInt(playerid, EquipSlots[eqSlot][2], serial);
	
	if(Items[item][itemType] == ITEM_TYPE_WEAPON) {
		new weaponTypes:weaponType = GetWeaponType(Items[item][itemModel]);
		if(weaponType == WEAPON_TYPE_MELEE || weaponType == WEAPON_TYPE_EXPLOSIVE) {
			GivePlayerWeapon(playerid, Items[item][itemModel], 1);
		}
		else if(weaponType == WEAPON_TYPE_MISC) {
			GivePlayerWeapon(playerid, Items[item][itemModel], 0);
		}
		else {
			GivePlayerWeapon(playerid, Items[item][itemModel], quantity-1);
		}
	}
	return 0;
}


/**
 * ���� ������ ������� � ������������� ����� ���������
 *
 * @param int ID ������
 * @param int ���� �������� � ���������
 * @param int ���������� ��������
 * @return true ���� �������
 */
 
stock EquipItem(playerid, slot, amount) {
	if(slot < 0 || slot >= MAX_ITEMS) return false;
	new item, quantity, serial,
		string[OUTPUT];
	GetPlayerSlotInfo(playerid, slot, item, quantity, serial);
	if(amount > quantity) { 
		amount = quantity;
	}
	TakeItem(playerid, slot, amount);
	if(Items[item][itemType] != ITEM_TYPE_AMMO) {
		UnequipItem(playerid, Items[item][itemEQSlot]);
		if(amount > 1) {
			format(string, sizeof(string), "/me takes out %dx%s", amount, Items[item][itemName]);
		}
		else {
			format(string, sizeof(string), "/me takes out %s", Items[item][itemName]);
		}
	}
	else {
		new gun = 0, ammo, gserial;
		amount--;
		GetEquippedItemInfo(playerid, Items[item][itemEQSlot], gun, ammo, gserial); 
		if(gun == 0) {
			Text_Send(playerid, $NO_FIREARM_EQUIPPED);
			return false;
		}
		if(Items[gun][itemValue] != Items[item][itemID]) {
			Text_Send(playerid, $WEAPON_CALIBRE_DIFFERS);
			return false;
		}
		
		format(string, sizeof(string), "/me loads %d %s", amount, Items[item][itemName]);
		item = gun;
		serial = gserial;
	}
	
	SetEquippedItem(playerid, Items[item][itemEQSlot], item, amount, serial);
	Command_ReProcess(playerid, string, 0);
	SortPlayerInventory(playerid);
	return true;
}

/**
 * ���������� ���������� � �������� � ������������ �����
 *
 * @param int ID ������
 * @param int ���� ����������
 * @param int& ����������, ���� �������� ID ��������
 * @param int& ����������, ���� �������� ����������
 * @param int& ����������, ���� �������� �������� �����
 * @return -1 ���� ������ �������� ���� ��� ����� �� �����������, 0 � ������ �������
 */

stock GetPlayerSlotInfo(playerid, slot, &item, &quantity, &serial) {
	if(!LoggedIn(playerid) || slot < 0 || slot >= MAX_ITEMS)
		return -1;
	item = GetPVarInt(playerid, InvItem[slot]);
	quantity = GetPVarInt(playerid, InvQuantity[slot]);
	serial = GetPVarInt(playerid, InvSerial[slot]);
	return 0;
}

/**
 * �������� � ������ ������ �������
 *
 * @param int ID ������
 * @param int ���� �������� �������� (��. EquipSlots)
 */

stock UnequipItem(playerid, slot) {
	new item = GetPVarInt(playerid, EquipSlots[slot][0]), quantity = GetPVarInt(playerid, EquipSlots[slot][1]),
		serial = GetPVarInt(playerid, EquipSlots[slot][2]);
	if(item == 0) return;
	new Float:x, Float:y, Float:z, int = GetPlayerInterior(playerid), vw = GetPlayerVirtualWorld(playerid);
	GetPlayerPos(playerid, x, y, z);

	if(Items[item][itemType] == ITEM_TYPE_WEAPON) {
		new weapon, ammo;
		GetPlayerWeaponData(playerid, GetWeaponSlot(Items[item][itemModel]), weapon, ammo);
		GivePlayerWeapon(playerid, weapon, -ammo);
		if(ammo > 0 && Items[item][itemValue] != 0) {
			if(GiveItem(playerid, Items[item][itemValue], ammo) == -1)
				CreateDroppedItem(Items[item][itemValue], ammo, x, y, z, int, vw, GetPVarInt(playerid, "ID"));
		}
		quantity = 1;
	}
	else {
		if(quantity == 0 && item != 0) 
			quantity = 1;
	}	
	if(GiveItem(playerid, item, quantity, serial) == -1)
		CreateDroppedItem(item, quantity, x, y, z, int, vw, GetPVarInt(playerid, "ID"), serial);
	SetPVarInt(playerid, EquipSlots[slot][0], 0);
	SetPVarInt(playerid, EquipSlots[slot][1], 0);
	SetPVarInt(playerid, EquipSlots[slot][2], 0);
	SortPlayerInventory(playerid);
}

/**
 * ������� ��������� ������� �� �����
 *
 * @param int ID ��������
 * @param int ����������
 * @param float X, Y, Z ����������
 * @param int ��������
 * @param int ����������� ���
 * @param int UID ������, ����������
 * @param int �������� ����� ��������
 *
 * @return int ID ���������� ������� 
 */

stock CreateDroppedItem(item, quantity, Float:PosX, Float:PosY, Float:PosZ, interior=0, virtualWorld=0, DroppedBy=0, serialNumber=0) {
	new itemSlot = -1;
	for(new i=0;i<sizeof(DroppedItems);i++) {
		if(DroppedItems[i][diID] == 0) {
			itemSlot = i;
			break;
		}
	}
	if(itemSlot == -1 || quantity == 0)
		return -1;
	
	DroppedItems[itemSlot][diID] = item;
	DroppedItems[itemSlot][diQuantity] = quantity;
	DroppedItems[itemSlot][diDropTime] = gettime();
	DroppedItems[itemSlot][diDroppedBy] = DroppedBy;
	DroppedItems[itemSlot][diPosX] = PosX;
	DroppedItems[itemSlot][diPosY] = PosY;
	DroppedItems[itemSlot][diPosZ] = PosZ;
	DroppedItems[itemSlot][diVW] = virtualWorld;
	DroppedItems[itemSlot][diInt] = interior;
	DroppedItems[itemSlot][diSerial] = serialNumber;
	
	
	if(Items[item][itemType] == ITEM_TYPE_WEAPON && quantity == 1) 
		DroppedItems[itemSlot][diObject] = CreateDynamicObject(GunObjects[Items[item][itemModel]], DroppedItems[itemSlot][diPosX], DroppedItems[itemSlot][diPosY], DroppedItems[itemSlot][diPosZ]-1, 93.7, 120.0, random(360), DroppedItems[itemSlot][diVW], DroppedItems[itemSlot][diInt]);
	else if(Items[item][itemType] == ITEM_TYPE_WEAPON && quantity > 1)
		DroppedItems[itemSlot][diObject] = CreateDynamicObject(2969, DroppedItems[itemSlot][diPosX], DroppedItems[itemSlot][diPosY], DroppedItems[itemSlot][diPosZ]-0.9, 0.0, 0.0, random(360), DroppedItems[itemSlot][diVW], DroppedItems[itemSlot][diInt]);
	else if(quantity > 0)
		DroppedItems[itemSlot][diObject] = CreateDynamicObject(Items[item][itemModel], DroppedItems[itemSlot][diPosX], DroppedItems[itemSlot][diPosY], DroppedItems[itemSlot][diPosZ]-0.9, 0.0, 0.0, random(360), DroppedItems[itemSlot][diVW], DroppedItems[itemSlot][diInt]);
	foreach(Player, pid) {
		Streamer_Update(pid);
	}
	
	new string[256];
	format(string, sizeof(string), "INSERT INTO `droppeditems` VALUES ( NULL, '%d', '%d', '%d', '%d', '%f', '%f', '%f', '%d', '%d', '%d' );",
		item, quantity, DroppedItems[itemSlot][diDropTime], DroppedItems[itemSlot][diDroppedBy], 
		DroppedItems[itemSlot][diPosX], DroppedItems[itemSlot][diPosY], DroppedItems[itemSlot][diPosZ], 
		DroppedItems[itemSlot][diVW], DroppedItems[itemSlot][diInt], DroppedItems[itemSlot][diSerial]);
	mysql_query(string, MySQL);
	return itemSlot;
}

/**
 * ����������� ������� �� ��������� ������ �� �����
 *
 * @param int ID ������
 * @param int ���� ���������
 *
 * @return true ���� ������, false ���� ���-�� ����� �� ���
 */

stock DropItem(playerid, slot) {
	new item=0,quantity=0, serial=0, Float:x, Float:y, Float:z;

	item = GetPVarInt(playerid, InvItem[slot]);

	if(!item) 
		return !Text_Send(playerid, $PLAYER_DOESNT_HAVE_ITEM);

	quantity = GetPVarInt(playerid, InvQuantity[slot]);
	serial = GetPVarInt(playerid, InvSerial[slot]);
	
	SetPVarInt(playerid, InvItem[slot], 0);
	SetPVarInt(playerid, InvQuantity[slot], 0);
	SetPVarInt(playerid, InvSerial[slot], 0);

	GetPlayerPos(playerid, x,y,z);

	if(CreateDroppedItem(item, quantity, x, y, z, GetPlayerInterior(playerid), GetPlayerVirtualWorld(playerid), GetPVarInt(playerid, "ID"), serial) == -1) 
		return !Text_Send(playerid, $DROPPED_ITEMS_NO_SLOTS);

	return true;
}

public InventoryPickupDialog(playerid, dialogid, response, listitem, string:inputtext[]) {
    #pragma unused  dialogid, inputtext
	if(!response) return 1;
	new count = 0, idx=-1, str[64];
	
	for(new i=0;i<sizeof(DroppedItems);i++) {
		if(!IsPlayerInRangeOfPoint(playerid, 3.0, DroppedItems[i][diPosX], DroppedItems[i][diPosY], DroppedItems[i][diPosZ]) || 
			GetPlayerVirtualWorld(playerid) != DroppedItems[i][diVW] || GetPlayerInterior(playerid) != DroppedItems[i][diInt] || DroppedItems[i][diID] == 0) continue;
		if(count == listitem) { 
			idx = i;
			break;
		}
		count ++;
	}
	if(idx == -1) 
		return Command_ReProcess(playerid, "/pickup", 0);
	if(GetFreePlayerSlot(playerid) == -1 && GetItemSlot(playerid, DroppedItems[idx][diID]) == -1) {
		Text_Send(playerid, $INVENTORY_HAS_NO_ROOM);
		return Command_ReProcess(playerid, "/pickup", 0);
	}
	
	SetPVarInt(playerid, "CurrentInvItem", idx);
	SetPVarInt(playerid, "CurrentInvItemQuantity", 1);
	
	if(DroppedItems[idx][diQuantity] == 1 || Items[DroppedItems[idx][diID]][itemType] == ITEM_TYPE_WEAPON) {
		format(str, sizeof(str), "Are you sure you want to pick up %s?", Items[DroppedItems[idx][diID]][itemName]);
		return Dialog_ShowCallback(playerid, using callback EnsurePickingUp, DIALOG_STYLE_MSGBOX, "Inventory", str, "Yes", "No");
	}
	
	format(str, sizeof(str), "How many of %s do you want to pick up?", Items[DroppedItems[idx][diID]][itemName]);
	Dialog_ShowCallback(playerid, using callback EnterAmount, DIALOG_STYLE_INPUT, "Inventory", str, "Next", "Cancel");
	return 1;
}

public EnsurePickingUp(playerid, dialogid, response, listitem, string:inputtext[]) {
    #pragma unused dialogid, listitem, inputtext
	if(!response)
		return Command_ReProcess(playerid, "/pickup", 0);
	new item = GetPVarInt(playerid, "CurrentInvItem"), quantity = GetPVarInt(playerid, "CurrentInvItemQuantity"), 
		str[OUTPUT];
	if(DroppedItems[item][diID] == 0 || !IsPlayerInRangeOfPoint(playerid, 3.0, DroppedItems[item][diPosX], DroppedItems[item][diPosY], DroppedItems[item][diPosZ])
		|| GetPlayerVirtualWorld(playerid) != DroppedItems[item][diVW] || GetPlayerInterior(playerid) != DroppedItems[item][diInt]) {
		Text_Send(playerid, $ITEM_ALREADY_PICKED_UP);
		return Command_ReProcess(playerid, "/pickup", 0);
	}
	if(GiveItem(playerid, DroppedItems[item][diID], quantity, DroppedItems[item][diSerial]) == -1) {
		Text_Send(playerid, $INVENTORY_HAS_NO_ROOM);
		return Command_ReProcess(playerid, "/pickup", 0);
	}
	format(str, sizeof(str), "/me picks up %d %s", quantity, Items[DroppedItems[item][diID]][itemName]);
	Command_ReProcess(playerid, str, 0);
	DroppedItems[item][diQuantity] -= quantity;
	if(DroppedItems[item][diQuantity] <= 0) {
		DroppedItems[item][diID] = 0;
		DroppedItems[item][diQuantity] = 0;
		DestroyDynamicObject(DroppedItems[item][diObject]);
		format(str, sizeof(str), "DELETE FROM `droppeditems` WHERE `id`='%d'", item);
		mysql_query(str, MySQL);
	}
	else {
		format(str, sizeof(str), "UPDATE `droppeditems` SET `Quantity`='%d' WHERE `id`='%d'", DroppedItems[item][diQuantity], item);
		mysql_query(str, MySQL);
	}
	return 1;
}

public PlayerInventory(playerid, dialogid, response, listitem, string:inputtext[]) {
	if(!response) 
		return SetPVarInt(playerid, "CurrentInvItem", -1);
	#pragma unused dialogid, inputtext
	new title[64], id = listitem;
	format(title, sizeof(title), "Options for %d x %s", GetPVarInt(playerid, InvQuantity[id]), Items[GetPVarInt(playerid, InvItem[id])][itemName]); 
	SetPVarInt(playerid, "CurrentInvItem", id);
	Dialog_ShowCallback(playerid, using callback ItemAction, DIALOG_STYLE_LIST, title, "Equip/Use\nDrop", "Select", "Cancel");
	return 1;
}

public ItemAction(playerid, dialogid, response, listitem, string:inputtext[]) {
    #pragma unused playerid, dialogid, response, listitem, inputtext
	if(!response) 
		return Command_ReProcess(playerid, "/inv", 0);
	new slot = GetPVarInt(playerid, "CurrentInvItem"),
		item, quantity, serial;
	GetPlayerSlotInfo(playerid, slot, item, quantity, serial);
	if(listitem == 0) {
		new gun, ammo, gserial;
		GetEquippedItemInfo(playerid, Items[item][itemEQSlot], gun, ammo, gserial);
		switch(Items[item][itemType]) {
			case ITEM_TYPE_WEAPON: {
				EquipItem(playerid, slot, 1);
			}
			case ITEM_TYPE_AMMO: {
				EquipItem(playerid, slot, MagazineSize[Items[gun][itemModel]]+1);
			}
			case ITEM_TYPE_MISC: {
				EquipItem(playerid, slot, 1);
			}
		}
	}
	else if(listitem == 1)
		DropItem(playerid, slot);
	SortPlayerInventory(playerid);
	return 1;
}

public EnterAmount(playerid, dialogid, response, listitem, string:inputtext[]) {
    #pragma unused dialogid, listitem
	if(!response) 
		return Command_ReProcess(playerid, "/pickup", 0);
	new str[64], quantity;
	sscanf(inputtext, "i", quantity);
	if(quantity < 1) {
		Text_Send(playerid, $AMOUNT_MUST_BE_POSITIVE);
		return Command_ReProcess(playerid, "/pickup", 0);
	}
	if(quantity > DroppedItems[GetPVarInt(playerid, "CurrentInvItem")][diQuantity]) {
		Text_Send(playerid, $AMOUNT_IS_TOO_HIGH);
		return Command_ReProcess(playerid, "/pickup", 0);
	}
	SetPVarInt(playerid, "CurrentInvItemQuantity", quantity);
	format(str, sizeof(str), "Are you sure you want to pick up %d %s?", GetPVarInt(playerid, "CurrentInvItemQuantity"), Items[DroppedItems[GetPVarInt(playerid, "CurrentInvItem")][diID]][itemName]);
	Dialog_ShowCallback(playerid, using callback EnsurePickingUp, DIALOG_STYLE_MSGBOX, "Inventory", str, "Yes", "No");
	return 1;
}



forward Dialog_Unequip(playerid, dialogid, response, listitem, string:inputtext[]);
public Dialog_Unequip(playerid, dialogid, response, listitem, string:inputtext[]) {
	if(response) {
		UnequipItem(playerid, listitem);
		Command_ReProcess(playerid, "/equipment", 0);
	}
	return 1;
}

YCMD:equipment(playerid, params[], help) {
	new string[256],
		item, quantity, serial;
	for(new i=0;i<MAX_EQUIP_SLOT; i++) {
		GetEquippedItemInfo(playerid, i, item, quantity, serial);
		if(item > 0) {
			format(string, sizeof(string), "%sSlot %d: %s %d #%d\n", string, i, Items[item][itemName], quantity, serial);
		}
		else {
			format(string, sizeof(string), "%sSlot %d: empty\n", string, i);
		}
	}
	Dialog_ShowCallback(playerid, using callback Dialog_Unequip, DIALOG_STYLE_LIST, "Equipment", string, "Unequip", "Close");
	return 1;
}




YCMD:pickup(playerid, params[], help) {
	if(help) 
		return Text_Send(playerid, $COMMAND_HELP, YCMD:pickup, "pickup nearby items from the ground");
	new string[2048], count = 0;
	for(new i=0;i<sizeof(DroppedItems) && count < 20;i++) {
		if(!IsPlayerInRangeOfPoint(playerid, 3.0, DroppedItems[i][diPosX], DroppedItems[i][diPosY], DroppedItems[i][diPosZ]) 
			|| GetPlayerVirtualWorld(playerid) != DroppedItems[i][diVW] || GetPlayerInterior(playerid) != DroppedItems[i][diInt] || DroppedItems[i][diID] == 0) 
			continue;
		count ++;
		if(DroppedItems[i][diSerial])
			format(string, sizeof(string), "%s%24s\t[{00AA00}%3d{FFFFFF} ��., {00AA00}%.3f{FFFFFF}��, #%6d]\n", 
				string, Items[DroppedItems[i][diID]][itemName], DroppedItems[i][diQuantity], Items[DroppedItems[i][diID]][itemWeight]*DroppedItems[i][diQuantity], DroppedItems[i][diSerial]);
		else
			format(string, sizeof(string), "%s%24s\t[{00AA00}%3d{FFFFFF} ��., {00AA00}%.3f{FFFFFF}��]\n", 
				string, Items[DroppedItems[i][diID]][itemName], DroppedItems[i][diQuantity], Items[DroppedItems[i][diID]][itemWeight]*DroppedItems[i][diQuantity]);
	}
	if(strlen(string) == 0) 
		return Text_Send(playerid, $NO_ITEMS_TO_PICK_UP);
	strmid(string, string, 0, strlen(string)-1);
	
	
	
	Dialog_ShowCallback(playerid, using callback InventoryPickupDialog, DIALOG_STYLE_LIST, "Items you can pick up", string, "Pick up", "Cancel");
	
	return 1;
}

YCMD:giveitem(playerid, params[], help) {
	if(help) 
		return Text_Send(playerid, $COMMAND_HELP, YCMD:giveitme, "give players items");
	new ply = INVALID_PLAYER_ID, item, quantity, serial;
	if(sscanf(params, "uiiI(0)", ply, item, quantity, serial)) {
		Text_Send(playerid, $COMMAND_SYNTAX, YCMD:giveitem, "<playerid> <item> <quantity> [serial]");
		return 1;
	}
	if(!IsPlayerConnected(ply)) {
		Text_Send(playerid, $TARGET_NOT_LOGGED);
		return 1;
	}
	if(GiveItem(ply, item, quantity, serial)) {
		Text_Send(playerid, $GIVING_FAILED);
	}
	else {
		Text_Send(ply, $ITEM_WAS_GIVEN_BY, GetPlayerNameEx(playerid), quantity, Items[item][itemName], serial);
		Text_Send(playerid, $ITEM_WAS_GIVEN_TO, GetPlayerNameEx(ply), quantity, Items[item][itemName], serial);
	}
	return 1;
}

YCMD:inv(playerid, params[], help) {
	if(help) 
		return Text_Send(playerid, $COMMAND_HELP, YCMD:inv, "view and manage inventory");
	new string[2048];
	SetPVarInt(playerid, "CurrentInvItem", -1);
	for(new i=0;i<MAX_ITEMS;i++) {
		new item, quantity, serial;
		GetPlayerSlotInfo(playerid, i, item, quantity, serial);
		if(!item) {
			continue;
		}
		if(serial)
			format(string, sizeof(string), "%s%24s\t[{00AA00}%3d{FFFFFF} ��., {00AA00}%.3f{FFFFFF}��., #%5d] \n", 
				string, Items[item][itemName],  quantity, CalculateItemWeight(playerid, i), serial);
		else 
			format(string, sizeof(string), "%s%24s\t[{00AA00}%3d{FFFFFF} ��., {00AA00}%.3f{FFFFFF}��.] \n", 
				string, Items[item][itemName],  quantity, CalculateItemWeight(playerid, i));
			
	}
	strmid(string, string, 0, strlen(string)-1);


	if(strlen(string) < 30) 
		return Text_Send(playerid, $INVENTORY_IS_EMPTY);
	Dialog_ShowCallback(playerid, using callback PlayerInventory, DIALOG_STYLE_LIST, "Inventory", string, "Select", "Cancel");
	return 1;
} 


public LoadDroppedItems(query[], playerid, extraid, connectionHandle) {
	new rows=0, fields=0, string[64];
	cache_get_data(rows, fields);
	if(!rows) {
		printf("[Items] No dropped items loaded.");
	    return 1;
	}
	for(new i=0, idx=0;i<rows;i++) {
		cache_get_field_content(i, "id", string);
		idx = strval(string);
		
		cache_get_field_content(i, "ItemID", string);
		DroppedItems[idx][diID] = strval(string);
		cache_get_field_content(i, "ItemQuantity", string);
		DroppedItems[idx][diQuantity] = strval(string);
		cache_get_field_content(i, "DropTime", string);
		DroppedItems[idx][diDropTime] = strval(string);
		cache_get_field_content(i, "DroppedBy", string);
		DroppedItems[idx][diDroppedBy] = strval(string);
		cache_get_field_content(i, "PosX", string);
		DroppedItems[idx][diPosX] = floatstr(string);
		cache_get_field_content(i, "PosY", string);
		DroppedItems[idx][diPosY] = floatstr(string);
		cache_get_field_content(i, "PosZ", string);
		DroppedItems[idx][diPosZ] = floatstr(string);
		cache_get_field_content(i, "VirtualWorld", string);
		DroppedItems[idx][diVW] = strval(string);
		cache_get_field_content(i, "Interior", string);
		DroppedItems[idx][diInt] = strval(string);
		cache_get_field_content(i, "Serial", string);
		DroppedItems[idx][diSerial] = strval(string);
		
		if(Items[DroppedItems[idx][diID]][itemType] == ITEM_TYPE_WEAPON && DroppedItems[idx][diQuantity] == 1) 
			DroppedItems[idx][diObject] = CreateDynamicObject(GunObjects[Items[DroppedItems[idx][diID]][itemModel]], DroppedItems[idx][diPosX], DroppedItems[idx][diPosY], DroppedItems[idx][diPosZ]-1, 93.7, 120.0, random(360), DroppedItems[idx][diVW], DroppedItems[idx][diInt]);
		else if(Items[DroppedItems[idx][diID]][itemType] == ITEM_TYPE_WEAPON && DroppedItems[idx][diQuantity] > 1)
			DroppedItems[idx][diObject] = CreateDynamicObject(2969, DroppedItems[idx][diPosX], DroppedItems[idx][diPosY], DroppedItems[idx][diPosZ]-0.9, 0.0, 0.0, random(360), DroppedItems[idx][diVW], DroppedItems[idx][diInt]);
		else
			DroppedItems[idx][diObject] = CreateDynamicObject(Items[DroppedItems[idx][diID]][itemModel], DroppedItems[idx][diPosX], DroppedItems[idx][diPosY], DroppedItems[idx][diPosZ]-0.9, 0.0, 0.0, random(360), DroppedItems[idx][diVW], DroppedItems[idx][diInt]);

	}
	printf("[Items]: %d dropped items loaded.", rows);
	return 1;
}

hook OnGameModeInit() {
	mysql_query("DELETE FROM `droppeditems` WHERE `DropTime` < UNIX_TIMESTAMP()-4320", MySQL); // Removing all the items older than 3 days 
	mysql_query_callback(1, "SELECT * FROM `droppeditems` WHERE `id`<'500' LIMIT 500", "LoadDroppedItems", -1, MySQL, true);
}

YCMD:printitems(playerid, params[], help) {
	if(help) 
		return Text_Send(playerid, $COMMAND_HELP, YCMD:printitems, "������ ���������");
	new page;
	if(sscanf(params, "i", page)) {
		Text_Send(playerid, $COMMAND_SYNTAX, YCMD:printitems, "<page>");
		return 1;
	}
	for(new i=(page-1)*10;i<page*10 && i<sizeof(Items); i++) {
		Text_Send(playerid, $ITEM_LIST, i, Items[i][itemName], ItemType[Items[i][itemType]], Items[i][itemModel], Items[i][itemCost], Items[i][itemValue], Items[i][itemWeight]);
	}
	return 1;
}
