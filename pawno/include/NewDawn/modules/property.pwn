/**
 * property.pwn
 *
 * �������� ���� ������� ������������
 * @author gim <myvrenik@gmail.com>
 * @version 0.8
 *
 * Property functions:
 * @see AddProperty(Property)
 * @see ClearProperty(Property)
 * @see CreateProperty(Type, Float:EnterX, Float:EnterY, Float:EnterZ, Float:EnterA, Interior, Price, Lock = 1, Alarm = 0)
 * @see DestroyProperty(Property)
 * @see UpdateProperty(Property, bool:TextOnly = false)
 * @see SaveProperty(Property)
 * @see LoadPropertys()
 *
 * Player functions:
 * @see GetPlayerNearProperty(playerid, Float:Distance, bool:Enter, bool:Exit)
 * @see GetPlayerInProperty(playerid)
 * @see SetPlayerInProperty(playerid, Property)
 * @see GetPlayerPropertyCount(playerid)
 * @see ShowPropertyStatsToPlayer(Property, playerid, bool:debugInfo = false)
 * @see PlayerOffersPropertyToPlayer(playerid, targetid, Property, Price)
 * 
 * Callbacks:
 * @see OnPlayerEnterProperty(playerid, Property)
 * @see OnPlayerExitProperty(playerid, Property)
 * @see OnPlayerKnockedPropertyDoor(playerid, Property)
 * @see OnPropertyBoughtByPlayer(Property, playerid)
 * @see OnPropertySoldByPlayer(Property, playerid)
 *
 */
 
#if !defined __property_inc_included 
    #endinput
#endif


#include <YSI\y_text>
#include <YSI\y_iterate>
loadtext Core[Commands], Core[Errors], Property[Messages], Property[Errors];


hook OnGameModeInit() {
    LoadProperty();
}

/*hook OnGameModeExit() {
    printf("[Property] Saving houses");
    foreach(new p : Propertys) {
        SaveProperty(p);
        ClearProperty(p);
    }
    printf("[Property] Saving complete");
    return 1;
}*/

hook OnPlayerKeyStateChange(playerid, newkeys, oldkeys) {
    if(PlayerPrIntChange[playerid]) {
        if((newkeys & KEY_ANALOG_RIGHT) && !(oldkeys & KEY_ANALOG_RIGHT)) { // Next
            PutPlayerIntoInterior(playerid, GetPVarInt(playerid, "_IntChange-CurrentInt")+1);
        }
        else if((newkeys & KEY_ANALOG_LEFT) && !(oldkeys & KEY_ANALOG_LEFT)) { // Previous
            PutPlayerIntoInterior(playerid, GetPVarInt(playerid, "_IntChange-CurrentInt")-1);
        }
        else if((newkeys & KEY_SPRINT) && !(oldkeys & KEY_SPRINT)) { // Choose
            new Property = GetPVarInt(playerid, "_IntChange-Property");
            new Int = GetPVarInt(playerid, "_IntChange-CurrentInt");
            EditPropertyValue(Property, eInterior, Int, "");
            StopPlayerPInteriorChange(playerid);
            foreach(new i : Player) if(GetPlayerInProperty(i) == Property) OnPlayerEnterProperty(i, Property);
        }
        else if((newkeys & KEY_CROUCH) && !(oldkeys & KEY_CROUCH)) { // Cancel
            StopPlayerPInteriorChange(playerid);
        }
    }
}

hook OnPlayerDisconnect(playerid, reason) {
    if(PlayerPrIntChange[playerid]) {
        TextDrawHideForPlayer(playerid, PropertyIntChangeInfo[playerid]);
        TextDrawDestroy(PropertyIntChangeInfo[playerid]);
    }
}
//----------------------------------------------------------------------------------------------------------
/**
 * @param playerid int:
 * @param Property int: Property array index
 */
stock OnPlayerEnterProperty(playerid, Property) {
    if(PropertyInfo[Property][prType] == P_TYPE_HOUSE) { 
        if(PropertyInfo[Property][prInt] < 0 || PropertyInfo[Property][prInt] >= sizeof(HouseInts)) return;
        Streamer_UpdateEx(playerid,  HouseInts[PropertyInfo[Property][prInt]][piPos][0], HouseInts[PropertyInfo[Property][prInt]][piPos][1], HouseInts[PropertyInfo[Property][prInt]][piPos][2], HouseInts[PropertyInfo[Property][prInt]][piInt], PropertyInfo[Property][prWorld]);
        MovePlayer(playerid, HouseInts[PropertyInfo[Property][prInt]][piPos][0], HouseInts[PropertyInfo[Property][prInt]][piPos][1], HouseInts[PropertyInfo[Property][prInt]][piPos][2], HouseInts[PropertyInfo[Property][prInt]][piAngle], HouseInts[PropertyInfo[Property][prInt]][piInt], PropertyInfo[Property][prWorld]);
        
    }
    else {
        if(PropertyInfo[Property][prInt] < 0 || PropertyInfo[Property][prInt] >= sizeof(BusinessInts)) return;
        Streamer_UpdateEx(playerid, BusinessInts[PropertyInfo[Property][prInt]][piPos][0], BusinessInts[PropertyInfo[Property][prInt]][piPos][1], BusinessInts[PropertyInfo[Property][prInt]][piPos][2], BusinessInts[PropertyInfo[Property][prInt]][piInt], PropertyInfo[Property][prWorld]);
        MovePlayer(playerid, BusinessInts[PropertyInfo[Property][prInt]][piPos][0], BusinessInts[PropertyInfo[Property][prInt]][piPos][1], BusinessInts[PropertyInfo[Property][prInt]][piPos][2], BusinessInts[PropertyInfo[Property][prInt]][piAngle], BusinessInts[PropertyInfo[Property][prInt]][piInt], PropertyInfo[Property][prWorld]);
    }
    GameTextForPlayer(playerid, PropertyInfo[Property][prDescript], 2000, 1);
    SetPlayerInProperty(playerid, Property);
    return;
}
/**
 * @param playerid int:
 * @param Property int: Property array index
 */
stock OnPlayerExitProperty(playerid, Property) {
    SetPlayerInProperty(playerid, -1);
    MovePlayer(playerid, PropertyInfo[Property][prEnter][0],PropertyInfo[Property][prEnter][1],PropertyInfo[Property][prEnter][2], PropertyInfo[Property][prEnter][3]+180, P_DEFAULT_INTERIOR, P_DEFAULT_VWORLD);
    return;
}
/**
 * @param playerid int:
 * @param Property int: Property array index
 */
stock OnPlayerKnockedPropertyDoor(playerid, Property) {
    Command_ReProcess(playerid, "/me ������ � �����", false);
    if(PropertyInfo[Property][prInt] >= 0) {
        if(PropertyInfo[Property][prType] == P_TYPE_HOUSE) SendLocalMessage("* ���-�� ������ � �����!", 10.0, HouseInts[PropertyInfo[Property][prInt]][piPos][0], HouseInts[PropertyInfo[Property][prInt]][piPos][1], HouseInts[PropertyInfo[Property][prInt]][piPos][2], 1, {COLOR_ACTION}, PropertyInfo[Property][prWorld]);
        else SendLocalMessage("* ���-�� ������ � �����!", 10.0, BusinessInts[PropertyInfo[Property][prInt]][piPos][0], BusinessInts[PropertyInfo[Property][prInt]][piPos][1], BusinessInts[PropertyInfo[Property][prInt]][piPos][2], 1, {COLOR_ACTION}, PropertyInfo[Property][prWorld]);
    }
    return;
}
/**
 * @param Property int: Property array index
 * @param playerid int:
 * @param Cost bool: Should player be charged to pay for the property?
 */
stock OnPropertyBoughtByPlayer(Property, playerid, bool:Cost = true) {
    if(!IsPlayerConnected(playerid)) return;
    // ������������:
    PropertyInfo[Property][prOwnerID] = GetPVarInt(playerid, "ID");
    strmid(PropertyInfo[Property][prOwner], GetPlayerNameEx(playerid, false), 0, strlen(GetPlayerNameEx(playerid, false)), MAX_PLAYER_NAME);
    PropertyInfo[Property][prStatus] = P_STATUS_SOLD;
    UpdateProperty(Property);
    StartPropertySaveTimer(Property, 30, true);
    // �����:
    if(Cost) GivePlayerMoney(playerid, -PropertyInfo[Property][prCost]);
    GameTextForPlayer(playerid, "Property bought!", 2000, 1);
    PlayerPlaySound(playerid, 1149, 0.0, 0.0, 0.0);
    Text_Send(playerid, $PROPERTY_BOUGHT);
    PlayAudioStreamForPlayer(playerid, "https://dl.dropbox.com/u/20302155/NewDawn/gta-sa-theme-cut.ogg");
    // Camera floating efect:
    new Float:camPos[3];
    GetPlayerCameraPos(playerid, camPos[0], camPos[1], camPos[2]);
    /* I'm using a polar coordinate system to find the point
     * where camera should move to based on property entrance angle.
     * x = R*cos(a)
     * y = R*sin(a)
     * http://en.wikipedia.org/wiki/Polar_coordinate_system
     */
    new Float:x = 30*floatcos(PropertyInfo[Property][prEnter][3]+130, degrees);
    new Float:y = 30*floatsin(PropertyInfo[Property][prEnter][3]+130, degrees);
    InterpolateCameraPos(playerid, camPos[0], camPos[1], camPos[2], camPos[0]-x, camPos[1]-y, camPos[2]+20.0, 6000, CAMERA_MOVE);
    InterpolateCameraLookAt(playerid, PropertyInfo[Property][prEnter][0]+x, PropertyInfo[Property][prEnter][1]+y, PropertyInfo[Property][prEnter][2], PropertyInfo[Property][prEnter][0], PropertyInfo[Property][prEnter][1], PropertyInfo[Property][prEnter][2], 6000, CAMERA_MOVE);
    SetTimerEx("SetCameraBehindPlayerTimer", 6500, false, "i", playerid);
    return;
}
forward SetCameraBehindPlayerTimer(playerid);
public SetCameraBehindPlayerTimer(playerid) {
    return SetCameraBehindPlayer(playerid);
}
/**
 * @param playerid int:
 * @param Property int: Property array index
 */
stock OnPropertySoldByPlayer(Property, playerid) {
    // ������������:
    PropertyInfo[Property][prOwnerID] = 0;
    strmid(PropertyInfo[Property][prOwner], "None", 0, strlen("None"), MAX_PLAYER_NAME);
    PropertyInfo[Property][prStatus] = P_STATUS_ONSALE;
    UpdateProperty(Property);
    StartPropertySaveTimer(Property, 30, true);
    // �����:
    new string[50];
    new Refund = floatround(PropertyInfo[Property][prCost]*0.8);
    GivePlayerMoney(playerid, Refund);
    format(string, sizeof(string), "~w~Property sold! ~g~$%d", Refund);
    GameTextForPlayer(playerid, string, 2000, 1);
    return;
}
//----------------------------------------------------------------------------------------------------------
/**
 * @see LoadProperty()
 */
forward LoadPropertyCallback(query[], index, extraid, connectionHandle);
public LoadPropertyCallback(query[], index, extraid, connectionHandle) { 
    new rows=0, fields=0;
    cache_get_data(rows, fields, connectionHandle);
    if(!rows) {
        printf("[Property] Property loaded: %d", rows);
        return 1;
    }
    new string[100];
    new ownerNameTmp[MAX_PLAYER_NAME];
    for(new p = 0; p < P_COUNT && p < rows; p++)
    {
        cache_get_field_content(p, "id", string, connectionHandle); PropertyInfo[p][prID] = strval(string);
        cache_get_field_content(p, "OwnerID", string, connectionHandle); PropertyInfo[p][prOwnerID] = strval(string);
        cache_get_field_content(p, "OwnerName", ownerNameTmp, connectionHandle);
        PropertyInfo[p][prOwner] = ownerNameTmp;
        cache_get_field_content(p, "Enter", string, connectionHandle); sscanf(string, "P<,>ffff", PropertyInfo[p][prEnter][0], PropertyInfo[p][prEnter][1], PropertyInfo[p][prEnter][2], PropertyInfo[p][prEnter][3]);
        cache_get_field_content(p, "Interior", string, connectionHandle); PropertyInfo[p][prInt] = strval(string)-1;
        cache_get_field_content(p, "Description", string, connectionHandle); 
        PropertyInfo[p][prDescript] = string;
        cache_get_field_content(p, "Lock", string, connectionHandle); PropertyInfo[p][prLock] = strval(string);
        cache_get_field_content(p, "Locked", string, connectionHandle); PropertyInfo[p][prLocked] = strval(string);
        cache_get_field_content(p, "Alarm", string, connectionHandle); PropertyInfo[p][prAlarm] = strval(string);
        cache_get_field_content(p, "Alarmed", string, connectionHandle); PropertyInfo[p][prAlarmed] = strval(string);
        cache_get_field_content(p, "Status", string, connectionHandle); PropertyInfo[p][prStatus] = strval(string);
        cache_get_field_content(p, "Cost", string, connectionHandle); PropertyInfo[p][prCost] = strval(string);
        cache_get_field_content(p, "Type", string, connectionHandle); PropertyInfo[p][prType] = strval(string);
        cache_get_field_content(p, "Money", string, connectionHandle); PropertyInfo[p][prMoney] = strval(string);
        PropertyInfo[p][prWorld] = P_MIN_VWORLD+PropertyInfo[p][prID];
        Iter_Add(Propertys, p);
        if(PropertyInfo[p][prType] == P_TYPE_HOUSE) Iter_Add(Houses, p);
        else {
            cache_get_field_content(p, "Prods", string, connectionHandle); PropertyInfo[p][prProds] = strval(string);
            cache_get_field_content(p, "MaxProds", string, connectionHandle); PropertyInfo[p][prMaxProds] = strval(string);
            cache_get_field_content(p, "PriceLevel", string, connectionHandle); PropertyInfo[p][prPriceLevel] = strval(string);
            Iter_Add(Business, p);
        }
        PropertyInfo[p][prUpdated] = false;
        AddProperty(p);
        format(string, sizeof(string), "SELECT * FROM `property_furniture` WHERE `Property`='%d'", PropertyInfo[p][prID]);
        mysql_query_callback(p, string, "LoadPropertyFurniture", -1, MySQL, false);
        LoadPrisonCells(p);
    }
    printf("[Property] Property loaded: %d", rows);
    return 1;
}
/**
 * @see CreateProperty()
 */
forward CreatePropertyCallback(query[], Property, extraid, connectionHandle);
public CreatePropertyCallback(query[], Property, extraid, connectionHandle) {
    if(mysql_errno(MySQL)) return ClearProperty(Property);
    PropertyInfo[Property][prID] = mysql_insert_id(MySQL);
    PropertyInfo[Property][prWorld] = P_MIN_VWORLD+PropertyInfo[Property][prID];
    if(PropertyInfo[Property][prID] < 1) return ClearProperty(Property);
    
    Iter_Add(Propertys, Property);
    if(PropertyInfo[Property][prType] == P_TYPE_HOUSE) Iter_Add(Houses, Property);
    else Iter_Add(Business, Property);
    
    AddProperty(Property);
    return 1;
}
/**
 * @see DestroyProperty()
 */
forward DestroyPropertyCallback(query[], Property, extraid, connectionHandle);
public DestroyPropertyCallback(query[], Property, extraid, connectionHandle) {
    foreach(new i : Player) if(GetPlayerInProperty(i) == Property) OnPlayerExitProperty(i, Property);
    printf("[Property] Property '%s' (SQLID:%d) has been fully destroyed", PropertyInfo[Property][prDescript], PropertyInfo[Property][prID]);
    ClearProperty(Property);
    return 1;
}
/**
 *
 */
forward DestroyPropertyConfirmation(playerid, response, Property);
public DestroyPropertyConfirmation(playerid, response, Property) {
    if(response) {
        Text_Send(playerid, $PROPERTY_DESTROYED, PropertyInfo[Property][prDescript], PropertyInfo[Property][prID]);
        printf("[Property] %s is about to destroy property '%s' (SQLID:%d)", GetPlayerNameEx(playerid), PropertyInfo[Property][prDescript], PropertyInfo[Property][prID]);
        DestroyProperty(Property);
    }
    return 0;
}
/**
 * @param Property int: Property array index
 * @param Seconds int: Duration of the timer in seconds
 * @param FullSave bool:
 */
stock StartPropertySaveTimer(Property, Seconds, bool:FullSave = false) {
    if(PropertyInfo[Property][prUpdated]) return;
    
    PropertyInfo[Property][prUpdated] = true;
    if(FullSave) SetTimerEx("SaveProperty", Seconds*1000, false, "i", Property);
    else SetTimerEx("SaveBusinessStats", Seconds*1000, false, "i", Property);
    return;
}
/**
 * Just loads all property from DB
 */
stock LoadProperty() {
    new string[200];
    format(string, sizeof(string), "SELECT *, (SELECT `Name` FROM `characters` WHERE `id`= OwnerID) AS `OwnerName` FROM `property_info` LEFT JOIN `property_bizz` ON `property_info`.id = `property_bizz`.id LIMIT %d;", P_COUNT);
    mysql_query_callback(-1, string, "LoadPropertyCallback", -1, MySQL, true);
    return 1;
}
/**
 * Creates a new property
 *
 * @param Type int: Type of the property. @see property.inc
 * @param EnterX,EnterY,EnterZ,EnterA float: Entrance position
 * @param Interior int: Interior id. @see property.inc - HouseInts/BusinessInts
 * @param Price int: Price of new house
 * @param Lock int: Lock level of new house
 * @param Alarm int: Alarm level of new house
 */
stock CreateProperty(Type, Float:EnterX, Float:EnterY, Float:EnterZ, Float:EnterA, Interior, Price, Lock = 1, Alarm = 0) {
    // ���������, ���� �� ��������� ����� � �������:
    new Property = -1;
    for(new p = 0; p < P_COUNT; p++) if(PropertyInfo[p][prID] == 0) { Property = p; break; }
    if(Property == -1) return -1;
    //
    strmid(PropertyInfo[Property][prOwner], "NULL", 0, strlen("NULL"), MAX_PLAYER_NAME);
    PropertyInfo[Property][prOwnerID] = 0;
    PropertyInfo[Property][prEnter][0] = EnterX;
    PropertyInfo[Property][prEnter][1] = EnterY;
    PropertyInfo[Property][prEnter][2] = EnterZ;
    PropertyInfo[Property][prEnter][3] = EnterA;
    PropertyInfo[Property][prInt] = Interior-1;
    strmid(PropertyInfo[Property][prDescript], "New property", 0, strlen("New property"), 30);
    PropertyInfo[Property][prAlarm] = Alarm;
    PropertyInfo[Property][prLock] = Lock;
    PropertyInfo[Property][prCost] = Price;
    PropertyInfo[Property][prStatus] = 0;
    PropertyInfo[Property][prLocked] = false;
    PropertyInfo[Property][prAlarmed] = false;
    PropertyInfo[Property][prType] = Type;
    // ������:
    new query[800], EnterCoords[60];
    format(EnterCoords, sizeof(EnterCoords), "%f,%f,%f,%f", PropertyInfo[Property][prEnter][0], PropertyInfo[Property][prEnter][1], PropertyInfo[Property][prEnter][2], PropertyInfo[Property][prEnter][3]);
    format(query, sizeof(query), "INSERT INTO `property_info` (`id`,`Type`,`OwnerID`,`Enter`,`Interior`,`Description`,`Alarm`,`Alarmed`,`Lock`,`Locked`,`Status`,`Cost`) VALUES (NULL,%d,0,'%s',%d,'New property',%d,0,%d,0,0,%d);",Type,EnterCoords,Interior,Alarm,Lock,Price);
    mysql_query_callback(Property, query, "CreatePropertyCallback", -1, MySQL, true);
    return Property;
}
/**
 * Fully removes property from the server & DB
 *
 * @param Property int: Property array index
 */
stock DestroyProperty(Property) {
    if(PropertyInfo[Property][prID] == 0) return 0;
    new query[100];
    format(query, sizeof(query), "DELETE FROM `property_info` WHERE `id`=%d", PropertyInfo[Property][prID]);
    mysql_query_callback(Property, query, "DestroyPropertyCallback", -1, MySQL, true);
    return 1;
}
/**
 * Visually adds property to the server (3D label texts, pickups, etc)
 * 
 * @param Property int: Property array index
 */
stock AddProperty(Property) {
    if(PropertyInfo[Property][prID] > 0) {
        new string[OUTPUT];
        if(PropertyInfo[Property][prType] == P_TYPE_HOUSE) {
            PropertyInfo[Property][prMarker] = CreateDynamicCP(PropertyInfo[Property][prEnter][0], PropertyInfo[Property][prEnter][1], PropertyInfo[Property][prEnter][2], 1.0, P_DEFAULT_VWORLD, P_DEFAULT_INTERIOR, -1, P_CP_DISTANCE);
            switch(PropertyInfo[Property][prStatus]) {
                case P_STATUS_ONSALE: format(string, sizeof(string), "%s\nON SALE $%d\n/property buy", PropertyInfo[Property][prDescript], PropertyInfo[Property][prCost]);
                case P_STATUS_SOLD: format(string, sizeof(string), "%s\nOwner: %s", PropertyInfo[Property][prDescript], PropertyInfo[Property][prOwner]);
                case P_STATUS_FROZEN: format(string, sizeof(string), "%s\nFROZEN", PropertyInfo[Property][prDescript]);
                default: return printf("[Property] Warning: Unknown house status (ID: %d; SQLID: %d)", Property, PropertyInfo[Property][prID]);
            }
            PropertyInfo[Property][pr3DText][0] = CreateDynamic3DTextLabel(string, P_HOUSE_3D_COLOR, PropertyInfo[Property][prEnter][0], PropertyInfo[Property][prEnter][1], PropertyInfo[Property][prEnter][2], 1.0, INVALID_PLAYER_ID, INVALID_VEHICLE_ID, 1, P_DEFAULT_VWORLD, P_DEFAULT_INTERIOR, -1, 2.0);
            if(PropertyInfo[Property][prInt] >= 0) PropertyInfo[Property][pr3DText][1] = CreateDynamic3DTextLabel("Exit", P_HOUSE_3D_COLOR, HouseInts[PropertyInfo[Property][prInt]][piPos][0], HouseInts[PropertyInfo[Property][prInt]][piPos][1], HouseInts[PropertyInfo[Property][prInt]][piPos][2], P_3D_DISTANCE, INVALID_PLAYER_ID, INVALID_VEHICLE_ID, 1, PropertyInfo[Property][prWorld], HouseInts[PropertyInfo[Property][prInt]][piInt], -1, P_3D_DISTANCE+1.0);
        }
        else {
            switch(PropertyInfo[Property][prStatus]) {
                case P_STATUS_ONSALE: {
                    format(string, sizeof(string), "%s\nON SALE $%d\n/property buy", PropertyInfo[Property][prDescript], PropertyInfo[Property][prCost]);
                    PropertyInfo[Property][prMarker] = CreateDynamicPickup(1274, 2, PropertyInfo[Property][prEnter][0], PropertyInfo[Property][prEnter][1], PropertyInfo[Property][prEnter][2], P_DEFAULT_VWORLD, P_DEFAULT_INTERIOR, -1, 150.0);
                }
                case P_STATUS_SOLD: {
                    format(string, sizeof(string), "%s\nOwner: %s", PropertyInfo[Property][prDescript], PropertyInfo[Property][prOwner]);
                    PropertyInfo[Property][prMarker] = CreateDynamicPickup(1239, 2, PropertyInfo[Property][prEnter][0], PropertyInfo[Property][prEnter][1], PropertyInfo[Property][prEnter][2], P_DEFAULT_VWORLD, P_DEFAULT_INTERIOR, -1, 150.0);
                }
                default: return printf("[Property] Warning: Unknown business status (ID: %d; SQLID: %d)", Property, PropertyInfo[Property][prID]);
            }
            PropertyInfo[Property][pr3DText][0] = CreateDynamic3DTextLabel(string, P_BIZZ_3D_COLOR, PropertyInfo[Property][prEnter][0], PropertyInfo[Property][prEnter][1], PropertyInfo[Property][prEnter][2], P_3D_DISTANCE, INVALID_PLAYER_ID, INVALID_VEHICLE_ID, 1, P_DEFAULT_VWORLD, P_DEFAULT_INTERIOR, -1, P_3D_DISTANCE+1.0);
            if(PropertyInfo[Property][prInt] >= 0) PropertyInfo[Property][pr3DText][1] = CreateDynamic3DTextLabel("Exit", P_BIZZ_3D_COLOR, BusinessInts[PropertyInfo[Property][prInt]][piPos][0], BusinessInts[PropertyInfo[Property][prInt]][piPos][1], BusinessInts[PropertyInfo[Property][prInt]][piPos][2], P_3D_DISTANCE, INVALID_PLAYER_ID, INVALID_VEHICLE_ID, 1, PropertyInfo[Property][prWorld], BusinessInts[PropertyInfo[Property][prInt]][piInt], -1, P_3D_DISTANCE+1.0);
        }
        foreach(new i : Player) if(IsPlayerInRangeOfPoint(i, P_3D_DISTANCE, PropertyInfo[Property][prEnter][0], PropertyInfo[Property][prEnter][1], PropertyInfo[Property][prEnter][2])) Streamer_Update(i);
        //printf("[Property] Notify: ��� #%d ���������: %s, Owner: %s (SQLID:%d)", Property, PropertyInfo[Property][prDescript], PropertyInfo[Property][prOwner], PropertyInfo[Property][prID]);
        return 1;
    }
    printf("[Property] Error: Failed to load a house (SQLID: %d)", PropertyInfo[Property][prID]);
    return 0;
}
/**
 * Visually updates property on the server (3D label texts, pickups, etc)
 * 
 * @param Property int: Property array index
 * @param TextOnly bool: Updates only 3D text labels if true
 */
stock UpdateProperty(Property, bool:TextOnly = false) {
    new string[100];
    if(PropertyInfo[Property][prType] == P_TYPE_HOUSE) {
        if(!TextOnly) {
            DestroyDynamic3DTextLabel(PropertyInfo[Property][pr3DText][1]);
            PropertyInfo[Property][pr3DText][1] = CreateDynamic3DTextLabel("�����", P_HOUSE_3D_COLOR, HouseInts[PropertyInfo[Property][prInt]][piPos][0], HouseInts[PropertyInfo[Property][prInt]][piPos][1], HouseInts[PropertyInfo[Property][prInt]][piPos][2], P_3D_DISTANCE, INVALID_PLAYER_ID, INVALID_VEHICLE_ID, 1, PropertyInfo[Property][prWorld], HouseInts[PropertyInfo[Property][prInt]][piInt], -1, P_3D_DISTANCE+1.0);
        }
        switch(PropertyInfo[Property][prStatus]) {
            case P_STATUS_ONSALE: {
                format(string, sizeof(string), "%s\nON SALE $%d\n/property buy", PropertyInfo[Property][prDescript], PropertyInfo[Property][prCost]);
                UpdateDynamic3DTextLabelText(PropertyInfo[Property][pr3DText][0], P_HOUSE_3D_COLOR, string);
            }
            case P_STATUS_SOLD: {
                format(string, sizeof(string), "%s\nOwner: %s", PropertyInfo[Property][prDescript], PropertyInfo[Property][prOwner]);
                UpdateDynamic3DTextLabelText(PropertyInfo[Property][pr3DText][0], P_HOUSE_3D_COLOR, string);
            }
            case P_STATUS_FROZEN: {
                format(string, sizeof(string), "%s\nFROZEN", PropertyInfo[Property][prDescript]);
                UpdateDynamic3DTextLabelText(PropertyInfo[Property][pr3DText][0], P_HOUSE_3D_COLOR, string);
            }
            default: printf("[Property] Warning: Unknown property status (ID: %d; SQLID: %d)", Property, PropertyInfo[Property][prID]);
        }
    }
    else {
        if(!TextOnly) {
            DestroyDynamicPickup(PropertyInfo[Property][prMarker]);
            DestroyDynamic3DTextLabel(PropertyInfo[Property][pr3DText][1]);
            PropertyInfo[Property][pr3DText][1] = CreateDynamic3DTextLabel("�����", P_BIZZ_3D_COLOR, BusinessInts[PropertyInfo[Property][prInt]][piPos][0], BusinessInts[PropertyInfo[Property][prInt]][piPos][1], BusinessInts[PropertyInfo[Property][prInt]][piPos][2], P_3D_DISTANCE, INVALID_PLAYER_ID, INVALID_VEHICLE_ID, 1, PropertyInfo[Property][prWorld], BusinessInts[PropertyInfo[Property][prInt]][piInt], -1, P_3D_DISTANCE+1.0);
        }
        switch(PropertyInfo[Property][prStatus]) {
            case P_STATUS_ONSALE: {
                format(string, sizeof(string), "%s\nON SALE $%d\n/property buy", PropertyInfo[Property][prDescript], PropertyInfo[Property][prCost]);
                UpdateDynamic3DTextLabelText(PropertyInfo[Property][pr3DText][0], P_BIZZ_3D_COLOR, string);
                if(!TextOnly) PropertyInfo[Property][prMarker] = CreateDynamicPickup(1273, 2, PropertyInfo[Property][prEnter][0], PropertyInfo[Property][prEnter][1], PropertyInfo[Property][prEnter][2], P_DEFAULT_INTERIOR, P_DEFAULT_VWORLD, -1, 150.0);
            }
            case P_STATUS_SOLD: {
                format(string, sizeof(string), "%s\nOwner: %s", PropertyInfo[Property][prDescript], PropertyInfo[Property][prOwner]);
                UpdateDynamic3DTextLabelText(PropertyInfo[Property][pr3DText][0], P_BIZZ_3D_COLOR, string);
                if(!TextOnly) PropertyInfo[Property][prMarker] = CreateDynamicPickup(1239, 2, PropertyInfo[Property][prEnter][0], PropertyInfo[Property][prEnter][1], PropertyInfo[Property][prEnter][2], P_DEFAULT_INTERIOR, P_DEFAULT_VWORLD, -1, 150.0);
            }
            default: printf("[Property] Warning: Unknown business status (ID: %d; SQLID: %d)", Property, PropertyInfo[Property][prID]);
        }
        
    }
    printf("[Property] Property #%d is updated: %s, Owner: %s (SQLID:%d)", Property, PropertyInfo[Property][prDescript], PropertyInfo[Property][prOwner], PropertyInfo[Property][prID]);
    return 1;
}
/**
 * Fully removes property from the server(!)
 * 
 * @param Property int: Property array index
 */
stock ClearProperty(Property) {
    // ���������� �����������:
    DestroyDynamic3DTextLabel(PropertyInfo[Property][pr3DText][0]);
    DestroyDynamic3DTextLabel(PropertyInfo[Property][pr3DText][1]);
    if(PropertyInfo[Property][prType] == P_TYPE_HOUSE) DestroyDynamicCP(PropertyInfo[Property][prMarker]);
    else DestroyDynamicPickup(PropertyInfo[Property][prMarker]);
    // ������ ����������:
    PropertyInfo[Property][prID] = 0;
    strdel(PropertyInfo[Property][prOwner], 0, MAX_PLAYER_NAME);
    PropertyInfo[Property][prOwnerID] = 0;
    PropertyInfo[Property][prEnter][0] = 0.0;
    PropertyInfo[Property][prEnter][1] = 0.0;
    PropertyInfo[Property][prEnter][2] = 0.0;
    PropertyInfo[Property][prEnter][3] = 0.0;
    PropertyInfo[Property][prInt] = -1;
    PropertyInfo[Property][prWorld] = 0;
    strdel(PropertyInfo[Property][prDescript], 0, 255);
    PropertyInfo[Property][prAlarm] = 0;
    PropertyInfo[Property][prLock] = 0;
    PropertyInfo[Property][prCost] = 0;
    PropertyInfo[Property][prStatus] = 0;
    PropertyInfo[Property][prLocked] = false;
    PropertyInfo[Property][prType] = 0;
    PropertyInfo[Property][prProds] = 0;
    PropertyInfo[Property][prMoney] = 0;
    
    //Iter_Remove(Property, Property);
    return 1;
}
/** 
 * Returns property index (-1 if not found)
 * 
 * @param playerid int:
 */
stock GetPlayerInProperty(playerid) {
    return GetPVarInt(playerid, "InProperty")-1;
}
/** 
 * Accepts values from 0 to N. 0 is not in a proper
 *
 * @param playerid int:
 * @param Property int: Property array index
 */
stock SetPlayerInProperty(playerid, property) {
    return SetPVarInt(playerid, "InProperty", property+1);
}
/** 
 * Gets player nearest property within the specified radius
 *
 * @param playerid int:
 * @param Distance float:
 * @param Enter bool:
 * @param Exit bool:
 */
stock GetPlayerNearProperty(playerid, Float:Distance, bool:Enter, bool:Exit) {
    new Property = -1;
    if(Enter) {
        foreach(new p : Propertys) {
            if(IsPlayerInRangeOfPoint(playerid, Distance, PropertyInfo[p][prEnter][0],PropertyInfo[p][prEnter][1],PropertyInfo[p][prEnter][2])) {
                Property = p;
                break;
            }
        }
    }
    if(Exit && Property == -1) {
        foreach(new p : Propertys) {
            if(PropertyInfo[p][prInt] < 0) continue;
            if(PropertyInfo[p][prType] == P_TYPE_HOUSE) {
                if(PropertyInfo[p][prInt] < 0 || PropertyInfo[p][prInt] > sizeof(HouseInts)) continue;
                if(IsPlayerInRangeOfPoint(playerid, Distance, HouseInts[PropertyInfo[p][prInt]][piPos][0], HouseInts[PropertyInfo[p][prInt]][piPos][1], HouseInts[PropertyInfo[p][prInt]][piPos][2])
                    && GetPlayerVirtualWorld(playerid) == PropertyInfo[p][prWorld]) {
                    Property = p;
                    break;
                }
            }
            else {
                if(PropertyInfo[p][prInt] < 0 || PropertyInfo[p][prInt] > sizeof(BusinessInts)) continue;
                if(IsPlayerInRangeOfPoint(playerid, Distance, BusinessInts[PropertyInfo[p][prInt]][piPos][0], BusinessInts[PropertyInfo[p][prInt]][piPos][1], BusinessInts[PropertyInfo[p][prInt]][piPos][2])
                && GetPlayerVirtualWorld(playerid) == PropertyInfo[p][prWorld]) {
                    Property = p;
                    break;
                }
            }
        }
    }
    return Property;
}
/** 
 * Shows property stats to the specified player
 *
 * @param Property int: Property array index
 * @param playerid int:
 * @param debugInfo bool: Shows technical information about the property
 */
stock ShowPropertyStatsToPlayer(Property, playerid, bool:debugInfo = false) {
    new string[200];
    
    new LockStatus[15];
    if(PropertyInfo[Property][prLocked]) strmid(LockStatus, "Closed", 0, 15);
    else strmid(LockStatus, "�������", 0, 15);
    
    new AlarmStatus[15];
    if(PropertyInfo[Property][prAlarmed]) strmid(AlarmStatus, "Open", 0, 15);
    else strmid(AlarmStatus, "���������", 0, 15);
    
    Text_Send(playerid, $PROPERTY_INFOTITLE, PropertyInfo[Property][prDescript]);
    Text_Send(playerid, $PROPERTY_INFO_1, PropertyInfo[Property][prLock], PropertyInfo[Property][prAlarm]);
    Text_Send(playerid, $PROPERTY_INFO_2, LockStatus, AlarmStatus);
    
    if(PropertyInfo[Property][prType] != P_TYPE_HOUSE) Text_Send(playerid, $PROPERTY_BIZZ_INFO, PropertyInfo[Property][prProds], PropertyInfo[Property][prMaxProds], PropertyInfo[Property][prPriceLevel], PropertyInfo[Property][prMoney]);
    
    if(debugInfo) {
        format(string, sizeof(string), "OwnerID:%d, IntID:%d, Type:%d, VW:%d, X:%f, Y:%f, Z:%f", 
            PropertyInfo[Property][prOwnerID],
            PropertyInfo[Property][prInt]+1,
            PropertyInfo[Property][prType],
            PropertyInfo[Property][prWorld],
            PropertyInfo[Property][prEnter][0],
            PropertyInfo[Property][prEnter][1],
            PropertyInfo[Property][prEnter][2]);
        SendClientMessage(playerid, COLOR_RED, string);
    }
    return;
}
/** 
 * Changes property owner
 *
 * @param Property int: Property array index
 * @param newOwnerid int: The player ID of the new owner of the property
 */
stock ChangePropertyOwner(Property, newOwnerid) {
    if(!IsPlayerConnected(newOwnerid)) return;
    printf("[Property] Owner of the property \"%s\" (ID:%d) has been changed (%s(SQLID:%d) > %s(SQLID:%d))", 
        PropertyInfo[Property][prDescript], 
        PropertyInfo[Property][prID],
        PropertyInfo[Property][prOwner],
        PropertyInfo[Property][prOwnerID],
        GetPlayerNameEx(newOwnerid),
        GetPVarInt(newOwnerid, "ID"));
    OnPropertyBoughtByPlayer(Property, newOwnerid, false);
    return;
}
stock EditPropertyValue(Property, prEdit:Edit, newValue, newValueTxt[]) {
    StartPropertySaveTimer(Property, 10, true);
    switch(Edit) {
        case eOwner: {
            if(!IsPlayerConnected(newValue)) return 0;
            printf("[Property] �������� ������������ \"%s\" (ID:%d) ������� (%s(SQLID:%d) > %s(SQLID:%d))", 
                PropertyInfo[Property][prDescript], 
                PropertyInfo[Property][prID],
                PropertyInfo[Property][prOwner],
                PropertyInfo[Property][prOwnerID],
                GetPlayerNameEx(newValue),
                GetPVarInt(newValue, "ID"));
            OnPropertyBoughtByPlayer(Property, newValue, false);
            return 1;
        }
        case eDescription: {
            // ��������� �� �����:
            new const BadSymbols[] = {'%','_'};
            for(new i = 0, len = strlen(newValueTxt); i < len; i++) {
                for(new s = 0; s < sizeof(BadSymbols); s++) {
                    if(newValueTxt[i] == BadSymbols[s]) {
                        newValueTxt[i] = ' ';
                        break;
                    }
                }
            }
            strmid(PropertyInfo[Property][prDescript], newValueTxt, 0, strlen(newValueTxt), 30);
            UpdateProperty(Property, true);
            return 1;
        }
        case eInterior: {
            PropertyInfo[Property][prInt] = newValue;
            return 1;
        }
        case ePosition: {
            new newPos[4];
            sscanf(newValueTxt, "P<,>ffff", newPos[0], newPos[1], newPos[2], newPos[3]);
            for(new i = 0; i < 4; i++) PropertyInfo[Property][prEnter][i] = newPos[i];
            return 1;
        }
    }
    return 0;
}
/** 
 * Counts propertys owned by player
 *
 * @param playerid int:
 */
stock GetPlayerPropertyCount(playerid) {
    new Count = 0, pID = GetPVarInt(playerid, "ID");
    foreach(new p : Propertys) if(PropertyInfo[p][prOwnerID] == pID) Count++;
    return Count;
}
/** 
 * Saves property to the DB
 *
 * @param Property int: Property array index
 */
forward SaveProperty(Property);
public SaveProperty(Property) {
    new string[2048], EnterCoords[60], DescriptEscape[100];
    format(EnterCoords, sizeof(EnterCoords), "%f,%f,%f,%f", PropertyInfo[Property][prEnter][0], PropertyInfo[Property][prEnter][1], PropertyInfo[Property][prEnter][2], PropertyInfo[Property][prEnter][3]);
    mysql_real_escape_string(PropertyInfo[Property][prDescript], DescriptEscape);
    // Query:
    format(string, sizeof(string), "UPDATE `property_info` SET \
    `OwnerID`='%d',\
    `Enter`='%s',\
    `Interior`='%d',\
    `Description`='%s',\
    `Alarm`='%d',\
    `Alarmed`='%d',\
    `Lock`='%d',\
    `Locked`='%d',\
    `Status`='%d',\
    `Cost`='%d'\
    WHERE `id`='%d';", 
    PropertyInfo[Property][prOwnerID], 
    EnterCoords,
    PropertyInfo[Property][prInt]+1,
    DescriptEscape,
    PropertyInfo[Property][prAlarm],
    PropertyInfo[Property][prAlarmed],
    PropertyInfo[Property][prLock],
    PropertyInfo[Property][prLocked],
    PropertyInfo[Property][prStatus],
    PropertyInfo[Property][prCost],
    PropertyInfo[Property][prID]);
    mysql_query(string, MySQL);
    PropertyInfo[Property][prUpdated] = false;
    return 1;
}
/** 
 * @param Property int: Property array index
 */
forward SaveBusinessStats(Property);
public SaveBusinessStats(Property) {
    new string[256];
    format(string, sizeof(string), "UPDATE `property_bizz` SET \
    `Prods`='%d',\
    `MaxProds`='%d',\
    `PriceLevel`='%d'\
    WHERE `id`='%d';",
    PropertyInfo[Property][prProds],
    PropertyInfo[Property][prMaxProds],
    PropertyInfo[Property][prPriceLevel],
    PropertyInfo[Property][prMoney],
    PropertyInfo[Property][prID]);
    mysql_query(string, MySQL);
    PropertyInfo[Property][prUpdated] = false;
    return 1;
}
/** 
 * @param playerid int:
 * @param targetid int:
 * @param Property int: Property array index
 * @param Price int:
 */
stock PlayerOffersPropertyToPlayer(playerid, targetid, Property, Price) {
    new text1[256], text2[256];
    // Player:
    SetPVarInt(playerid, "PropertyOfferPrice", Price);
    SetPVarInt(playerid, "PropertyOfferID", Property);
    format(text1, sizeof(text1), "{FFFFFF}You have just offered your property \"%s\" to %s\nYour price: {00FF00}$%d{FFFFFF}\n\nAwaiting response..", PropertyInfo[Property][prDescript], GetPlayerNameEx(targetid, true), Price);
    // Target:
    format(text2, sizeof(text2), "{FFFFFF}%s has just offered you to buy \"%s\"\nLock Level: %d\nAlarm Level: %d\nPrice: {00FF00}$%d{FFFFFF}\n", GetPlayerNameEx(playerid, true), PropertyInfo[Property][prDescript], PropertyInfo[Property][prLock], PropertyInfo[Property][prAlarm], Price);
    
    CreateOffer(playerid, targetid, text1, text2, "PropertyOfferDialog");
    return;
}
/*
 *
 */
stock StartPlayerPInteriorChange(playerid, Property) {
    // TextDraw info:
    PropertyIntChangeInfo[playerid] = TextDrawCreate(300.0, 363.0, "_");
    TextDrawBackgroundColor(PropertyIntChangeInfo[playerid], 255);
    TextDrawFont(PropertyIntChangeInfo[playerid], 2);
    TextDrawLetterSize(PropertyIntChangeInfo[playerid], 0.35, 1.2);
    TextDrawColor(PropertyIntChangeInfo[playerid], -1);
    TextDrawSetOutline(PropertyIntChangeInfo[playerid], 1);
    TextDrawSetProportional(PropertyIntChangeInfo[playerid], 1);
    TextDrawAlignment(PropertyIntChangeInfo[playerid], 2);
    TextDrawShowForPlayer(playerid, PropertyIntChangeInfo[playerid]);
    //
    SetPlayerVirtualWorld(playerid, 1);
    PutPlayerIntoInterior(playerid, 0);
    SetPVarInt(playerid, "_IntChange-Property", Property);
    PlayerPrIntChange[playerid] = true;
    return;
}
stock StopPlayerPInteriorChange(playerid) {
    new Property = GetPVarInt(playerid, "_IntChange-Property");
    OnPlayerEnterProperty(playerid, Property);
    DeletePVar(playerid, "_IntChange-Property");
    DeletePVar(playerid, "_IntChange-CurrentInt");
    PlayerPrIntChange[playerid] = false;
    // TextDraw info:
    TextDrawHideForPlayer(playerid, PropertyIntChangeInfo[playerid]);
    TextDrawDestroy(PropertyIntChangeInfo[playerid]);
    return;
}
stock PutPlayerIntoInterior(playerid, Int) {
    new Property = GetPVarInt(playerid, "_IntChange-Property");
    new Float:PlayerFacingAngle;
    GetPlayerFacingAngle(playerid, PlayerFacingAngle);
    if(PropertyInfo[Property][prType] == P_TYPE_HOUSE) { 
        if(Int < 0) Int = sizeof(HouseInts)-1;
        else if(Int >= sizeof(HouseInts)) Int = 0;
        MovePlayer(playerid, HouseInts[Int][piPos][0], HouseInts[Int][piPos][1], HouseInts[Int][piPos][2], PlayerFacingAngle, HouseInts[Int][piInt]);
    }
    else {
        if(Int < 0) Int = sizeof(BusinessInts)-1;
        if(Int >= sizeof(BusinessInts)) Int = 0;
        MovePlayer(playerid, BusinessInts[Int][piPos][0], BusinessInts[Int][piPos][1], BusinessInts[Int][piPos][2], PlayerFacingAngle, BusinessInts[Int][piInt]);
    }
    SetPVarInt(playerid, "_IntChange-CurrentInt", Int);
    // TextDraw info:
    new string[200];
    if(PropertyInfo[Property][prType] == P_TYPE_HOUSE) format(string, sizeof(string), "~w~%s~n~~r~< ~w~~k~~VEHICLE_TURRETLEFT~ ; ~k~~VEHICLE_TURRETRIGHT~ ~r~>~n~~g~Accept: ~w~~k~~PED_SPRINT~ ; ~r~Calcel: ~w~~k~~PED_DUCK~", HouseInts[Int][piName]);
    else format(string, sizeof(string), "~w~%s~n~~r~< ~w~~k~~VEHICLE_TURRETLEFT~ ; ~k~~VEHICLE_TURRETRIGHT~ ~r~>~n~~g~Accept: ~w~~k~~PED_SPRINT~ ; ~r~Calcel: ~w~~k~~PED_DUCK~", BusinessInts[Int][piName]);
    TextDrawSetString(PropertyIntChangeInfo[playerid], string);
    return;
}
//----------------------------------------------------------------------------------------------------------
forward PropertyOfferDialog(playerid, targetid, response);
public PropertyOfferDialog(playerid, targetid, response) {
    new Price = GetPVarInt(playerid, "PropertyOfferPrice");
    new Property = GetPVarInt(playerid, "PropertyOfferID");
    DeletePVar(playerid, "PropertyOfferPrice");
    DeletePVar(playerid, "PropertyOfferID");
    
    if(response) { // Accept
        if(GetPlayerMoney(targetid) < Price) {
            Text_Send(targetid, $PLAYER_CANT_AFFORD);
            goto DeclineOffer; // ������������� �������� �����������.
        }
        if(GetPlayerPropertyCount(targetid) > P_LIMIT_PER_PLAYER) {
            Text_Send(playerid, $LIMITED_PROPERTY_PER_PLAYER, P_LIMIT_PER_PLAYER);
            goto DeclineOffer; // ������������� �������� �����������.
        }
        new string[OUTPUT];
        // playerid:
        GivePlayerMoney(playerid, Price);
        format(string, sizeof(string), "~w~Property sold! ~g~$%d", Price);
        GameTextForPlayer(playerid, string, 2000, 1);
        // targetid:
        GivePlayerMoney(targetid, -Price);
        format(string, sizeof(string), "~w~Property bought! ~r~-$%d", Price);
        GameTextForPlayer(targetid, string, 2000, 1);
        ChangePropertyOwner(Property, targetid);
    }
    else { // Decline
        DeclineOffer:
        return false;
    }
    return true;
}
forward PropertyCancelOfferDialog(playerid, dialogid, response, listitem, string:inputtext[]);
public PropertyCancelOfferDialog(playerid, dialogid, response, listitem, string:inputtext[]) {
    new offertoID = GetPVarInt(playerid, "PropertyOfferTo");
    ShowPlayerDialog(offertoID, 0, DIALOG_STYLE_MSGBOX, "Cancellation", "Offer has been cancelled!", "Close", "");
    
    DeletePVar(playerid, "PropertyOfferTo");
    DeletePVar(playerid, "PropertyOfferID");
    DeletePVar(playerid, "PropertyOfferPrice");
    DeletePVar(offertoID, "PropertyOfferFrom");
    return 1;
}
//----------------------------------------------------------------------------------------------------------
YCMD:property(playerid, params[], help) {
    if(!LoggedIn(playerid)) return true;
    if(isnull(params)) {
        Text_Send(playerid, $COMMAND_SYNTAX, YCMD:property, "<param> (buy; sell; list; find; status; sellto)");
        return true;
    }    
    new Act[8], string[OUTPUT], param[2];
    sscanf(params, "s[8]I(0)I(0)", Act, param[0], param[1]);
    
    if(!strcmp(Act, "buy", true)) {
        new Property = GetPlayerNearProperty(playerid, 5.0, true, false);
        if(Property == -1) return Text_Send(playerid, $NOT_NEAR);
        if(PropertyInfo[Property][prStatus] != P_STATUS_ONSALE) return Text_Send(playerid, $NOT_ON_SALE);
        if(GetPlayerMoney(playerid) <= PropertyInfo[Property][prCost]) return Text_Send(playerid, $PLAYER_CANT_AFFORD);
        if(GetPlayerPropertyCount(playerid) > P_LIMIT_PER_PLAYER) return Text_Send(playerid, $LIMITED_PROPERTY_PER_PLAYER, P_LIMIT_PER_PLAYER);
        
        OnPropertyBoughtByPlayer(Property, playerid);
        return true;
    }
    else if(!strcmp(Act, "sell", true)) {
        new Property = GetPlayerNearProperty(playerid, 5.0, true, false);
        if(Property == -1) return Text_Send(playerid, $NOT_NEAR);
        if(PropertyInfo[Property][prOwnerID] != GetPVarInt(playerid, "ID")) return Text_Send(playerid, $NOT_YOUR_PROPERTY);
        
        OnPropertySoldByPlayer(Property, playerid);
        return true;
    }
    else if(!strcmp(Act, "list", true)) {
        new PropertyCount = 1, pID = GetPVarInt(playerid, "ID");
        foreach(new p : Propertys) {
            if(PropertyInfo[p][prOwnerID] != pID)
                continue;
            if(PropertyInfo[p][prType] != P_TYPE_HOUSE) {
                format(string, sizeof(string), " Business #%d: %s", PropertyCount, PropertyInfo[p][prDescript]);
            }
            else {
                format(string, sizeof(string), " House #%d: %s", PropertyCount, PropertyInfo[p][prDescript]);
            }
            PropertyCount++;
            SendClientMessage(playerid, COLOR_NOTIFICATION, string);
        }
        return true;
    }
    else if(!strcmp(Act, "find", true)) {
        if(param[0] < 1) return Text_Send(playerid, $COMMAND_SYNTAX, YCMD:property, "find <id>");
        
        new PropertyCount = 0, pID = GetPVarInt(playerid, "ID");
        foreach(new p : Propertys) {
            if(PropertyInfo[p][prOwnerID] != pID) continue;
            PropertyCount++;
            if(PropertyCount == param[0]) {
                SetPlayerCheckpoint(playerid, PropertyInfo[p][prEnter][0], PropertyInfo[p][prEnter][1], PropertyInfo[p][prEnter][2], 4.0);
                Text_Send(playerid, $HOUSE_MARKED_ON_MAP);
                return 1;
            }
        }
        Text_Send(playerid, $UNKNOWN_ID);
        return true;
    }
    else if(!strcmp(Act, "status", true) || !strcmp(Act, "stats", true) || !strcmp(Act, "info", true)) {
        if(param[0] < 1) return Text_Send(playerid, $COMMAND_SYNTAX, YCMD:property, "status <id>");
        
        new PropertyCount = 0, pID = GetPVarInt(playerid, "ID");
        foreach(new p : Propertys) {
            if(PropertyInfo[p][prOwnerID] != pID) continue;
            PropertyCount++;
            if(PropertyCount == param[0]) {
                ShowPropertyStatsToPlayer(p, playerid, true);
                return true;
            }
        }
        Text_Send(playerid, $UNKNOWN_ID);
        return true;
    }
    else if(!strcmp(Act, "sellto", true)) {
        new Property = GetPlayerNearProperty(playerid, 5.0, true, false);
        if(Property == -1) return Text_Send(playerid, $NOT_NEAR);
        if(PropertyInfo[Property][prOwnerID] != GetPVarInt(playerid, "ID")) return Text_Send(playerid, $NOT_YOUR_PROPERTY);
        
        if(sscanf(params, "s[8]ui", Act, param[0], param[1])) return Text_Send(playerid, $COMMAND_SYNTAX, YCMD:property, "sellto <playerid> <price>");
        if(!IsPlayerConnected(param[0])) return Text_Send(playerid, $TARGET_NOT_LOGGED);
        if(params[1] < 0) return Text_Send(playerid, $COMMAND_SYNTAX, YCMD:property, "sellto <playerid> <price>");
        PlayerOffersPropertyToPlayer(playerid, param[0], Property, param[1]);
        return true;
    }
    else if(!strcmp(Act, "create", true)) {
        if(!IsAnAdmin(playerid, ADMIN_LEVEL_LEAD)) return true;
        new stringType[10];
        if(sscanf(params, "s[8]s[10]ii", Act, stringType, param[0], param[1])) {
            Text_Send(playerid, $COMMAND_SYNTAX, YCMD:property, "create <type> <interior> <price>");
            return true;
        }
        new Float:Pos[4], Type = -1;
        
        for(new i = 0; i < sizeof(prTypeName); i++) if(!strcmp(stringType, prTypeName[i], true)) { Type = i; break; }
        if(Type == -1) return Text_Send(playerid, $UNKNOWN_TYPE);
        if(param[0] < 0 || param[1] < 0) return true;
        else if(Type == P_TYPE_HOUSE && param[0] > sizeof(HouseInts)) return true;
        else if(Type != P_TYPE_HOUSE && param[0] > sizeof(BusinessInts)) return true;
        
        GetPlayerPos(playerid, Pos[0],Pos[1],Pos[2]);
        GetPlayerFacingAngle(playerid, Pos[3]);
        CreateProperty(Type, Pos[0], Pos[1], Pos[2], Pos[3], param[0], param[1]);
        Text_Send(playerid, $PROPERTY_CREATED, prTypeName[Type], param[0], param[1]);
        printf("[Property] %s created a new property: Type: %s | Interior: %d | Price: $%d", GetPlayerNameEx(playerid), prTypeName[Type], param[0], param[1]);
        return true;
    }
    else if(!strcmp(Act, "destroy", true)) {
        if(!IsAnAdmin(playerid, ADMIN_LEVEL_LEAD)) return true;
        new Property = -1;
        if(param[0] < 1) {
            Property = GetPlayerNearProperty(playerid, 5.0, true, false);
            if(Property == -1) return Text_Send(playerid, $NOT_NEAR);
        }
        else {
            foreach(new p : Propertys) {
                if(PropertyInfo[p][prID] == param[0]) {
                    Property = p;
                    break;
                }
            }
            if(Property == -1) return Text_Send(playerid, $UNKNOWN_ID);
        }
        format(string, sizeof(string), "{FFFFFF}Are you sure you want to {FF0000}fully destroy{FFFFFF} this property?\n{FFFFFF}%s\n{00FF00}SQLID: %d", PropertyInfo[Property][prDescript], PropertyInfo[Property][prID]);
        CreateConfirmationDialog(playerid, string, "DestroyPropertyConfirmation", Property);
        return true;
    }
    else if(!strcmp(Act, "edit", true)) {
        if(!IsAnAdmin(playerid, ADMIN_LEVEL_LEAD)) return true;
        new Subject[15];
        if(sscanf(params, "s[8]s[15]", Act, Subject)) {
            Text_Send(playerid, $COMMAND_SYNTAX, YCMD:property, "edit <subject> <value>");
            return true;
        }
        if(!strcmp(Subject, "description", true)
        || !strcmp(Subject, "descript", true)
        || !strcmp(Subject, "descr", true)) {
            // Todo
        }
        else if(!strcmp(Subject, "interior", true)
        || !strcmp(Subject, "int", true)) {
            new Property = GetPlayerInProperty(playerid);
            if(Property == -1) return true;
            StartPlayerPInteriorChange(playerid, Property);
            return true;
        }
        else if(!strcmp(Subject, "position", true)
        || !strcmp(Subject, "pos", true)) {
            // Todo
        }
        return true;
    }
    Text_Send(playerid, $COMMAND_SYNTAX, YCMD:property, "<param> (buy; sell; list; find; status; sellto)");
    return true;
}

CMD:knock(playerid, params[]) {
    if(!LoggedIn(playerid)) return true;
    new Property = GetPlayerNearProperty(playerid, 5.0, true, false);
    if(Property == -1) return Text_Send(playerid, $NO_DOOR_HERE);
    OnPlayerKnockedPropertyDoor(playerid, Property);
    return true;
}
