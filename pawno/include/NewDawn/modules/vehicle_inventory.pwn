/**
 * �������������� ����� ������ ��������� ��� ������� �����
 *
 * @version 0.5 
 * @author Moroz <moroz@malefics.com
 */
#if !defined vehicle_inventory_included
	#endinput
#endif 

#include <YSI\y_text>

loadtext Vehicles[Errors], Vehicles[Messages], Items[Messages], Items[Errors];


/** 
 * Adds an item to the vehicle inventory
 * @param int vehicle ID
 * @param int item ID
 * @param int quantity
 * @param int serial
 *
 * @return true if succeeded and false if faile
 */
 
stock bool:Vehicle_AddItem(veh, item, quantity, serial=0) {
	new model = Vehicles[veh][vModel]-400;
	if(Vehicle_GetInventoryItemCount(veh) >= VehInfo[model][V_C_TRUNK_SIZE]) {
		return false;
	}
	for(new i=0;i<VehInfo[model][V_C_TRUNK_SIZE] && i < VEHICLE_TRUNK_SIZE;i++) {
		if((Vehicles[veh][vInvItem][i] == item && Items[item][itemType] != ITEM_TYPE_WEAPON) && 
			(Vehicles[veh][vInvSerial][i] != serial || serial == 0) || Vehicles[veh][vInvItem][i] == 0) {
			Vehicles[veh][vInvItem][i] = item;
			Vehicles[veh][vInvQuantity][i] += quantity;
			Vehicles[veh][vInvSerial][i] = serial;
			break;
		}
	}
	return true;
}

/**
 * ������� ������ � ���������� ������
 *
 * @param int ID ������
 * @param int ID ������ (�����������)
 */
stock Player_ShowVehicleInventory(playerid, vehicle) {
	new result[756];
	SetPVarInt(playerid, "VehInventory", vehicle);
	for(new i=0;i<VehInfo[Vehicles[vehicle][vModel]-400][V_C_TRUNK_SIZE] && i < VEHICLE_TRUNK_SIZE;i++) {
		if(Vehicles[vehicle][vInvItem][i] == 0) break;
		if(Vehicles[vehicle][vInvSerial][i])
			format(result, sizeof(result), "%s%s (%d ��., #%d)\n", result, Items[Vehicles[vehicle][vInvItem][i]][itemName], 
				Vehicles[vehicle][vInvQuantity][i], Vehicles[vehicle][vInvSerial][i]);
		else	
			format(result, sizeof(result), "%s%s (%d ��.)\n", result, Items[Vehicles[vehicle][vInvItem][i]][itemName], 
				Vehicles[vehicle][vInvQuantity][i]);
	}
	if(Vehicle_GetInventoryItemCount(vehicle) < VehInfo[Vehicles[vehicle][vModel]-400][V_C_TRUNK_SIZE])
		strcat(result, "��������");
	
	Dialog_ShowCallback(playerid, using callback VehicleDialog, DIALOG_STYLE_LIST, "�������� ����������", result, "�������", "������");
	return;
}

/**
 * ��������� ��������� ������
 *
 * @param int ID ������
 */
stock Vehicle_SortInventory(vehicle) {
	for(new i=0;i<VehInfo[Vehicles[vehicle][vModel]-400][V_C_TRUNK_SIZE]-1 && i < VEHICLE_TRUNK_SIZE-1;i++) {
		if(Vehicles[vehicle][vInvItem][i] <= 0 || Vehicles[vehicle][vInvQuantity][i] <= 0) {
			Vehicles[vehicle][vInvItem][i] = Vehicles[vehicle][vInvItem][i+1];
			Vehicles[vehicle][vInvQuantity][i] = Vehicles[vehicle][vInvQuantity][i+1];
			Vehicles[vehicle][vInvSerial][i] = Vehicles[vehicle][vInvSerial][i+1];
			Vehicles[vehicle][vInvItem][i+1] = 0;
			Vehicles[vehicle][vInvQuantity][i+1] = 0;
			Vehicles[vehicle][vInvSerial][i+1] = 0;
		}
	}
}

/**
 * ���������� ���-�� ��������� � ��������� ������
 *
 * @param int ID ������
 * @return int ����
 */
stock Vehicle_GetInventoryItemCount(vehicle) {
	new count = 0;
	for(new i=0;i<VehInfo[Vehicles[vehicle][vModel]-400][V_C_TRUNK_SIZE] && i < VEHICLE_TRUNK_SIZE;i++) {
		if(Vehicles[vehicle][vInvItem][i] <= 0)
			break;
		count ++;
	}
	return count;

}

public VehicleItemObserve(playerid, dialogid, response, listitem, string:inputtext[]) {
	#pragma unused inputtext, dialogid, response
	listitem = 	GetPVarInt(playerid, "VehInventoryItem");
	new veh = GetPVarInt(playerid, "VehInventory");
	if(listitem == -1) {
		Player_ShowVehicleInventory(playerid, GetPVarInt(playerid, "VehInventory"));
		return 1;
	}
	new title[48];
	format(title, sizeof(title), "��������� %s - �������� ��� %s", VehInfo[GetVehicleModel(veh)-400][V_C_NAME], 
		Items[Vehicles[veh][vInvItem][listitem]][itemName]);
	Dialog_ShowCallback(playerid, using callback VehicleItemAction, DIALOG_STYLE_LIST, title, "���������\n�������", "�������", "������");
	return 1;
}

public PutInCarInventory_Numbered(playerid, dialogid, response, listitem, string:inputtext[]) {
	new slot = GetPVarInt(playerid, "CurrentInvItem"), result = strval(inputtext), 
		quantity, item, serial, str[64], veh = GetPVarInt(playerid, "VehInventory");
	GetPlayerSlotInfo(playerid, slot, item, quantity, serial);
	if(!response) {
		Player_ShowVehicleInventory(playerid, GetPVarInt(playerid, "VehInventory"));
		return 1;
	}
	if(result > quantity) {
		Text_Send(playerid, $AMOUNT_IS_TOO_HIGH);
		format(str, sizeof(str), "������� %s �� ������ ��������?", Items[item][itemName]);
		Dialog_ShowCallback(playerid, using callback PutInCarInventory_Numbered, DIALOG_STYLE_INPUT, "���������", str, "��������", "������");
		return 1;
	}
	else if(result < 1) {
		Text_Send(playerid, $AMOUNT_MUST_BE_POSITIVE);
		format(str, sizeof(str), "������� %s �� ������ ��������?", Items[item][itemName]);
		Dialog_ShowCallback(playerid, using callback PutInCarInventory_Numbered, DIALOG_STYLE_INPUT, "���������", str, "��������", "������");
		return 1;
	}
	TakeItem(playerid, slot, result);
	Vehicle_AddItem(veh, item, result, serial);
	format(str, sizeof(str), "/me ������ %d %s � ��������", result, Items[item][itemName]);
	Command_ReProcess(playerid, str, 0);
	Player_ShowVehicleInventory(playerid, veh);
	
	return 1;
}

public PutInCarInventory(playerid, dialogid, response, listitem, string:inputtext[]) {
	if(!response) {
		Player_ShowVehicleInventory(playerid, GetPVarInt(playerid, "VehInventory"));
		return 1;
	}
	new item, quantity, serial, str[64];
	GetPlayerSlotInfo(playerid, listitem, item, quantity, serial);
	SetPVarInt(playerid, "CurrentInvItem", listitem);
	if(quantity > 1) {
		format(str, sizeof(str), "������� %s �� ������ ��������?", Items[item][itemName]);
		Dialog_ShowCallback(playerid, using callback PutInCarInventory_Numbered, DIALOG_STYLE_INPUT, "���������", str, "��������", "������");
		return 1;
	}
	else	{
		SetPVarInt(playerid, "CurrentInvItemQuantity", 1);
		PutInCarInventory_Numbered(playerid, dialogid, response, listitem, "1");
	}
	return 1;
}
public VehicleItemAction(playerid, dialogid, response, listitem, string:inputtext[]) {
	#pragma unused inputtext, dialogid
	new veh = GetPVarInt(playerid, "VehInventory"), string[OUTPUT], item = GetPVarInt(playerid, "VehInventoryItem");
	if(!response) {
		Player_ShowVehicleInventory(playerid, veh);
		return;
	}
	if(listitem == 0) { // ������
		if(Vehicles[veh][vInvSerial][item] > 0) 
			format(string, sizeof(string), "�������: %s\n����������:%d\n���:%s\n���:%.3f ��\n�����:#%d",
				Items[Vehicles[veh][vInvItem][item]][itemName], Vehicles[veh][vInvQuantity][item],
				ItemType[Vehicles[veh][vInvItem][item]], Items[Vehicles[veh][vInvItem][item]][itemWeight] * Vehicles[veh][vInvQuantity][item],
				Vehicles[veh][vInvSerial][item]);
		else if(Vehicles[veh][vInvSerial][item] == 0)
			format(string, sizeof(string), "�������: %s\n����������:%d\n���:%s\n���:%.3f ��",
				Items[Vehicles[veh][vInvItem][item]][itemName], Vehicles[veh][vInvQuantity][item],
				ItemType[Vehicles[veh][vInvItem][item]], Items[Vehicles[veh][vInvItem][item]][itemWeight] * Vehicles[veh][vInvQuantity][item]);
		else 
			format(string, sizeof(string), "�������: %s\n����������:%d\n���:%s\n���:%.3f ��\n����� ������",
				Items[Vehicles[veh][vInvItem][item]][itemName], Vehicles[veh][vInvQuantity][item],
				ItemType[Vehicles[veh][vInvItem][item]], Items[Vehicles[veh][vInvItem][item]][itemWeight] * Vehicles[veh][vInvQuantity][item]);
				
			
		Dialog_ShowCallback(playerid, using callback VehicleItemObserve, DIALOG_STYLE_LIST, "������ ���������", string, "���������");
	}
	else {
		if(GiveItem(playerid, Vehicles[veh][vInvItem][item], Vehicles[veh][vInvQuantity][item], Vehicles[veh][vInvSerial][item])) {
			Text_Send(playerid, $INVENTORY_HAS_NO_ROOM);
			return;
		}
		Text_Send(playerid, $ITEM_PICKUP, Items[Vehicles[veh][vInvItem][item]][itemName], Vehicles[veh][vInvQuantity][item]);
		Vehicles[veh][vInvItem][item] = 0;
		Vehicles[veh][vInvQuantity][item] = 0;
		Vehicles[veh][vInvSerial][item] = 0;
	}
	return;
}

public VehicleDialog(playerid, dialogid, response, listitem, string:inputtext[]) { 
	new veh = GetPVarInt(playerid, "VehInventory");
	#pragma unused inputtext, dialogid
	if(!response) {
		DeletePVar(playerid, "VehInventory");
		return;
	}
	if(PlayerToVehicle(playerid, veh) > 5.0) {
		return;
	}
	if(listitem == Vehicle_GetInventoryItemCount(veh)) {
		
		if(listitem == VehInfo[GetVehicleModel(veh)-400][V_C_NAME] || listitem >= VEHICLE_TRUNK_SIZE) {
			Text_Send(playerid, $INVENTORY_HAS_NO_ROOM);
			return;
		}
		new string[2048];
		for(new i=0;i<MAX_ITEMS;i++) {
			new item, quantity, serial;
			GetPlayerSlotInfo(playerid, i, item, quantity, serial);
			if(!item) {
				continue;
			}
			if(serial)
				format(string, sizeof(string), "%s%24s\t[{00AA00}%3d{FFFFFF} ��., {00AA00}%.3f{FFFFFF}��., #%5d] \n", 
					string, Items[item][itemName],  quantity, CalculateItemWeight(playerid, i), serial);
			else 
				format(string, sizeof(string), "%s%24s\t[{00AA00}%3d{FFFFFF} ��., {00AA00}%.3f{FFFFFF}��.] \n", 
					string, Items[item][itemName],  quantity, CalculateItemWeight(playerid, i));
				
		}
		strmid(string, string, 0, strlen(string)-1);


		if(strlen(string) < 30) {
			Text_Send(playerid, $INVENTORY_IS_EMPTY);
			return;
		}
		Dialog_ShowCallback(playerid, using callback PutInCarInventory, DIALOG_STYLE_LIST, "���������", string, "��������", "������");
		return;
	}
	new title[48];
	format(title, sizeof(title), "��������� %s - �������� ��� %s", VehInfo[GetVehicleModel(veh)-400][V_C_NAME], 
		Items[Vehicles[veh][vInvItem][listitem]][itemName]);
	SetPVarInt(playerid, "VehInventoryItem", listitem);
	Dialog_ShowCallback(playerid, using callback VehicleItemAction, DIALOG_STYLE_LIST, title, "���������\n�������", "�������", "������");
	return;
}

