/**
 * i18n.inc
 *
 * Language: ENG
 * 
 * File to add your localization macros into
 * Format: LANG_<MODULENAME>_<TEXTNAME>
 * e.g.: #define LANG_JOB_FOO "derp herp merp"
 *
 * @author Moroz <igorsmorozov@gmail.com>
 */ 
 
#define ND_BUTTON_OK     "OK"
#define ND_BUTTON_NEXT "Next"
#define ND_BUTTON_PREVIOUS "Prev."
#define ND_BUTTON_SUBMIT "Submit"
#define ND_BUTTON_CANCEL "Cancel"
#define ND_BUTTON_SELECT "Select"
 
#define LANG_ADMIN_ADUTY_TITLE   "Admin Duty"
#define LANG_ADMIN_ADUTY_CONTENT "Enter your admin code. Note! If you enter a wrong code, you will be banned"