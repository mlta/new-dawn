#if defined sweeper_job_included
	#endinput
#endif
#define sweeper_job_included

/**
 * Implementing a simple checkpoint job system
 */

#define MAX_JOB_CHECKPOINTS 20
 
 
enum JCPType {
	JOBCP_TYPE_NONE,
	JOBCP_TYPE_RANDOM,
	JOBCP_TYPE_INDEXED
};

enum JOB_CHECKPOINTS {
	Float:jcpPosX,
	Float:jcpPosY,
	Float:jcpPosZ,
	Float:size
	jcpExtraid // useful for payments and stuff
};

static PlayerCP[MAX_PLAYERS][MAX_JOB_CHECKPOINTS][JCPType];
static JCPType:PlayerCPType[MAX_PLAYERS];
static JCPType:PlayerCPIndex[MAX_PLAYERS];




stock Job_StopRoute(playerid) {	
	new tmp[MAX_JOB_CHECKPOINTS][JCPType];
	PlayerCP[playerid] = tmp;
	PlayerCPType[playerid] = JOBP_TYPE_NONE;
	PlayerCPIndex[playerid] = 0;
	return;
}


stock Job_SetCheckpoint(playerid, cpidx, nextcp = cpidx+1) {
	if(nextcp == 0) {
		Job_StopRoute(playerid);
		return 1;
	}
	cpidx --;
	nextcp --;
	SetPlayerRaceCheckpoint(playerid, nextcp == cpidx, PlayerCP[playerid], 
		PlayerCP[playerid][cpidx][jcpPosX],  PlayerCP[playerid][cpidx][jcpPosY],  PlayerCP[playerid][cpidx][jcpPosZ], 
		PlayerCP[playerid][nextcp][jcpPosX],  PlayerCP[playerid][nextcp][jcpPosY],  PlayerCP[playerid][nextcp][jcpPosZ],
		);
	PlayerCPIndex[playerid] ++;
	CallRemoteFunction("OnPlayerEnterJobCheckpoint", "iii", playerid, cpid, nextcp);
	return 1;
}

stock Job_StartRoute(playerid, const &array[][JOB_CHECKPOINTS], JCPType:type = JOBCP_TYPE_INDEXED, start=1, end = sizeof(array)) {
	PlayerCP[playerid] = array;
	PlayerCPType[playerid] = type;
	return 1;
}


hook OnPlayerConnect(playerid) {
	Job_StopRoute(playerid);
	return 1;
}


hook OnPlayerEnterDynamicCP(playerid, checkpointid) {
	return 1;
}


YCMD:sweeper(playerid, params[], help) {
	new tmp[5][JOB_CHECKPOINTS];
	Job_StartRoute(playerid, tmp);
	return 1;
}