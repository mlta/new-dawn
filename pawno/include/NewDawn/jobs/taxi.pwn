/**
 * taxi.pwn
 *
 * @author Moroz
 */

#if defined taxi_job_included
	#endinput
#endif

#include <YSI\y_text>

loadtext Core[Commands], Core[Errors];

static Float:TaxiFare[MAX_VEHICLES][2]; // 0 is per km, 1 is per raid
static bool:IsTaxi[MAX_VEHICLES];

timer TaxiCounter[1000](vehicle) {
	new Float:dist = GetVehicleSpeed(vehicle) / 3600, 
		price = dist*TaxiFare[0];
	TaxiFare[1] += price;
	return 1;
}

forward TaxiCallback(playerid, dialogid, response, listitem, string:inputtext[]);
public TaxiCallback(playerid, dialogid, response, listitem, string:inputtext[]) {
	new veh = GetPlayerVehicleID(playerid);
	if(response || veh == 0 || GetPlayerState(playerid) != PLAYER_STATE_DRIVER) {	
		return 1;
	}

	if(listitem == 0) {
		
	}
	else if(listitem == 1) {
		IsTaxi[veh] = !IsTaxi[veh];
		TaxiFare[1] = 0.0;
		Command_ReProcess(playerid, "/taxi", 0);
	}
	return 1;
}


YCMD:taxi(playerid, params[], help) {
	new string[OUTPUT];
	if(!IsPlayerInAnyVehicle(playerid)) {
		Text_Send(playerid, $STATE_IN_VEHICLE);
		return 1;
	}
	if(GetPlayerState(playerid) == PLAYER_STATE_DRIVER) {
		if(IsTaxi[GetPlayerVehicleID(playerid)]) {
			format(string, sizeof(string), "{FFFFFF}Current fare is {1111BB}$%.2f per km. [CHANGE]{FFFFFF}\nCost: {1111BB}$%d [DISABLE&RESET]", TaxiFare[0], TaxiFare[1]);
		} 
		else {
			format(string, sizeof(string), "{FFFFFF}Current fare is {1111BB}$%.2f per km. [CHANGE]{FFFFFF}\nCost: {1111BB}$%d [ENABLE]", TaxiFare[0], TaxiFare[1]);
		}
		Dialog_ShowCallback(playerid, using callback TaxiCallback, DIALOG_STYLE_PASSWORD, "{FF0000}Taxi", string, "OK", "Cancel");
	else {
		format(string, sizeof(string), "{FFFFFF}Current fare is {1111BB}$%.2f per km. {FFFFFF}\nCost: {1111BB}$%d", TaxiFare[0], TaxiFare[1]);
		ShowPlayerDialog(playerid, 0, DIALOG_STYLE_MSGBOX, "{FF0000}Taxi", string, "OK", "");
	}
	return 1;
}