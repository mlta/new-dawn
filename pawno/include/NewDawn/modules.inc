/**
 * List include files of modules
 * @author Moroz <moroz@malefics.com>
 * @author Jonathan_Rosewood <jonathan-rosewood@yandex.ru>
 *
 * Example of major modules:
 * #include <gamemode/functions/filename.pwn>
 *
 * Example of minor modules:
 * #tryinclude <gamemode/functions/filename.pwn>
 *
 */

#tryinclude <NewDawn/modules/admin.pwn>
#tryinclude <NewDawn/modules/anticheat.pwn>
#tryinclude <NewDawn/modules/inventory.pwn>
#tryinclude <NewDawn/modules/vehicle_inventory.pwn>
#tryinclude <NewDawn/modules/vehicles.pwn>
#tryinclude <NewDawn/modules/factions.pwn>
#tryinclude <NewDawn/modules/phone.pwn>
#tryinclude <NewDawn/modules/factions_law.pwn>
#tryinclude <NewDawn/modules/property.pwn>
#tryinclude <NewDawn/modules/property_furniture.pwn>
#tryinclude <NewDawn/modules/property_buying.pwn>
#tryinclude <NewDawn/modules/prison.pwn>
#tryinclude <NewDawn/modules/commands.pwn>
#tryinclude <NewDawn/modules/lists.pwn>
#tryinclude <NewDawn/modules/maps.pwn>
#tryinclude <NewDawn/modules/radio.pwn>
#tryinclude <NewDawn/modules/bank.pwn>  
//#tryinclude <NewDawn/modules/drugs.pwn>  
